package com.lubgo.app.rest;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.lubgo.app.base.BaseRestTest;
import com.lubgo.app.dao.UserRepository;
import com.lubgo.app.model.User;
import com.lubgo.app.service.TestUserService;

import sun.security.acl.PrincipalImpl;

public class TestUserRestWebService extends BaseRestTest {

    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testCreateUser() throws Exception {
        mockMvc.perform(post("/user").contentType(MediaType.APPLICATION_JSON)
                                     .content("{\"plainTextPassword\": \"Password5\", \"email\": \"test@gmail.com\"}")
                                     .accept(MediaType.APPLICATION_JSON)
                                     .principal(new PrincipalImpl(TestUserService.EMAIL)))
               .andDo(print())
               .andExpect(status().isOk());

        User user = userRepository.findUserByEmail("test@gmail.com");
        assertTrue("email not correct: " + user.getEmail(), "test@gmail.com".equals(user.getEmail()));
    }

}
