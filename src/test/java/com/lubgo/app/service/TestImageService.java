package com.lubgo.app.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.lubgo.app.base.BaseTest;
import com.lubgo.app.model.Image;
import com.lubgo.app.services.ImageService;

public class TestImageService extends BaseTest {

    private static final Logger LOGGER = Logger.getLogger(ImageService.class);

    @Autowired
    private ImageService imageService;

    @PersistenceContext
    private EntityManager em;

    @Test
    public void saveImage() throws IOException {

        // String name = "test-image.jpg";
        String name = "time-square.png";

        URL url = TestImageService.class.getResource(name);

        File imageFile = new File(url.getFile());

        byte[] readFileToByteArray = FileUtils.readFileToByteArray(imageFile);

        String uuid = imageService.uploadImage(readFileToByteArray);

        Assert.notNull(uuid);

        Image image = imageService.saveImage(uuid);

        Assert.notNull(image);
        Assert.notNull(image.getId());

    }

    public static void main(String[] args) {

        String tempDir = "C:\\Users\\ToZLa\\Desktop\\Slike\\MashaCopy";

        for (int i = 99; i < 2200; i++) {

            String prefix = "00";
            try {

                if (i > 100) {
                    prefix = "0";
                }

                if (i > 1000) {
                    prefix = "";
                }

                String imageName = "CAM0" + prefix + i + ".jpg";
                File tempImageFile = new File(tempDir + File.separator + imageName);

                BufferedImage src = ImageIO.read(tempImageFile);

                // BufferedImage resized = Scalr.resize(src, Method.QUALITY, 1200, 675);
                BufferedImage resized = Scalr.resize(src, Method.QUALITY, 1024, 768);
                ImageIO.write(resized, "png", tempImageFile);

                LOGGER.debug("Image resized " + imageName);
            }
            catch (Exception e) {}

        }

    }

}
