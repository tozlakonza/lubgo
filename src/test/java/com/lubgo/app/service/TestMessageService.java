package com.lubgo.app.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.lubgo.app.base.BaseTest;
import com.lubgo.app.model.Conversation;
import com.lubgo.app.model.Message;
import com.lubgo.app.model.User;
import com.lubgo.app.services.ConversationService;
import com.lubgo.app.services.MessageService;
import com.lubgo.app.services.UserService;

public class TestMessageService extends BaseTest {

    @Autowired
    private ConversationService conversationService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private UserService userService;

    @PersistenceContext
    private EntityManager em;

    @Test
    @SuppressWarnings("unchecked")
    public void saveMessage() {

        userService.createUser("John", "Doe", "participant1@gmail.com", "Password3");
        User participant = userService.findUserByEmail("participant1@gmail.com");

        List<Conversation> conversations = em.createQuery("select u from Conversation u ").getResultList();

        int size = conversations.size();

        Conversation conversation = conversationService.saveConversation(TestUserService.EMAIL, null, null);

        conversations = em.createQuery("select u from Conversation u ").getResultList();

        Assert.notNull(conversations);
        Assert.isTrue(conversations.size() == size + 1);
        Assert.notNull(conversation.getId());

        Long conversationId = conversation.getId();

        HashSet<String> emails = new HashSet<String>();
        emails.add(participant.getEmail());

        Message message = messageService.saveMessage(TestUserService.EMAIL, conversationId, "Caoo :)", null);

        Assert.notNull(message);
        Assert.notNull(message.getId());
        Assert.notNull(message.getConversation());

        messageService.deleteMessageById(message.getId());

        conversation = conversationService.findConversationById(message.getConversation().getId());
        Assert.notNull(conversation);
        Assert.notNull(conversation.getParticipants());
        Set<User> participants = conversation.getParticipants();
        Assert.isTrue(participants.size() == 2, "Expected size was 2");

        conversationService.leaveConversationById(participant.getEmail(), conversationId);
        conversation = conversationService.findConversationById(message.getConversation().getId());
        participants = conversation.getParticipants();
        Assert.isTrue(conversation.getParticipants().size() == 1, "Expected size was 1");

        conversationService.addToConversationById(participant.getEmail(), conversationId);
        conversation = conversationService.findConversationById(message.getConversation().getId());
        participants = conversation.getParticipants();
        Assert.isTrue(participants.size() == 2, "Expected size was 2");

        conversationService.deleteConversationById(conversationId);

    }

}
