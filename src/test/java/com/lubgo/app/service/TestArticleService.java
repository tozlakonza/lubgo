package com.lubgo.app.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.lubgo.app.base.BaseTest;
import com.lubgo.app.model.Article;
import com.lubgo.app.model.Post;
import com.lubgo.app.services.ArticleService;
import com.lubgo.app.services.PostService;
import com.lubgo.app.services.UserService;

public class TestArticleService extends BaseTest {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @PersistenceContext
    private EntityManager em;

    @Test
    public void savePost() {

        userService.createUser("John", "Doe", "participant1@gmail.com", "Password3");

        Article saveArticle = articleService.saveArticle(TestUserService.EMAIL, null, "title", "body", null);
        Assert.notNull(saveArticle);
        Assert.notNull(saveArticle.getId());

        Post commentPost1 = postService.saveComment(TestUserService.EMAIL, saveArticle.getId(), "comment1");
        postService.saveComment(TestUserService.EMAIL, saveArticle.getId(), "comment2");

        postService.deleteComment(saveArticle.getId(), commentPost1.getId());

        postService.likePost("participant1@gmail.com", saveArticle.getId());

        saveArticle = articleService.findArticleById(saveArticle.getId());

        Assert.isTrue(!saveArticle.getLikes().isEmpty());
        Assert.isTrue(saveArticle.getLikes().size() == 1);

        postService.unlikePost("participant1@gmail.com", saveArticle.getId());

        saveArticle = articleService.findArticleById(saveArticle.getId());

        Assert.isTrue(saveArticle.getLikes().isEmpty());

        articleService.deleteArticle(saveArticle.getId());

        saveArticle = articleService.findArticleById(saveArticle.getId());

        Assert.isNull(saveArticle);

    }

}
