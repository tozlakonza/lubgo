package com.lubgo.app.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.lubgo.app.base.BaseTest;
import com.lubgo.app.model.User;
import com.lubgo.app.services.UserService;

public class TestUserService extends BaseTest {

    public static final String EMAIL = "test@email.com";

    @Autowired
    private UserService userService;

    @PersistenceContext
    private EntityManager em;

    @Test
    public void testFindUserByEmail() {
        User user = findUserByEmail(EMAIL);
        assertNotNull("User is mandatory", user);
        assertTrue("Unexpected user " + user.getEmail(), user.getEmail().equals(EMAIL));
    }

    @Test
    public void testUserNotFound() {
        User user = findUserByEmail("doesnotexist");
        assertNull("User must be null", user);
    }

    @Test
    public void testCreateValidUser() {
        userService.createUser("John", "Doe", "test123@gmail.com", "Password3");
        User user = findUserByEmail("test123@gmail.com");

        assertTrue("email not expected " + user.getEmail(), "test123@gmail.com".equals(user.getEmail()));

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        assertTrue("password not expected " + user.getPasswordDigest(),
                   passwordEncoder.matches("Password3", user.getPasswordDigest()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBlankUser() {
        userService.createUser("John", "Doe", "", "Password3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBlankEmail() {
        userService.createUser("John", "Doe", "", "Password3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidEmail() {
        userService.createUser("John", "Doe", "test", "Password3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBlankPassword() {
        userService.createUser("John", "Doe", "test@gmail.com", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPasswordPolicy() {
        userService.createUser("John", "Doe", "test@gmail.com", "Password");
    }

    private User findUserByEmail(String email) {
        List<User> users = em.createQuery("select u from User u where email = :email")
                             .setParameter("email", email)
                             .getResultList();

        return users.size() == 1 ? users.get(0) : null;
    }

}
