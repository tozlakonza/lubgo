package com.lubgo.app.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.lubgo.app.base.BaseTest;
import com.lubgo.app.model.Post;
import com.lubgo.app.services.PostService;
import com.lubgo.app.services.UserService;

public class TestPostService extends BaseTest {

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @PersistenceContext
    private EntityManager em;

    @Test
    public void savePost() {

        userService.createUser("John", "Doe", "participant1@gmail.com", "Password3");

        Post savePost = postService.savePost(TestUserService.EMAIL, null, null, null, "text", null, null, null, null, null);
        Assert.notNull(savePost);
        Assert.notNull(savePost.getId());

        Post commentPost1 = postService.saveComment(TestUserService.EMAIL, savePost.getId(), "comment1");
        postService.saveComment(TestUserService.EMAIL, savePost.getId(), "comment2");

        postService.deleteComment(savePost.getId(), commentPost1.getId());

        postService.likePost("participant1@gmail.com", savePost.getId());

        savePost = postService.getPost(savePost.getId());

        Assert.isTrue(!savePost.getLikes().isEmpty());
        Assert.isTrue(savePost.getLikes().size() == 1);

        postService.unlikePost("participant1@gmail.com", savePost.getId());

        savePost = postService.getPost(savePost.getId());

        Assert.isTrue(savePost.getLikes().isEmpty());

        postService.deletePost(savePost.getId());

    }

}
