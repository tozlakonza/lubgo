package com.lubgo.app.service;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.lubgo.app.base.BaseTest;
import com.lubgo.app.model.Connection;
import com.lubgo.app.model.User;
import com.lubgo.app.services.ConnectionService;
import com.lubgo.app.services.UserService;

/**
 * Created by Pedja on 1/9/2016.
 */
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class })
public class TestConnectionService extends BaseTest {

    @Autowired
    private ConnectionService connectionService;

    @Autowired
    private UserService userService;

    private User user1;
    private User user2;
    private Connection cr;

    public void setup() {
        user1 = userService.createUser("John", "Doe", "participant1@gmail.com", "Password3");
        user2 = userService.createUser("John 2", "Doe 2", "participant2@gmail.com", "Password3");
    }

    @Test
    @Transactional
    public void testAcceptConnectionRequest() {
        setup();

        cr = connectionService.saveConnection(user1, user2);

        connectionService.acceptConnection(user1, user2);

        List<Connection> connectionsUser1 = connectionService.findAcceptedConnectionsByUserId(user1.getId());
        List<Connection> connectionsUser2 = connectionService.findAcceptedConnectionsByUserId(user2.getId());

        assertTrue(connectionsUser1.size() == 1);
        assertTrue(connectionsUser2.size() == 1);

        assertTrue(cr.getAccepted());
    }

    @Test
    @Transactional
    public void testDeclineConnectionRequest() {
        setup();
        cr = connectionService.saveConnection(user1, user2);

        connectionService.declineConnection(user1, user2);

        List<Connection> connectionsUser1 = connectionService.findDeclinedConnectionsByUserId(user1.getId());
        List<Connection> connectionsUser2 = connectionService.findDeclinedConnectionsByUserId(user2.getId());

        assertTrue(connectionsUser1.size() == 1);
        assertTrue(connectionsUser2.size() == 1);
        assertTrue(!cr.getAccepted());
    }

}
