package com.lubgo.app.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestArticleService.class, TestConnectionService.class, TestImageService.class, TestMessageService.class, TestPostService.class,
    TestUserService.class })
public class AllServiceTests {

}
