package com.lubgo.app.base;

import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.lubgo.config.root.ApplicationProperties;
import com.lubgo.config.root.RootContextConfig;
import com.lubgo.config.root.TestConfiguration;
import com.lubgo.config.servlet.ServletContextConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ActiveProfiles("test")
@ContextConfiguration(classes = { TestConfiguration.class, RootContextConfig.class, ApplicationProperties.class, ServletContextConfig.class })
public class BaseRestTest {

}
