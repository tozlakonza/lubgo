package com.lubgo.config.root;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.lubgo.app.init.TestDataInitializer;

/**
 *
 * Development specific configuration - creates a localhost postgresql
 * datasource, sets hibernate on create drop mode and inserts some test data on
 * the database.
 *
 * Set -Dspring.profiles.active=development to activate this config.
 *
 */
@Configuration
@EnableTransactionManagement
public class ApplicationConfiguration {

    @Autowired
    ApplicationProperties applicationProperties;

    @Bean(initMethod = "init")
    public TestDataInitializer initTestData() {
        if (applicationProperties.isInitTestData()) {
            return new TestDataInitializer();
        }
        else {
            return null;
        }
    }

    @Bean(name = "datasource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(applicationProperties.getDbDriver());
        dataSource.setUrl(applicationProperties.getDbUrl());
        dataSource.setUsername(applicationProperties.getDbUsername());
        dataSource.setPassword(applicationProperties.getDbPassword());
        return dataSource;
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DriverManagerDataSource dataSource) {

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPackagesToScan(new String[] { "com.lubgo.app.model" });
        entityManagerFactoryBean.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        Map<String, Object> jpaProperties = new HashMap<String, Object>();
        jpaProperties.put("hibernate.show_sql", applicationProperties.getHibernateShowSql());
        jpaProperties.put("hibernate.format_sql", applicationProperties.getHibernateFormatSql());
        jpaProperties.put("hibernate.use_sql_comments", applicationProperties.getHibernateUseSqlComments());
        jpaProperties.put("hibernate.dialect", applicationProperties.getHibernateDialect());
        entityManagerFactoryBean.setJpaPropertyMap(jpaProperties);

        return entityManagerFactoryBean;
    }

}
