package com.lubgo.config.root;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import lombok.Getter;

@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationProperties {

    @Value("${db.url}")
    private @Getter String dbUrl;
    @Value("${db.driver}")
    private @Getter String dbDriver;
    @Value("${db.username}")
    private @Getter String dbUsername;
    @Value("${db.password}")
    private @Getter String dbPassword;

    @Value("${hibernate.show_sql}")
    private @Getter String hibernateShowSql;
    @Value("${hibernate.format_sql}")
    private @Getter String hibernateFormatSql;
    @Value("${hibernate.use_sql_comments}")
    private @Getter String hibernateUseSqlComments;
    @Value("${hibernate.dialect}")
    private @Getter String hibernateDialect;

    @Value("${files.upload.path}")
    private @Getter String filesUploadPath;

    @Value("${init.test.data}")
    private @Getter boolean initTestData;

    @Value("${email.enabled}")
    private @Getter boolean emailEnabled;
    @Value("${email.no_reply}")
    private @Getter String noReplyAddress;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
