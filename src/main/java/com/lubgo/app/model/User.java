package com.lubgo.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "USERS")
@NamedQueries({
    @NamedQuery(name = User.FIND_BY_EMAIL, query = "select u from User u where u.email = :email"),
    @NamedQuery(name = User.FIND, query = "select u from User u where lower(u.email) like :term or lower(u.firstName) like :term or lower(u.lastName) like :term"),
    @NamedQuery(name = User.FIND_BY_UUID, query = "select u from User u where u.uuidStr = :uuid"),
    @NamedQuery(name = User.CHECK_IF_CONNECTED, query = "select count(c) from Connection c where ((c.from.uuidStr = :fromId and c.to.uuidStr = :toId) or (c.from.uuidStr = :toId and c.to.uuidStr = :fromId)) and accepted = true")
})
public class User extends AbstractEntity {

    public static final String FIND_BY_EMAIL = "user.findByEmail";
    public static final String FIND = "user.find";
    public static final String FIND_BY_UUID = "user.findByUUID";
    public static final String CHECK_IF_CONNECTED = "user.checkIfConnected";

    private String firstName;
    private String lastName;
    private String email;
    private String passwordDigest;
    private Date birthday;
    private String alliance;
    private String club;
    private String phone;
    private String location;
    private String autobiography;

    @OneToOne
    private Image image;

    public User() {

    }

    private User(UserBuilder builder) {
        this.email = builder.email;
        this.passwordDigest = builder.passwordDigest;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.phone = builder.phone;
        this.alliance = builder.alliance;
        this.location = builder.location;
        this.club = builder.club;
        this.autobiography = builder.autobiography;
        this.birthday = builder.birthday;
        this.image = builder.image;
    }

    public String getPasswordDigest() {
        return passwordDigest;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPasswordDigest(String passwordDigest) {
        this.passwordDigest = passwordDigest;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAlliance() {
        return alliance;
    }

    public void setAlliance(String alliance) {
        this.alliance = alliance;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getAutobiography() {
        return autobiography;
    }

    public void setAutobiography(String autobiography) {
        this.autobiography = autobiography;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getName() {
        return getFirstName() + " " + getLastName();
    }

    @Override
    public String toString() {
        return "User{" + "email='" + email + '}';
    }

    public static class UserBuilder {

        private String firstName;
        private String lastName;
        private String email;
        private String passwordDigest;
        private Date birthday;
        private String alliance;
        private String club;
        private String phone;
        private String location;
        private String autobiography;
        private Image image;

        public UserBuilder() {

        }

        public UserBuilder email(String email) {
            this.email = email;
            return this;
        }

        public UserBuilder passwordDigest(String passwordDigest) {
            this.passwordDigest = passwordDigest;
            return this;
        }

        public UserBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public UserBuilder alliance(String alliance) {
            this.alliance = alliance;
            return this;
        }

        public UserBuilder location(String location) {
            this.location = location;
            return this;
        }

        public UserBuilder club(String club) {
            this.club = club;
            return this;
        }

        public UserBuilder autobiography(String autobiography) {
            this.autobiography = autobiography;
            return this;
        }

        public UserBuilder birthday(Date birthday) {
            this.birthday = birthday;
            return this;
        }

        public UserBuilder image(Image image) {
            this.image = image;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
