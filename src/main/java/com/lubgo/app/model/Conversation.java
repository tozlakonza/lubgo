package com.lubgo.app.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "CONVERSATIONS")
@NamedQueries({
    @NamedQuery(name = Conversation.FIND_BY_ID, query = "select c from Conversation c where c.id = :id"),
    @NamedQuery(name = Conversation.FIND_BY_USER_ID, query = "select c from Conversation c join c.participants p where p.id = :userId order by c.startDate desc")
})
public class Conversation extends AbstractEntity {

    public static final String FIND_BY_ID = "conversation.findById";
    public static final String FIND_BY_USER_ID = "conversation.findByUserId";

    private Timestamp startDate;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<User> participants;

    @OneToMany(mappedBy = "conversation", fetch = FetchType.EAGER)
    @OrderBy(value = "timestamp")
    private List<Message> messages;

    public Conversation() {}

    public Conversation(ConversationBuilder conversationBuilder) {
        this.startDate = conversationBuilder.startDate;
        this.participants = conversationBuilder.participants;
        this.messages = conversationBuilder.messages;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Set<User> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<User> participants) {
        this.participants = participants;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public static class ConversationBuilder {

        private Timestamp startDate;
        private Set<User> participants;
        private List<Message> messages;

        public ConversationBuilder() {}

        public ConversationBuilder startDate(Timestamp startDate) {
            this.startDate = startDate;
            return this;
        }

        public ConversationBuilder participants(Set<User> participants) {
            this.participants = participants;
            return this;
        }

        public ConversationBuilder messages(List<Message> messages) {
            this.messages = messages;
            return this;
        }

        public Conversation build() {
            return new Conversation(this);
        }

    }

}
