package com.lubgo.app.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "MESSAGES")
@NamedQueries({
    @NamedQuery(name = Message.FIND_BY_ID, query = "select m from Message m where m.id = :id"),
    @NamedQuery(name = Message.FIND_BY_CONVERSATION_ID, query = "select m from Message m where m.conversation.id = :conversationId order by m.timestamp desc")
})
public class Message extends AbstractEntity {

    public static final String FIND_BY_ID = "message.findById";
    public static final String FIND_BY_CONVERSATION_ID = "message.findByConversationId";

    private @Getter @Setter String body;
    private @Getter @Setter Timestamp timestamp;

    @ManyToOne
    private @Getter @Setter User sender;

    @ManyToOne
    private @Getter @Setter Conversation conversation;

    public Message() {

    }

    @Builder
    public Message(String body, Timestamp timestamp, User sender, Conversation conversation) {
        super();
        this.body = body;
        this.timestamp = timestamp;
        this.sender = sender;
        this.conversation = conversation;
    }

}
