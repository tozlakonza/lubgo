package com.lubgo.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "CONNECTION_REQUEST")
@NamedQueries({
    @NamedQuery(name = Connection.FIND_PENDING_BY_TO_USER_ID, query = "select cr from Connection cr where cr.to.id = :userId and cr.accepted is null"),
    @NamedQuery(name = Connection.FIND_ACCEPTED_BY_USER_ID, query = "select cr from Connection cr where ((cr.to.id = :userId and cr.from.id <> :userId) or (cr.to.id <> :userId and cr.from.id = :userId)) and cr.accepted = true"),
    @NamedQuery(name = Connection.FIND_DECLINED_BY_USER_ID, query = "select cr from Connection cr where (cr.to.id = :userId or cr.from.id = :userId) and cr.accepted = false"),
    @NamedQuery(name = Connection.FIND_PENDING_AND_ACCEPTED_COUNT, query = "select count (cr) from Connection cr where cr.from.id = :fromId and cr.to.id = :toId and (cr.accepted is null or cr.accepted = true) "),
    @NamedQuery(name = Connection.FIND_PENDING_BY_UUIDS, query = "select cr from Connection cr where cr.from.uuidStr = :fromUuid and cr.to.uuidStr = :toUuid and cr.accepted is null"),
    @NamedQuery(name = Connection.FIND_PENDING_BY_TO_USER_ID_AND_FROM_USER_ID, query = "select cr from Connection cr where cr.to.id = :toUserId and cr.from.id = :fromUserId and cr.accepted is null"),
    @NamedQuery(name = Connection.COUNT_ACCEPTED_BY_USER_ID, query = "select count(cr) from Connection cr where ((cr.to.id = :userId and cr.from.id <> :userId) or (cr.to.id <> :userId and cr.from.id = :userId)) and cr.accepted = true"),
    @NamedQuery(name = Connection.FIND_FRIENDS_FROM, query = "select f from Connection cr join cr.from f where cr.to.id = :userId and cr.accepted = true"),
    @NamedQuery(name = Connection.FIND_FRIENDS_TO, query = "select t from Connection cr join cr.to t where cr.from.id = :userId and cr.accepted = true")
})
public class Connection extends AbstractEntity {
    public static final String FIND_PENDING_BY_TO_USER_ID = "connectionRequest.findPendingByTargetUserId";
    public static final String FIND_PENDING_BY_TO_USER_ID_AND_FROM_USER_ID = "connectionRequest.findPendingByFAndToUserId";
    public static final String FIND_PENDING_AND_ACCEPTED_COUNT = "connectionRequest.findPendingAndAcceptedCount";
    public static final String FIND_PENDING_BY_UUIDS = "connectionRequest.findPendingByUuids";
    public static final String FIND_ACCEPTED_BY_USER_ID = "connectionRequest.findAcceptedByUserId";
    public static final String FIND_DECLINED_BY_USER_ID = "connectionRequest.findDeclinedByUserId";
    public static final String COUNT_ACCEPTED_BY_USER_ID = "connectionRequest.countAcceptedByUserId";
    public static final String FIND_FRIENDS_FROM = "connectionRequest.findFriendsFrom";
    public static final String FIND_FRIENDS_TO = "connectionRequest.findFriendsTo";

    @OneToOne
    private @Getter @Setter User from;

    @OneToOne
    private @Getter @Setter User to;

    private @Getter @Setter Date createdOn;

    private @Getter @Setter Date resolvedOn;

    /**
     * null - pending invitation
     */
    private @Getter @Setter Boolean accepted;

    public Connection() {

    }

}
