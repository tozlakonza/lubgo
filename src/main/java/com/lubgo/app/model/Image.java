package com.lubgo.app.model;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "IMAGES")
@NamedQueries({
    @NamedQuery(name = Image.FIND_BY_UUID, query = "select i from Image i where i.uuidStr = :uuid")
})
public class Image extends AbstractEntity {

    public static final String FIND_BY_UUID = "image.findByUUID";

    public static final int MAX_WIDTH = 768; // 960
    public static final int MAX_HEIGHT = 576; // 720

    public Image() {}

    public Image(String uuid) {
        setUuidStr(uuid);
    }

}
