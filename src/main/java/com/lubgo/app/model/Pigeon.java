package com.lubgo.app.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.lubgo.app.enums.Sex;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "PIGEONS")
@NamedQueries({
    @NamedQuery(name = Pigeon.FIND_BY_ID, query = "select p from Pigeon p where p.id = :id"),
    @NamedQuery(name = Pigeon.FIND_BY_UUID, query = "select p from Pigeon p where p.uuidStr = :uuid"),
    @NamedQuery(name = Pigeon.FIND_BY_USER_ID, query = "select p from Pigeon p where p.user.id = :userId"),
    @NamedQuery(name = Pigeon.COUNT_BY_OWNER_ID, query = "select count(p) from Pigeon p where p.user.id = :ownerId")
})
@ToString(exclude = { "achievements", "comments", "likes" })
public class Pigeon extends AbstractEntity {

    public static final String FIND_BY_ID = "pigeon.findById";
    public static final String FIND_BY_UUID = "pigeon.findByUUID";
    public static final String FIND_BY_USER_ID = "pigeon.findByUserId";
    public static final String COUNT_BY_OWNER_ID = "pigeon.countByAuthorId";

    private @Getter @Setter boolean mainstream; // maticni golub
    private @Getter @Setter Sex sex;
    private @Getter @Setter String name;
    private @Getter @Setter String breed; // rasa
    private @Getter @Setter String ring;
    private @Getter @Setter String owner; // vlasnik
    private @Getter @Setter String origin; // poreklo
    private @Getter @Setter String dynasty; // linija
    private @Getter @Setter Date dateOfBirth;
    private @Getter @Setter Date dateAcquired;
    private @Getter @Setter Date dateDisposedOf;

    @Lob
    private @Getter @Setter String description;

    @OneToOne
    private @Getter @Setter Image image;

    @ManyToOne
    private @Getter @Setter User user;

    @OneToMany(mappedBy = "pigeon", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private @Setter List<PigeonAchievement> achievements;

    @OneToMany(mappedBy = "pigeon", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private @Setter List<Post> comments = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER)
    private @Setter Set<User> likes = new HashSet<User>();

    public Pigeon() {

    }

    @Builder
    public Pigeon(boolean mainstream, Sex sex, String name, String breed, String ring, String owner, String origin, String dynasty, Date dateOfBirth,
        Date dateAcquired, Date dateDisposedOf, String description, Image image, User user, List<PigeonAchievement> achievements, List<Post> comments,
        Set<User> likes) {
        super();
        this.mainstream = mainstream;
        this.sex = sex;
        this.name = name;
        this.breed = breed;
        this.ring = ring;
        this.owner = owner;
        this.origin = origin;
        this.dynasty = dynasty;
        this.dateOfBirth = dateOfBirth;
        this.dateAcquired = dateAcquired;
        this.dateDisposedOf = dateDisposedOf;
        this.description = description;
        this.image = image;
        this.user = user;
        this.achievements = achievements;
        this.comments = comments;
        this.likes = likes;
    }

    public List<PigeonAchievement> getAchievements() {
        if (achievements == null) {
            achievements = new ArrayList<PigeonAchievement>();
        }
        return achievements;
    }

    public List<Post> getComments() {
        if (comments == null) {
            comments = new ArrayList<Post>();
        }
        return comments;
    }

    public Set<User> getLikes() {
        if (likes == null) {
            likes = new HashSet<User>();
        }
        return likes;
    }

}
