package com.lubgo.app.model;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "POSTS")
@NamedQueries({
    @NamedQuery(name = Post.FIND_BY_USER, query = "select p from Post p where p.user.id = :userId and p.parent is null and type = 'Post' and article.id is null and pigeon.id is null order by p.date desc, p.time desc"),
    @NamedQuery(name = Post.FIND_FEED_BY_USER, query = "select p from Post p where p.user.id <> :userId and p.parent is null and type = 'Post' and article.id is null and pigeon.id is null order by p.date desc, p.time desc")
})
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public class Post extends AbstractEntity {

    public static final String FIND_BY_USER = "post.findByUser";
    public static final String FIND_FEED_BY_USER = "post.findFeedByUser";

    @ManyToOne
    private User user;

    private Date date;
    private Time time;
    private String description;

    @ManyToOne
    private Post parent;

    @ManyToOne
    private Pigeon pigeon;

    @ManyToOne
    private Article article;

    @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Post> children = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<User> likes = new HashSet<User>();

    @OneToOne(cascade = CascadeType.REMOVE)
    private Link link;

    @OneToOne(cascade = CascadeType.REMOVE)
    private Image image;

    public Post() {}

    public Post(User user, Date date, Time time, String description, Link link, Image image) {
        this.user = user;
        this.date = date;
        this.time = time;
        this.description = description;
        this.link = link;
        this.image = image;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getParent() {
        return parent;
    }

    public void setParent(Post parent) {
        this.parent = parent;
    }

    public Pigeon getPigeon() {
        return pigeon;
    }

    public void setPigeon(Pigeon pigeon) {
        this.pigeon = pigeon;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public List<Post> getChildren() {
        return children;
    }

    public void setChildren(List<Post> children) {
        this.children = children;
    }

    public Set<User> getLikes() {
        return likes;
    }

    public void setLikes(Set<User> likes) {
        this.likes = likes;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
