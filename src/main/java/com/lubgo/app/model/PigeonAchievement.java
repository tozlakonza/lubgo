package com.lubgo.app.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.lubgo.app.enums.PigeonAchivementType;

@Entity
@Table(name = "PIGEON_ACHIEVEMENT")
@NamedQueries({
    @NamedQuery(name = PigeonAchievement.FIND_BY_ID, query = "select pr from PigeonAchievement pr where pr.id = :id"),
    @NamedQuery(name = PigeonAchievement.FIND_BY_PIGEON_ID, query = "select pr from PigeonAchievement pr where pr.pigeon.id = :pigeonId")
})
public class PigeonAchievement extends AbstractEntity {

    public static final String FIND_BY_ID = "pigeonResult.findById";
    public static final String FIND_BY_PIGEON_ID = "pigeonResult.findByPigeonId";

    private PigeonAchivementType type;
    private Integer place;
    private String race;
    private Integer birds;
    private Integer year;
    private String description;

    @ManyToOne
    private Pigeon pigeon;

    public PigeonAchievement() {}

    public PigeonAchievement(PigeonAchievementBuilder pigeonAchivementBuilder) {
        this.type = pigeonAchivementBuilder.type;
        this.place = pigeonAchivementBuilder.place;
        this.race = pigeonAchivementBuilder.race;
        this.birds = pigeonAchivementBuilder.birds;
        this.year = pigeonAchivementBuilder.year;
        this.description = pigeonAchivementBuilder.description;
        this.pigeon = pigeonAchivementBuilder.pigeon;
    }

    public PigeonAchivementType getType() {
        return type;
    }

    public void setType(PigeonAchivementType type) {
        this.type = type;
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Integer getBirds() {
        return birds;
    }

    public void setBirds(Integer birds) {
        this.birds = birds;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Pigeon getPigeon() {
        return pigeon;
    }

    public void setPigeon(Pigeon pigeon) {
        this.pigeon = pigeon;
    }

    public static class PigeonAchievementBuilder {

        private PigeonAchivementType type;
        private Integer place;
        private String race;
        private Integer birds;
        private Integer year;
        private String description;

        private Pigeon pigeon;

        public PigeonAchievementBuilder() {}

        public PigeonAchievementBuilder type(PigeonAchivementType type) {
            this.type = type;
            return this;
        }

        public PigeonAchievementBuilder place(Integer place) {
            this.place = place;
            return this;
        }

        public PigeonAchievementBuilder race(String race) {
            this.race = race;
            return this;
        }

        public PigeonAchievementBuilder birds(Integer birds) {
            this.birds = birds;
            return this;
        }

        public PigeonAchievementBuilder year(Integer year) {
            this.year = year;
            return this;
        }

        public PigeonAchievementBuilder description(String description) {
            this.description = description;
            return this;
        }

        public PigeonAchievementBuilder pigeon(Pigeon pigeon) {
            this.pigeon = pigeon;
            return this;
        }

        public PigeonAchievement build() {
            return new PigeonAchievement(this);
        }

    }

}
