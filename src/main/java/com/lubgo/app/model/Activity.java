package com.lubgo.app.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.lubgo.app.enums.ActivityType;

@Entity
@Table(name = "ACTIVITIES")
@NamedQueries({
    @NamedQuery(name = Activity.FIND_BY_PERFORMER_ID, query = "select a from Activity a where a.performer.id = :id"),
    @NamedQuery(name = Activity.FIND_BY_TARGET_ID, query = "select a from Activity a where a.target.id = :id"),
    @NamedQuery(name = Activity.FIND_NEW_FOR_USER, query = "select a from Activity a where a.notify like :userIdPiped and a.notified not like :userIdPiped order by a.id desc"),
    @NamedQuery(name = Activity.FIND_ALL_FOR_USER, query = "select a from Activity a where a.notify like :userIdPiped order by a.id desc"),
    @NamedQuery(name = Activity.FIND_BY_POST_ID, query = "select a from Activity a where a.post.id = :postId or a.post.parent.id = :postId"),
    @NamedQuery(name = Activity.FIND_BY_PIGEON_ID, query = "select a from Activity a where a.pigeon.id = :pigeonId"),
    @NamedQuery(name = Activity.FIND_BY_ARTICLE_ID, query = "select a from Activity a where a.article.id = :articleId"),
    @NamedQuery(name = Activity.FIND_BY_PARAMS, query = "select a from Activity a where a.type = :type and a.connection.from.id = :fromId and a.connection.to.id = :toId")
})
public class Activity extends AbstractEntity {

    public static final String FIND_BY_PERFORMER_ID = "activity.findByPerformerId";
    public static final String FIND_BY_TARGET_ID = "activity.findByTargetId";
    public static final String FIND_NEW_FOR_USER = "activity.findNewForUser";
    public static final String FIND_BY_PARAMS = "activity.findByParams";
    public static final String FIND_ALL_FOR_USER = "activity.findAllForUser";
    public static final String FIND_BY_POST_ID = "activity.findByPostId";
    public static final String FIND_BY_PIGEON_ID = "activity.findByPigeonId";
    public static final String FIND_BY_ARTICLE_ID = "activity.findByArticleId";

    @ManyToOne
    private User performer;

    private ActivityType type;

    @OneToOne
    private User target;

    @ManyToOne
    private Post post;

    @ManyToOne
    private Pigeon pigeon;

    @ManyToOne
    private Article article;

    @OneToOne
    private Connection connection;

    /* ids of users to notify of this activity */
    private String notify;

    /* ids of notified users */
    private String notified;

    public Activity() {}

    public Activity(User performer, ActivityType type, Long objectId) {
        this.performer = performer;
        this.type = type;
    }

    public User getPerformer() {
        return performer;
    }

    public void setPerformer(User performer) {
        this.performer = performer;
    }

    public ActivityType getType() {
        return type;
    }

    public void setType(ActivityType type) {
        this.type = type;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public String getNotify() {
        return notify;
    }

    public void setNotify(String notify) {
        this.notify = notify;
    }

    public String getNotified() {
        return notified;
    }

    public void setNotified(String notified) {
        this.notified = notified;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}