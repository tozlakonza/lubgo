package com.lubgo.app.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "LINKS")
public class Link extends AbstractEntity {

    private @Getter @Setter String title;
    private @Getter @Setter String description;
    private @Getter @Setter String url;

    @OneToOne(cascade = CascadeType.REMOVE)
    private @Getter @Setter Image image;

    @OneToOne
    private @Getter @Setter Post post;

    public Link() {

    }

    @Builder
    public Link(String title, String description, String url, Image image, Post post) {
        super();
        this.title = title;
        this.description = description;
        this.url = url;
        this.image = image;
        this.post = post;
    }

}
