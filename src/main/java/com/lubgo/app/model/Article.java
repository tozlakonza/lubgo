package com.lubgo.app.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ARTICLES")
@NamedQueries({
    @NamedQuery(name = Article.FIND_BY_ID, query = "select a from Article a where a.id = :id and type = 'Article'"),
    @NamedQuery(name = Article.FIND_BY_UUID, query = "select a from Article a where a.uuidStr = :uuid and type = 'Article'"),
    @NamedQuery(name = Article.FIND_BY_AUTHOR_ID, query = "select a from Article a where a.user.id = :id and type = 'Article'"),
    @NamedQuery(name = Article.COUNT_BY_AUTHOR_ID, query = "select count(a) from Article a where a.user.id = :id and type = 'Article'")
})
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public class Article extends Post {

    public static final String FIND_BY_ID = "article.findById";
    public static final String FIND_BY_UUID = "article.findByUUID";
    public static final String FIND_BY_AUTHOR_ID = "article.findByAuthorId";
    public static final String COUNT_BY_AUTHOR_ID = "article.countByAuthorId";

    private String title;

    @Lob
    private String body;

    @OneToMany(mappedBy = "article", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Post> comments = new ArrayList<>();

    public Article() {}

    public Article(String title, String body, User author, Date date) {
        super(author, date, null, null, null, null);
        this.title = title;
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getAuthor() {
        return getUser();
    }

    public void setAuthor(User author) {
        this.setUser(author);
    }

    public List<Post> getComments() {
        return comments;
    }

    public void setComments(List<Post> comments) {
        this.comments = comments;
    }

}