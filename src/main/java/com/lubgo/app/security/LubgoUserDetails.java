package com.lubgo.app.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class LubgoUserDetails extends User {

    private static final long serialVersionUID = -3444245092193653985L;

    private String uuid;

    public LubgoUserDetails(String username, String password, String uuid, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
