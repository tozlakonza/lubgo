package com.lubgo.app.enums;

public enum ConnectionStatus {
                              AVAILABLE,
                              CONNECTED,
                              PENDING,
                              AWAITING_ACCEPTANCE,
                              NOT_APPLICABLE
}
