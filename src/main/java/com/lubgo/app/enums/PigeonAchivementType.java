package com.lubgo.app.enums;

public enum PigeonAchivementType {

    PRIZE,

    RESULT,

    TITLE

}
