package com.lubgo.app.init;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lubgo.app.enums.PigeonAchivementType;
import com.lubgo.app.enums.Sex;
import com.lubgo.app.model.Article;
import com.lubgo.app.model.Connection;
import com.lubgo.app.model.Conversation;
import com.lubgo.app.model.Image;
import com.lubgo.app.model.Message;
import com.lubgo.app.model.Pigeon;
import com.lubgo.app.model.PigeonAchievement;
import com.lubgo.app.model.PigeonAchievement.PigeonAchievementBuilder;
import com.lubgo.app.model.Post;
import com.lubgo.app.model.User;
import com.lubgo.app.model.User.UserBuilder;

/**
 *
 * This is a initializing bean that inserts some test data in the database. It
 * is only active in the development profile, to see the data login with
 * test@email.com / Password2
 *
 */
@Component
public class TestDataInitializer {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public void init() throws Exception {

        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        // IMAGES
        Image noImage = new Image();
        noImage.setUuidStr("no-image");
        Image image1 = new Image();
        image1.setUuidStr("user8-128x128");
        Image image2 = new Image();
        image2.setUuidStr("user2-160x160");
        Image image3 = new Image();
        image3.setUuidStr("user3-128x128");
        Image image4 = new Image();
        image4.setUuidStr("user4-128x128");
        Image image5 = new Image();
        image5.setUuidStr("user5-128x128");

        session.persist(noImage);
        session.persist(image1);
        session.persist(image2);
        session.persist(image3);
        session.persist(image4);
        session.persist(image5);

        // USERS
        User user1 = new UserBuilder().email("test@email.com")
                                      .passwordDigest("$2a$10$x9vXeDsSC2109FZfIJz.pOZ4dJ056xBpbesuMJg3jZ.ThQkV119tS")
                                      .firstName("Rock")
                                      .lastName("Doe")
                                      .phone("+381 11 256 44 44")
                                      .location("Belgrade, Serbia")
                                      .image(image1)
                                      .build();

        User user2 = new UserBuilder().email("test2@email.com")
                                      .passwordDigest("$2a$10$x9vXeDsSC2109FZfIJz.pOZ4dJ056xBpbesuMJg3jZ.ThQkV119tS")
                                      .firstName("Steven")
                                      .lastName("Hawking")
                                      .image(image2)
                                      .build();

        User user3 = new UserBuilder().email("john@email.com")
                                      .passwordDigest("$2a$10$x9vXeDsSC2109FZfIJz.pOZ4dJ056xBpbesuMJg3jZ.ThQkV119tS")
                                      .firstName("John")
                                      .lastName("Locke")
                                      .image(image3)
                                      .build();

        User user4 = new UserBuilder().email("rilic@email.com")
                                      .passwordDigest("$2a$10$x9vXeDsSC2109FZfIJz.pOZ4dJ056xBpbesuMJg3jZ.ThQkV119tS")
                                      .firstName("Risto")
                                      .lastName("Ilic")
                                      .phone("+381 11 000 00 00")
                                      .alliance("SRB")
                                      .club("52 - Staro Zarkovo")
                                      .location("Belgrade, Serbia")
                                      .birthday(new GregorianCalendar(1960 + 1900, 9, 21).getTime())
                                      .autobiography("Strast za golubovima stekao jos davne 1980. uz oca Miroslava.")
                                      .image(image4)
                                      .build();

        User user5 = new UserBuilder().email("john5@email.com")
                                      .passwordDigest("$2a$10$x9vXeDsSC2109FZfIJz.pOZ4dJ056xBpbesuMJg3jZ.ThQkV119tS")
                                      .firstName("John5")
                                      .lastName("Locke5")
                                      .image(image5)
                                      .build();

        session.persist(user1);
        session.persist(user2);
        session.persist(user3);
        session.persist(user4);
        session.persist(user5);

        for (int i = 0; i < 20; i++) {
            User userI = new UserBuilder().email("userI" + i + "@email.com")
                                          .passwordDigest("$2a$10$x9vXeDsSC2109FZfIJz.pOZ4dJ056xBpbesuMJg3jZ.ThQkV119tS")
                                          .firstName("UserI" + i)
                                          .lastName("Doe")
                                          .phone("+381 11 256 44 44")
                                          .location("Belgrade, Serbia")
                                          .image(image1)
                                          .build();
            session.persist(userI);

        }

        Connection connectionRequest = new Connection();
        connectionRequest.setFrom(user1);
        connectionRequest.setTo(user2);
        connectionRequest.setAccepted(true);
        connectionRequest.setCreatedOn(new Date());
        connectionRequest.setResolvedOn(new Date());

        session.persist(connectionRequest);

        Pigeon pigeon = Pigeon.builder()
                              .breed("Arap 1")
                              .name("Lena 1")
                              .sex(Sex.COCK)
                              .ring("RS-212-21")
                              .dateAcquired(new Date())
                              .dateOfBirth(new Date())
                              .description("Sta god 1")
                              .image(noImage)
                              .user(user1)
                              .build();

        Pigeon pigeon2 = Pigeon.builder()
                               .breed("Pavian 2")
                               .name("Lena 2")
                               .sex(Sex.COCK)
                               .ring("RS-209-IR")
                               .dateAcquired(new Date())
                               .dateOfBirth(new Date())
                               .description("Sta god 2")
                               .image(noImage)
                               .user(user1)
                               .build();

        Pigeon pigeon3 = Pigeon.builder()
                               .breed("Tipler 3")
                               .name("Lena 3")
                               .sex(Sex.HEN)
                               .ring("RS-217-21")
                               .dateAcquired(new Date())
                               .dateOfBirth(new Date())
                               .description("Sta god 3")
                               .image(noImage)
                               .user(user1)
                               .build();

        session.persist(pigeon);
        session.persist(pigeon2);
        session.persist(pigeon3);

        for (int i = 0; i < 20; i++) {
            Pigeon pigeonI = Pigeon.builder()
                                   .breed("Tipler " + i)
                                   .name("Lena " + i)
                                   .sex(Sex.HEN)
                                   .ring("RS-217-" + i)
                                   .dateAcquired(new Date())
                                   .dateOfBirth(new Date())
                                   .description("Sta god " + i)
                                   .image(noImage)
                                   .user(user1)
                                   .build();
            session.persist(pigeonI);

        }

        PigeonAchievement pigeonAchivement1 = new PigeonAchievementBuilder().type(PigeonAchivementType.PRIZE)
                                                                            .race("Velika trka 1")
                                                                            .description("Bleja trka 1")
                                                                            .year(2015)
                                                                            .place(20)
                                                                            .birds(1212)
                                                                            .pigeon(pigeon)
                                                                            .build();

        PigeonAchievement pigeonAchivement2 = new PigeonAchievementBuilder().type(PigeonAchivementType.RESULT)
                                                                            .race("Velika trka 2")
                                                                            .description("Bleja trka 2")
                                                                            .year(2016)
                                                                            .place(24)
                                                                            .birds(1212)
                                                                            .pigeon(pigeon)
                                                                            .build();

        PigeonAchievement pigeonAchivement3 = new PigeonAchievementBuilder().type(PigeonAchivementType.TITLE)
                                                                            .race("Velika trka 3")
                                                                            .description("Bleja trka 3")
                                                                            .year(2017)
                                                                            .place(21)
                                                                            .birds(1212)
                                                                            .pigeon(pigeon)
                                                                            .build();

        List<PigeonAchievement> achivements = new ArrayList<PigeonAchievement>();
        achivements.add(pigeonAchivement1);
        achivements.add(pigeonAchivement2);
        achivements.add(pigeonAchivement3);

        pigeon.setAchievements(achivements);

        session.update(pigeon);

        /// CONVERSATIONS

        Conversation conversation1 = new Conversation();
        Set<User> participants = new java.util.HashSet<User>();
        participants.add(user1);
        participants.add(user2);
        participants.add(user3);
        participants.add(user4);
        conversation1.setParticipants(participants);
        conversation1.setStartDate(new Timestamp(System.currentTimeMillis()));

        Conversation conversation2 = new Conversation();
        participants = new java.util.HashSet<User>();
        participants.add(user1);
        participants.add(user2);
        conversation2.setParticipants(participants);
        conversation2.setStartDate(new Timestamp(System.currentTimeMillis()));

        Conversation conversation3 = new Conversation();
        participants = new java.util.HashSet<User>();
        participants.add(user1);
        participants.add(user2);
        participants.add(user3);
        conversation3.setParticipants(participants);
        conversation3.setStartDate(new Timestamp(System.currentTimeMillis()));

        Conversation conversation4 = new Conversation();
        participants = new java.util.HashSet<User>();
        participants.add(user1);
        participants.add(user2);
        participants.add(user3);
        participants.add(user4);
        participants.add(user5);
        conversation4.setParticipants(participants);
        conversation4.setStartDate(new Timestamp(System.currentTimeMillis()));

        session.persist(conversation1);
        session.persist(conversation2);
        session.persist(conversation3);
        session.persist(conversation4);

        Message message = Message.builder()
                                 .body("E cao 2 3")
                                 .sender(user1)
                                 .conversation(conversation1)
                                 .timestamp(new Timestamp(System.currentTimeMillis()))
                                 .build();
        Message message2 = Message.builder()
                                  .body("E cao 1 3")
                                  .sender(user2)
                                  .conversation(conversation1)
                                  .timestamp(new Timestamp(System.currentTimeMillis()))
                                  .build();
        Message message3 = Message.builder()
                                  .body("E cao 1 2")
                                  .sender(user3)
                                  .conversation(conversation1)
                                  .timestamp(new Timestamp(System.currentTimeMillis()))
                                  .build();

        Message message4 = Message.builder()
                                  .body("E cao 2")
                                  .sender(user1)
                                  .conversation(conversation2)
                                  .timestamp(new Timestamp(System.currentTimeMillis()))
                                  .build();
        Message message5 = Message.builder()
                                  .body("E cao 1")
                                  .sender(user2)
                                  .conversation(conversation2)
                                  .timestamp(new Timestamp(System.currentTimeMillis()))
                                  .build();
        Message message6 = Message.builder()
                                  .body("E cao 2 3")
                                  .sender(user1)
                                  .conversation(conversation3)
                                  .timestamp(new Timestamp(System.currentTimeMillis()))
                                  .build();

        Message message7 = Message.builder()
                                  .body("E cao 1")
                                  .sender(user5)
                                  .conversation(conversation4)
                                  .timestamp(new Timestamp(System.currentTimeMillis()))
                                  .build();

        session.persist(message);
        session.persist(message2);
        session.persist(message3);
        session.persist(message4);
        session.persist(message5);
        session.persist(message6);
        session.persist(message7);

        Post post1 = new Post(user1, new Date(), null, "Example post", null, null);
        Post post2 = new Post(user1, new Date(), null, "Example post", null, null);

        session.persist(post1);
        session.persist(post2);

        Article article1 = new Article("Title of Article", "Test article", user1, new Date());
        Article article2 = new Article("Title of Article", "Test article", user2, new Date());
        Article article3 = new Article("Title of Article", "Test article", user3, new Date());

        session.persist(article1);
        session.persist(article2);
        session.persist(article3);

        for (int i = 0; i < 20; i++) {
            Article articleI = new Article("Title of Article " + i, "Test article " + i, user1, new Date());
            session.persist(articleI);
        }

        List<Post> comments = new ArrayList<Post>();

        Post articleComment = new Post(user2, new Date(), null, "Example post", null, null);
        articleComment.setArticle(article1);
        comments.add(articleComment);

        article1.setComments(comments);

        session.persist(articleComment);
        session.persist(article1);

        transaction.commit();
    }
}
