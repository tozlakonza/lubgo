package com.lubgo.app.dto;

import java.util.List;

public class PigeonsDTO {

    List<PigeonDTO> pigeons;

    public PigeonsDTO(List<PigeonDTO> pigeons) {
        this.pigeons = pigeons;
    }

    public List<PigeonDTO> getpigeons() {
        return pigeons;
    }

    public void setPigeons(List<PigeonDTO> pigeons) {
        this.pigeons = pigeons;
    }
}