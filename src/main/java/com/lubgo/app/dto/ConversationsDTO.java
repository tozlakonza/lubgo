package com.lubgo.app.dto;

import java.util.List;

public class ConversationsDTO {

    List<ConversationDTO> conversations;

    public ConversationsDTO(List<ConversationDTO> conversations) {
        this.conversations = conversations;
    }

    public List<ConversationDTO> getConversations() {
        return conversations;
    }

    public void setConversations(List<ConversationDTO> conversations) {
        this.conversations = conversations;
    }

}