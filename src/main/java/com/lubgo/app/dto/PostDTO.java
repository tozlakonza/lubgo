package com.lubgo.app.dto;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lubgo.app.dto.serialization.CustomTimeDeserializer;
import com.lubgo.app.dto.serialization.CustomTimeSerializer;
import com.lubgo.app.model.Image;
import com.lubgo.app.model.Link;
import com.lubgo.app.model.Post;
import com.lubgo.app.model.User;

/**
 *
 * JSON serializable DTO containing Post data
 * k home
 *
 */
public class PostDTO {

    private Long id;

    @JsonFormat(pattern = "yyyy/MM/dd", timezone = "CET")
    private Date date;

    @JsonSerialize(using = CustomTimeSerializer.class)
    @JsonDeserialize(using = CustomTimeDeserializer.class)
    private Time time;

    private String description;

    private UserInfoDTO user;

    private List<PostDTO> children;

    private Set<UserBasicDTO> likes;

    private LinkDTO link;
    private ImageDTO image;

    public PostDTO() {}

    public PostDTO(Long id, Date date, Time time, String description, User user, List<Post> children, Set<User> likes, Link link, Image image) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.description = description;
        if (children != null) {
            for (Post child : children) {
                getChildren().add(new PostDTO(child.getId(),
                                              child.getDate(),
                                              child.getTime(),
                                              child.getDescription(),
                                              child.getUser(),
                                              child.getChildren(),
                                              child.getLikes(),
                                              null,
                                              null));

            }
        }
        if (likes != null) {
            for (User like : likes) {
                getLikes().add(UserBasicDTO.mapFromUserEntity(like));
            }
        }
        this.user = new UserInfoDTO(user.getEmail(), user.getFirstName(), user.getLastName(), user.getPhone(), user.getUuidStr(), user.getImage());
        if (link != null) {
            this.link = new LinkDTO(link.getTitle(), link.getDescription(), link.getUrl(), link.getImage());
        }

        this.image = (image != null) ? new ImageDTO(image.getUuidStr()) : new ImageDTO(null);
    }

    public static PostDTO mapFromPostEntity(Post post) {
        return new PostDTO(post.getId(),
                           post.getDate(),
                           post.getTime(),
                           post.getDescription(),
                           post.getUser(),
                           post.getChildren(),
                           post.getLikes(),
                           post.getLink(),
                           post.getImage());
    }

    public static List<PostDTO> mapFromPostsEntities(List<Post> posts) {
        return posts.stream().map((post) -> mapFromPostEntity(post)).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PostDTO> getChildren() {
        if (children == null) {
            children = new ArrayList<>();
        }
        return children;
    }

    public void setChildren(List<PostDTO> children) {
        this.children = children;
    }

    public UserInfoDTO getUser() {
        return user;
    }

    public void setUser(UserInfoDTO user) {
        this.user = user;
    }

    public Set<UserBasicDTO> getLikes() {
        if (likes == null) {
            likes = new HashSet<UserBasicDTO>();
        }
        return likes;
    }

    public void setLikes(Set<UserBasicDTO> likes) {
        this.likes = likes;
    }

    public LinkDTO getLink() {
        return link;
    }

    public void setLink(LinkDTO link) {
        this.link = link;
    }

    public ImageDTO getImage() {
        return image;
    }

    public void setImage(ImageDTO image) {
        this.image = image;
    }

}
