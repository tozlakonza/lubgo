package com.lubgo.app.dto;

import java.util.Date;
import java.util.List;

import com.lubgo.app.model.Message;
import com.lubgo.app.model.User;

import lombok.Getter;
import lombok.Setter;

public class MessageDTO {

    private @Getter @Setter Long id;
    private @Getter @Setter Long conversationId;
    private @Getter @Setter String body;
    private @Getter @Setter Date time;
    private @Getter @Setter UserBasicDTO sender;
    private @Getter @Setter List<UserBasicDTO> messageRecepients;

    public MessageDTO() {}

    public MessageDTO(Long id, String body, Date time, User sender) {
        this.id = id;
        this.body = body;
        this.time = time;
        this.sender = UserBasicDTO.mapFromUserEntity(sender);
    }

    public static MessageDTO mapFromMessageEntity(Message message) {
        return new MessageDTO(message.getConversation().getId(), message.getBody(), message.getTimestamp(), message.getSender());
    }

}
