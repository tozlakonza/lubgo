package com.lubgo.app.dto;

import java.util.List;

/**
 *
 * JSON serializable DTO containing data concerning a post search request.
 *
 */
public class UsersBasicDTO {

    List<UserBasicDTO> users;

    public UsersBasicDTO(List<UserBasicDTO> users) {
        this.users = users;
    }

    public List<UserBasicDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserBasicDTO> users) {
        this.users = users;
    }

}