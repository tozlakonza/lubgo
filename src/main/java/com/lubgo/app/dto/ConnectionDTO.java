package com.lubgo.app.dto;

import java.util.Date;

import com.lubgo.app.model.Connection;
import com.lubgo.app.model.User;

public class ConnectionDTO {

    private UserBasicDTO user;
    private Date resolvedOn;

    public ConnectionDTO(UserBasicDTO user, Date resolvedOn) {
        this.user = user;
        this.resolvedOn = resolvedOn;
    }

    public static ConnectionDTO mapFromConnectionEntity(User user, Connection connection) {
        return new ConnectionDTO(UserBasicDTO.mapFromUserEntity(user), connection.getResolvedOn());
    }

    public UserBasicDTO getUser() {
        return user;
    }

    public void setUser(UserBasicDTO user) {
        this.user = user;
    }

    public Date getResolvedOn() {
        return resolvedOn;
    }

    public void setResolvedOn(Date resolvedOn) {
        this.resolvedOn = resolvedOn;
    }

}
