package com.lubgo.app.dto;

import java.util.List;

public class MessagesDTO {

    List<MessageDTO> messages;

    public MessagesDTO(List<MessageDTO> messages) {
        this.messages = messages;
    }

    public List<MessageDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageDTO> messages) {
        this.messages = messages;
    }

}