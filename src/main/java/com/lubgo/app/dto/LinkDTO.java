package com.lubgo.app.dto;

import com.lubgo.app.model.Image;
import com.lubgo.app.model.Link;

public class LinkDTO {

    private String title;
    private String description;
    private String url;
    private ImageDTO image;

    public LinkDTO() {

    }

    public LinkDTO(String title, String description, String url, Image image) {
        this.title = title;
        this.description = description;
        this.url = url;
        this.image = (image != null) ? new ImageDTO(image.getUuidStr()) : new ImageDTO(null);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static LinkDTO mapFromLinkEntity(Link link) {
        return new LinkDTO(link.getTitle(), link.getDescription(), link.getUrl(), link.getImage());
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ImageDTO getImage() {
        return image;
    }

    public void setImage(ImageDTO image) {
        this.image = image;
    }

}
