package com.lubgo.app.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lubgo.app.enums.Sex;
import com.lubgo.app.model.Image;
import com.lubgo.app.model.Pigeon;
import com.lubgo.app.model.PigeonAchievement;
import com.lubgo.app.model.Post;
import com.lubgo.app.model.User;

import lombok.Getter;
import lombok.Setter;

public class PigeonDTO {

    private @Getter @Setter Long id;
    private @Getter @Setter String uuid;

    private @Getter @Setter boolean mainstream;
    private @Getter @Setter Sex sex;
    private @Getter @Setter String name;
    private @Getter @Setter String breed;
    private @Getter @Setter String ring;
    private @Getter @Setter String owner;
    private @Getter @Setter String origin;
    private @Getter @Setter String dynasty;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "CET")
    private @Getter @Setter Date dateOfBirth;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "CET")
    private @Getter @Setter Date dateAcquired;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "CET")
    private @Getter @Setter Date dateDisposedOf;

    private @Getter @Setter String description;

    private @Getter @Setter ImageDTO image;
    private @Getter @Setter UserInfoDTO user;

    private @Setter List<PigeonAchievementDTO> achievements;

    private @Setter Map<String, List<PigeonAchievementDTO>> achievementsMap;

    private @Setter Set<UserBasicDTO> likes;

    private @Setter List<PostDTO> comments;

    public PigeonDTO() {

    }

    public PigeonDTO(Long id, String uuid, boolean mainstream, Sex sex, String name, String breed, String ring, String owner, String origin,
        String dynasty,
        Date dateOfBirth, Date dateAcquired, Date dateDisposedOf, String description, User user, Image image,
        List<PigeonAchievement> achievements, Set<User> likes, List<Post> comments) {

        this.id = id;
        this.uuid = uuid;
        this.mainstream = mainstream;
        this.sex = sex;
        this.name = name;
        this.breed = breed;
        this.ring = ring;
        this.owner = owner;
        this.origin = origin;
        this.dynasty = dynasty;
        this.dateOfBirth = dateOfBirth;
        this.dateAcquired = dateAcquired;
        this.dateDisposedOf = dateDisposedOf;
        this.description = description;
        this.user = new UserInfoDTO(user.getEmail(), user.getFirstName(), user.getLastName(), user.getPhone(), user.getUuidStr(), user.getImage());
        this.image = ImageDTO.mapFromImageEntity(image);

        for (PigeonAchievement pigeonAchievement : achievements) {
            getAchievements().add(PigeonAchievementDTO.mapFromPigeonAchievementEntity(pigeonAchievement));
        }

        if (getAchievements() != null) {
            for (PigeonAchievementDTO pigeonAchievementDTO : getAchievements()) {
                List<PigeonAchievementDTO> achievementDTOs = getAchievementsMap().get(pigeonAchievementDTO.getType());
                if (achievementDTOs == null) {
                    achievementDTOs = new ArrayList<PigeonAchievementDTO>();
                    achievementDTOs.add(pigeonAchievementDTO);
                    getAchievementsMap().put(pigeonAchievementDTO.getType(), achievementDTOs);
                }
                else {
                    achievementDTOs.add(pigeonAchievementDTO);
                }
            }
        }

        for (User like : likes) {
            getLikes().add(UserBasicDTO.mapFromUserEntity(like));
        }

        for (Post comment : comments) {
            PostDTO commentDTO = new PostDTO(comment.getId(),
                                             comment.getDate(),
                                             comment.getTime(),
                                             comment.getDescription(),
                                             comment.getUser(),
                                             null,
                                             null,
                                             null,
                                             null);
            getComments().add(commentDTO);
        }
    }

    public static PigeonDTO mapFromPigeonEntity(Pigeon pigeon) {
        return new PigeonDTO(pigeon.getId(),
                             pigeon.getUuidStr(),
                             pigeon.isMainstream(),
                             pigeon.getSex(),
                             pigeon.getName(),
                             pigeon.getBreed(),
                             pigeon.getRing(),
                             pigeon.getOwner(),
                             pigeon.getOrigin(),
                             pigeon.getDynasty(),
                             pigeon.getDateOfBirth(),
                             pigeon.getDateAcquired(),
                             pigeon.getDateDisposedOf(),
                             pigeon.getDescription(),
                             pigeon.getUser(),
                             pigeon.getImage(),
                             pigeon.getAchievements(),
                             pigeon.getLikes(),
                             pigeon.getComments());
    }

    public static List<PigeonDTO> mapFromPigeonsEntities(List<Pigeon> pigeons) {
        return pigeons.stream().map((pigeon) -> mapFromPigeonEntity(pigeon)).collect(Collectors.toList());
    }

    public List<PigeonAchievementDTO> getAchievements() {
        if (achievements == null) {
            achievements = new ArrayList<PigeonAchievementDTO>();
        }
        return achievements;
    }

    public Map<String, List<PigeonAchievementDTO>> getAchievementsMap() {
        if (achievementsMap == null) {
            achievementsMap = new HashMap<String, List<PigeonAchievementDTO>>();
        }
        return achievementsMap;
    }

    public Set<UserBasicDTO> getLikes() {
        if (likes == null) {
            likes = new HashSet<UserBasicDTO>();
        }
        return likes;
    }

    public List<PostDTO> getComments() {
        if (comments == null) {
            comments = new ArrayList<PostDTO>();
        }
        return comments;
    }

}
