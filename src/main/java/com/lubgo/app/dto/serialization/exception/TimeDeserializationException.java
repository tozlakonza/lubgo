package com.lubgo.app.dto.serialization.exception;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 *
 * Custom exception thrown when it was not possible to deserialize a time field,
 * 
 * @see com.lubgo.app.dto.serialization.CustomTimeDeserializer
 *
 */
public class TimeDeserializationException extends JsonProcessingException {

    private static final long serialVersionUID = 4960704779151882065L;

    public TimeDeserializationException(Throwable rootCause) {
        super(rootCause);
    }

}
