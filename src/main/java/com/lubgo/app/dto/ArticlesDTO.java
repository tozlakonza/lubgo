package com.lubgo.app.dto;

import java.util.List;

/**
 *
 * JSON serializable DTO containing data concerning a post search request.
 *
 */
public class ArticlesDTO {

    List<ArticleDTO> articles;

    public ArticlesDTO(List<ArticleDTO> articles) {
        this.articles = articles;
    }

    public List<ArticleDTO> getArticles() {
        return articles;
    }

    public void setPosts(List<ArticleDTO> articles) {
        this.articles = articles;
    }
}