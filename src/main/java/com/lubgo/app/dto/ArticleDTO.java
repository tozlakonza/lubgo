package com.lubgo.app.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lubgo.app.model.Article;
import com.lubgo.app.model.Post;
import com.lubgo.app.model.User;

public class ArticleDTO {

    private Long id;

    private String uuid;

    private String title;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "CET")
    private Date date;

    private String body;

    private UserInfoDTO author;

    private List<PostDTO> comments;

    private Set<UserBasicDTO> likes;

    public ArticleDTO() {}

    public ArticleDTO(Long id, String uuid, String title, String body, Date date, User author, List<Post> comments, Set<User> likes) {

        this.id = id;
        this.uuid = uuid;
        this.title = title;
        this.body = body;
        this.date = date;
        this.author = new UserInfoDTO(author.getEmail(),
                                      author.getFirstName(),
                                      author.getLastName(),
                                      author.getPhone(),
                                      author.getUuidStr(),
                                      author.getImage());

        for (Post comment : comments) {
            PostDTO commentDTO = new PostDTO(comment.getId(),
                                             comment.getDate(),
                                             comment.getTime(),
                                             comment.getDescription(),
                                             comment.getUser(),
                                             null,
                                             null,
                                             null,
                                             null);
            getComments().add(commentDTO);
        }
        for (User like : likes) {
            getLikes().add(UserBasicDTO.mapFromUserEntity(like));
        }
    }

    public static ArticleDTO mapFromArticleEntity(Article article) {
        return new ArticleDTO(article.getId(),
                              article.getUuidStr(),
                              article.getTitle(),
                              article.getBody(),
                              article.getDate(),
                              article.getAuthor(),
                              article.getComments(),
                              article.getLikes());
    }

    public static List<ArticleDTO> mapFromArticlesEntities(List<Article> articles) {
        return articles.stream().map((article) -> mapFromArticleEntity(article)).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public UserInfoDTO getAuthor() {
        return author;
    }

    public void setAuthor(UserInfoDTO author) {
        this.author = author;
    }

    public List<PostDTO> getComments() {
        if (comments == null) {
            comments = new ArrayList<PostDTO>();
        }
        return comments;
    }

    public void setComments(List<PostDTO> comments) {
        this.comments = comments;
    }

    public Set<UserBasicDTO> getLikes() {
        if (likes == null) {
            likes = new HashSet<UserBasicDTO>();
        }
        return likes;
    }

    public void setLikes(Set<UserBasicDTO> likes) {
        this.likes = likes;
    }

}
