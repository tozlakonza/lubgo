package com.lubgo.app.dto;

import com.lubgo.app.model.PigeonAchievement;

public class PigeonAchievementDTO {

    private Long id;
    private String uuid;
    private String type;
    private Integer place;
    private String race;
    private Integer birds;
    private Integer year;
    private String description;

    public PigeonAchievementDTO() {}

    public PigeonAchievementDTO(Long id, String uuid, String type, Integer place, String race, Integer birds, Integer year, String description) {
        this.id = id;
        this.uuid = uuid;
        this.type = type;
        this.place = place;
        this.race = race;
        this.birds = birds;
        this.year = year;
        this.description = description;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public static PigeonAchievementDTO mapFromPigeonAchievementEntity(PigeonAchievement pigeonAchievement) {
        return new PigeonAchievementDTO(pigeonAchievement.getId(),
                                        pigeonAchievement.getUuidStr(),
                                        pigeonAchievement.getType().toString(),
                                        pigeonAchievement.getPlace(),
                                        pigeonAchievement.getRace(),
                                        pigeonAchievement.getBirds(),
                                        pigeonAchievement.getYear(),
                                        pigeonAchievement.getDescription());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Integer getBirds() {
        return birds;
    }

    public void setBirds(Integer birds) {
        this.birds = birds;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
