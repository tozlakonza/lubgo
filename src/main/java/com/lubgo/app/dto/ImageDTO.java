package com.lubgo.app.dto;

import com.lubgo.app.model.Image;

import lombok.Getter;
import lombok.Setter;

public class ImageDTO {

    private @Getter @Setter String uuid;

    public ImageDTO() {}

    public ImageDTO(String uuid) {
        this.uuid = uuid;
    }

    public static ImageDTO mapFromImageEntity(Image image) {
        if (image == null) {
            return new ImageDTO("no-image");
        }
        return new ImageDTO(image.getUuidStr());
    }

}
