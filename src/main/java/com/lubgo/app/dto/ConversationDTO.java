package com.lubgo.app.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.lubgo.app.model.Conversation;
import com.lubgo.app.model.Message;
import com.lubgo.app.model.User;

public class ConversationDTO {

    private Long id;

    private Date startDate;

    private List<UserBasicDTO> participants;

    private List<MessageDTO> messages;

    public ConversationDTO() {}

    public ConversationDTO(Long id, Timestamp startDate, Set<User> participants, List<Message> messages) {
        this.id = id;
        this.startDate = startDate;
        for (User participant : participants) {
            getParticipants().add(UserBasicDTO.mapFromUserEntity(participant));
        }
        if (messages != null) {
            for (Message message : messages) {
                getMessages().add(MessageDTO.mapFromMessageEntity(message));
            }
        }
    }

    public static ConversationDTO mapFromConversationEntity(Conversation conversation) {
        return new ConversationDTO(conversation.getId(), conversation.getStartDate(), conversation.getParticipants(), conversation.getMessages());
    }

    public static List<ConversationDTO> mapFromConversationsEntities(List<Conversation> conversations) {
        return conversations.stream().map((conversation) -> mapFromConversationEntity(conversation)).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public List<UserBasicDTO> getParticipants() {
        if (participants == null) {
            participants = new ArrayList<UserBasicDTO>();
        }
        return participants;
    }

    public void setParticipants(List<UserBasicDTO> participants) {
        this.participants = participants;
    }

    public List<MessageDTO> getMessages() {
        if (messages == null) {
            messages = new ArrayList<MessageDTO>();
        }
        return messages;
    }

    public void setMessages(List<MessageDTO> messages) {
        this.messages = messages;
    }

}
