package com.lubgo.app.dto;

import com.lubgo.app.model.Image;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * JSON-serializable DTO containing user data
 *
 */
public class UserInfoDTO {

    private @Getter @Setter String firstName;
    private @Getter @Setter String lastName;
    private @Getter @Setter String email;
    private @Getter @Setter String phone;

    private @Getter @Setter String uuid;

    private @Getter @Setter String location;
    private @Getter @Setter String club;
    private @Getter @Setter String alliance;
    private @Getter @Setter String autobiography;

    private @Getter @Setter String connectionStatus;

    private @Getter @Setter boolean isLoggedInUser;

    private @Getter @Setter ImageDTO image;

    private @Getter @Setter boolean friend;

    public UserInfoDTO(String email, String firstName, String lastName, String phone, String uuid, Image image) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.uuid = uuid;
        this.image = ImageDTO.mapFromImageEntity(image);
    }

    public UserInfoDTO(String email, String firstName, String lastName, String phone, String alliance, String location, String club,
        String autobiography, String uuid, Image image) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.alliance = alliance;
        this.location = location;
        this.club = club;
        this.autobiography = autobiography;
        this.uuid = uuid;
        this.image = ImageDTO.mapFromImageEntity(image);
    }

}
