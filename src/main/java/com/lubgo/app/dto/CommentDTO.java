package com.lubgo.app.dto;

/**
 * Created by Pedja on 12/26/2015.
 */
public class CommentDTO {

    private Long postId;
    private String comment;

    public CommentDTO() {}

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
