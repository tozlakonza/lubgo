package com.lubgo.app.dto;

import com.lubgo.app.model.Image;
import com.lubgo.app.model.User;

import lombok.Getter;
import lombok.Setter;

public class UserBasicDTO {

    private @Getter @Setter Long id;
    private @Getter @Setter String uuid;
    private @Getter @Setter String firstName;
    private @Getter @Setter String lastName;
    private @Getter @Setter String email;
    private @Getter @Setter ImageDTO image;

    public UserBasicDTO() {}

    public UserBasicDTO(Long id, String email, String firstName, String lastName, String uuid, Image image) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.uuid = uuid;
        this.image = ImageDTO.mapFromImageEntity(image);
    }

    public static UserBasicDTO mapFromUserEntity(User user) {
        return new UserBasicDTO(user.getId(), user.getEmail(), user.getFirstName(), user.getLastName(), user.getUuidStr(), user.getImage());
    }

}
