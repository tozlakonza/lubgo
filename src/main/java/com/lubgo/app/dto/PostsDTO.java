package com.lubgo.app.dto;

import java.util.List;

/**
 *
 * JSON serializable DTO containing data concerning a post search request.
 *
 */
public class PostsDTO {

    List<PostDTO> posts;

    public PostsDTO(List<PostDTO> posts) {
        this.posts = posts;
    }

    public List<PostDTO> getPosts() {
        return posts;
    }

    public void setPosts(List<PostDTO> posts) {
        this.posts = posts;
    }
}