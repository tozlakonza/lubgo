package com.lubgo.app.services;

import static com.lubgo.app.services.util.ValidationUtils.assertNotBlank;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lubgo.app.dao.ConnectionRepository;
import com.lubgo.app.dao.UserRepository;
import com.lubgo.app.enums.ActivityType;
import com.lubgo.app.model.Activity;
import com.lubgo.app.model.Connection;
import com.lubgo.app.model.User;

/**
 * Created by Pedja on 1/9/2016.
 * 
 */
@Service
public class ConnectionService {

    private static final Logger LOGGER = Logger.getLogger(PigeonService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConnectionRepository connectionRepository;

    @Autowired
    private ActivityService activityService;

    @Transactional
    public Connection saveConnection(User from, User to) {
        Connection result = null;
        if (connectionRepository.canCreateNewConnection(from.getId(), to.getId())) {
            result = new Connection();
            from = userRepository.findUserByEmail(from.getEmail());
            to = userRepository.findUserByEmail(to.getEmail());
            result.setFrom(from);
            result.setTo(to);
            result = connectionRepository.saveConnection(result);
        }
        return result;
    }

    @Transactional
    public Connection saveConnectionRequest(String fromEmail, String toUuid) {

        assertNotBlank(fromEmail, "fromEmail cannot be blank");
        assertNotBlank(toUuid, "toUuid cannot be blank");

        User from = userRepository.findUserByEmail(fromEmail);
        User to = userRepository.findUserByUUID(toUuid);

        if (from.getEmail().equalsIgnoreCase(to.getEmail())) {
            LOGGER.error("Cannot send connection request to yourself");
            throw new IllegalArgumentException("Cannot send connection request to yourself");
        }
        Connection result = saveConnection(from, to);
        Activity activity = new Activity(from, ActivityType.CONNECTION_SENT, result.getId());
        activity.setConnection(result);
        activityService.save(activity);
        return result;
    }

    @Transactional
    public void acceptConnection(User from, User to) {
        resolveConnection(from, to, true);
    }

    @Transactional
    public void declineConnection(User from, User to) {
        resolveConnection(from, to, false);
    }

    private void resolveConnection(User from, User to, boolean outcome) {
        Connection cr = connectionRepository.findPendingConnection(from.getId(), to.getId());
        cr.setAccepted(outcome);
        cr.setResolvedOn(new Date());
        Connection result = connectionRepository.saveConnection(cr);
        ActivityType activityType = outcome ? ActivityType.CONNECTION_ACCEPTED : ActivityType.CONNECTION_REJECTED;
        Activity activity = new Activity(from, activityType, result.getId());
        activity.setConnection(result);
        activityService.save(activity);
    }

    @Transactional
    public void acceptConnection(String fromUuid, String toUuid) {
        User from = userRepository.findUserByUUID(fromUuid);
        User to = userRepository.findUserByUUID(toUuid);
        resolveConnection(from, to, true);
    }

    @Transactional(readOnly = true)
    public List<Connection> findPendingConnectionsByUserId(Long userId) {
        return connectionRepository.findPendingConnectionsByUserId(userId);
    }

    @Transactional(readOnly = true)
    public List<Connection> findAcceptedConnectionsByUserId(Long userId) {
        return connectionRepository.findAcceptedConnectionsByUserId(userId);
    }

    @Transactional(readOnly = true)
    public List<Connection> findDeclinedConnectionsByUserId(Long userId) {
        return connectionRepository.findDeclinedConnectionsByUserId(userId);
    }

    @Transactional
    public void rejectConnection(String fromUserUuid, String uuidStr) {
        User from = userRepository.findUserByUUID(fromUserUuid);
        User to = userRepository.findUserByUUID(uuidStr);
        resolveConnection(from, to, false);
    }

    @Transactional(readOnly = true)
    public Long countAcceptedConnectionsByUserId(Long id) {
        return connectionRepository.countAcceptedConnectionsByUserId(id);
    }

    @Transactional(readOnly = true)
    public List<User> findFriends(Long userId) {
        List<User> resultFrom = connectionRepository.findFriendsFrom(userId);
        List<User> resultTo = connectionRepository.findFriendsTo(userId);
        List<User> result = new ArrayList<>();
        result.addAll(resultFrom);
        result.addAll(resultTo);
        return result;
    }
}
