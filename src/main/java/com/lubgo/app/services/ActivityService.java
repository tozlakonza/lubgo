package com.lubgo.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lubgo.app.dao.ActivityRepository;
import com.lubgo.app.enums.ActivityType;
import com.lubgo.app.model.Activity;
import com.lubgo.app.model.Connection;
import com.lubgo.app.services.util.NotificationUtils;

/**
 * Created by Pedja on 6/3/2016.
 */
@Service
public class ActivityService {
    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private ConnectionService connectionService;

    public Activity save(Activity activity) {
        if (activity.getType() == ActivityType.CONNECTION_SENT) {
            activity.setNotify("|" + activity.getConnection().getTo().getId() + "|");
            // without this, "not like" query doesn't work on this field
            activity.setNotified("");
            String msg = NotificationUtils.getMessage(activity);
            template.convertAndSend("/topic/message/" + activity.getConnection().getTo().getUuidStr(),
                                    NotificationUtils.makeActivityJSONString("note", ActivityType.CONNECTION_SENT, msg));
        }
        else if (activity.getType() == ActivityType.CONNECTION_ACCEPTED) {
            activity.setNotify("|" + activity.getConnection().getFrom().getId() + "|");
            // without this, "not like" query doesn't work on this field
            activity.setNotified("");
            String msg = NotificationUtils.getMessage(activity);
            template.convertAndSend("/topic/message/" + activity.getConnection().getFrom().getUuidStr(),
                                    NotificationUtils.makeActivityJSONString("note", ActivityType.CONNECTION_ACCEPTED, msg));
            // find a CONNECTION_SENT notification for this connection and mark it as read
            Activity connectionSentActivity =
                activityRepository.findByParams(ActivityType.CONNECTION_SENT,
                                                activity.getConnection().getFrom().getId(),
                                                activity.getConnection().getTo().getId());
            connectionSentActivity.setNotified(connectionSentActivity.getNotified() + "|" + activity.getConnection().getTo().getId() + "|");
            activityRepository.update(connectionSentActivity);
        }
        else if (activity.getType() == ActivityType.POST_ADDED) {
            List<Connection> connections = connectionService.findAcceptedConnectionsByUserId(activity.getPerformer().getId());
            activity.setNotify("|");
            String msg = NotificationUtils.getMessage(activity);
            for (Connection c : connections) {
                activity.setNotify(activity.getNotify()
                    + (c.getFrom().getId().equals(activity.getPerformer().getId()) ? c.getTo().getId() : c.getFrom().getId()) + "|");
                String uuidToNotify = c.getFrom().getId().equals(activity.getPerformer().getId()) ? c.getTo().getUuidStr() : c.getFrom().getUuidStr();
                template.convertAndSend("/topic/message/" + uuidToNotify,
                        NotificationUtils.makeActivityJSONString("note", ActivityType.CONNECTION_ACCEPTED, msg));
            }
            activity.setNotified("");
        }
        else if (activity.getType() == ActivityType.POST_COMMENT) {
            activity.setNotify("|" + activity.getPost().getParent().getUser().getId() + "|");
            activity.setNotified("");
        }
        return activityRepository.save(activity);
    }

    @Transactional
    public void deleteActivitiesByPostId(Long postId) {
        List<Activity> activities = activityRepository.findActivitiesByPostId(postId);
        for (Activity activity : activities) {
            activityRepository.delete(activity);
        }
    }

    @Transactional
    public void deleteActivitiesByPigeonId(Long postId) {
        List<Activity> activities = activityRepository.findActivitiesByPigeonId(postId);
        for (Activity activity : activities) {
            activityRepository.delete(activity);
        }
    }

    @Transactional
    public void deleteActivitiesByArticleId(Long postId) {
        List<Activity> activities = activityRepository.findActivitiesByArticleId(postId);
        for (Activity activity : activities) {
            activityRepository.delete(activity);
        }
    }
}
