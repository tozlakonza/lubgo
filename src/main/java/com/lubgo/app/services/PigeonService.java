package com.lubgo.app.services;

import static com.lubgo.app.services.util.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lubgo.app.dao.ImageRepository;
import com.lubgo.app.dao.PigeonRepository;
import com.lubgo.app.dao.UserRepository;
import com.lubgo.app.enums.Sex;
import com.lubgo.app.model.Image;
import com.lubgo.app.model.Pigeon;
import com.lubgo.app.model.User;

@Service
public class PigeonService {

    private static final Logger LOGGER = Logger.getLogger(PigeonService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PigeonRepository pigeonRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Transactional(readOnly = true)
    public Pigeon findPigeonByUUID(String uuid) {
        return pigeonRepository.findPigeonByUUID(uuid);
    }

    @Transactional
    public Pigeon savePigeon(String email, Long id, boolean mainstream, String breed, String ring, String name, Sex sex, String owner, String origin,
        String dynasty, Date dateOfBirth, Date dateAcquired, Date dateDisposedOf, String description, String imageUUID) {

        if (dateAcquired == null) {
            dateAcquired = new Date();
        }

        assertNotBlank(email, "email cannot be blank");
        assertNotBlank(name, "name cannot be blank");

        Pigeon pigeon = new Pigeon();

        if (id != null) {
            pigeon = pigeonRepository.findPigeonById(id);
        }

        pigeon.setMainstream(mainstream);
        pigeon.setBreed(breed);
        pigeon.setRing(ring);
        pigeon.setName(name);
        pigeon.setSex(sex);
        pigeon.setOwner(owner);
        pigeon.setOrigin(origin);
        pigeon.setDynasty(dynasty);
        pigeon.setDateOfBirth(dateOfBirth);
        pigeon.setDateAcquired(dateAcquired);
        pigeon.setDateDisposedOf(dateDisposedOf);
        pigeon.setDescription(description);

        User user = userRepository.findUserByEmail(email);

        pigeon.setUser(user);

        Image image = imageRepository.findImageByUUID(imageUUID);

        pigeon.setImage(image);

        if (user != null) {
            pigeon = pigeonRepository.save(pigeon);
        }
        else {
            LOGGER.error("Cannot find a user with email: " + email);
            throw new IllegalArgumentException("Cannot find a user with email.");
        }

        return pigeon;
    }

    @Transactional(readOnly = true)
    public List<Pigeon> findPigeonByUserId(String email) {
        User user = userRepository.findUserByEmail(email);
        return pigeonRepository.findPigeonsByUserId(user.getId());
    }

    @Transactional(readOnly = true)
    public List<Pigeon> findPigeonByUserId(String email, Integer firstResult, Integer maxResult) {
        User user = userRepository.findUserByEmail(email);
        return pigeonRepository.findPigeonsByUserId(user.getId(), firstResult, maxResult);
    }

    @Transactional(readOnly = true)
    public Long countPigeonByUserId(Long id) {
        return pigeonRepository.countPigeonsByUserId(id);
    }

    @Transactional
    public void likePigeon(String email, Long id) {
        assertNotBlank(email, "email cannot be blank");
        notNull(id, "id is mandatory");
        User user = userRepository.findUserByEmail(email);

        if (user != null) {
            pigeonRepository.likePigeon(user, id);
        }
        else {
            LOGGER.error("Cannot find a user with email: " + email);
            throw new IllegalArgumentException("Cannot find a user with email.");
        }
    }

    @Transactional
    public void unlikePigeon(String email, Long id) {
        assertNotBlank(email, "email cannot be blank");
        notNull(id, "id is mandatory");
        User user = userRepository.findUserByEmail(email);

        if (user != null) {
            pigeonRepository.unlikePigeon(user, id);
        }
        else {
            LOGGER.error("Cannot find a user with email: " + email);
            throw new IllegalArgumentException("Cannot find a user with email.");
        }
    }

    @Transactional
    public void deletePigeon(Long deletedPigeonId) {
        Pigeon pigeon = pigeonRepository.findPigeonById(deletedPigeonId);
        pigeonRepository.delete(pigeon);
    }
}
