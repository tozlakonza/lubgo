package com.lubgo.app.services;

import com.lubgo.config.root.ApplicationProperties;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by Pedja on 6/25/2016.
 */
@Service
public class EmailService {

    private static final Logger LOGGER = Logger.getLogger(PigeonService.class);

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private ApplicationProperties applicationProperties;

    @PostConstruct
    private void init() {

    }

    @Async
    public void sendEmail(SimpleMailMessage mailMessage) {
        if (applicationProperties.isEmailEnabled()) {
            mailSender.send(mailMessage);
        }
    }

    @Async
    public void sendEmail(MimeMessagePreparator preparator) {
        if (applicationProperties.isEmailEnabled()) {
            mailSender.send(preparator);
        }
    }
}
