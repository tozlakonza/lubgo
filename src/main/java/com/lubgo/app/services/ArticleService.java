package com.lubgo.app.services;

import static com.lubgo.app.services.util.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lubgo.app.dao.ArticleRepository;
import com.lubgo.app.dao.UserRepository;
import com.lubgo.app.model.Article;
import com.lubgo.app.model.User;

@Service
public class ArticleService {

    private static final Logger LOGGER = Logger.getLogger(ArticleService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Transactional(readOnly = true)
    public Article findArticleByUUID(String uuid) {
        return articleRepository.findArticleByUUID(uuid);
    }

    @Transactional(readOnly = true)
    public Article findArticleById(Long id) {
        return articleRepository.findArticleById(id);
    }

    @Transactional
    public Article saveArticle(String email, Long id, String title, String body, Date date) {

        if (date == null) {
            date = new Date();
        }

        assertNotBlank(title, "title cannot be blank");
        assertNotBlank(body, "body cannot be blank");

        Article article = new Article();

        if (id != null) {
            article = articleRepository.findArticleById(id);
        }

        article.setTitle(title);
        article.setBody(body);

        User user = userRepository.findUserByEmail(email);

        article.setAuthor(user);

        if (user != null) {
            article = articleRepository.save(article);
        }
        else {
            LOGGER.error("Cannot find a user with email: " + email);
            throw new IllegalArgumentException("Cannot find a user with email.");
        }

        return article;
    }

    @Transactional(readOnly = true)
    public List<Article> findArticlesByAuthorEmail(String email) {
        User user = userRepository.findUserByEmail(email);
        return articleRepository.findArticlesByAuthorId(user.getId());
    }

    @Transactional(readOnly = true)
    public List<Article> findArticlesByAuthorEmail(String email, Integer firstResult, Integer maxResult) {
        User user = userRepository.findUserByEmail(email);
        return articleRepository.findArticlesByAuthorId(user.getId(), firstResult, maxResult);
    }

    @Transactional(readOnly = true)
    public Long countArticlesByAuthorId(Long id) {
        return articleRepository.countArticlesByAuthorId(id);
    }

    @Transactional
    public void deleteArticle(Long deletedArticleId) {
        notNull(deletedArticleId, "deletedArticleId is mandatory");
        articleRepository.delete(deletedArticleId);
    }

    @Transactional
    public void likeArticle(String email, Long id) {
        assertNotBlank(email, "email cannot be blank");
        notNull(id, "id is mandatory");
        User user = userRepository.findUserByEmail(email);

        if (user != null) {
            articleRepository.likeArticle(user, id);
        }
        else {
            LOGGER.error("Cannot find a user with email: " + email);
            throw new IllegalArgumentException("Cannot find a user with email.");
        }
    }

    @Transactional
    public void unlikeArticle(String email, Long id) {
        assertNotBlank(email, "email cannot be blank");
        notNull(id, "id is mandatory");
        User user = userRepository.findUserByEmail(email);

        if (user != null) {
            articleRepository.unlikeArticle(user, id);
        }
        else {
            LOGGER.error("Cannot find a user with email: " + email);
            throw new IllegalArgumentException("Cannot find a user with email.");
        }
    }
}
