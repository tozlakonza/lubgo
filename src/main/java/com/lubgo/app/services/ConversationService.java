package com.lubgo.app.services;

import static com.lubgo.app.services.util.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lubgo.app.dao.ConversationRepository;
import com.lubgo.app.dao.MessageRepository;
import com.lubgo.app.dao.UserRepository;
import com.lubgo.app.model.Conversation;
import com.lubgo.app.model.Conversation.ConversationBuilder;
import com.lubgo.app.model.User;

@Service
public class ConversationService {

    private static final Logger LOGGER = Logger.getLogger(ConversationService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Transactional(readOnly = true)
    public Conversation findConversationById(Long id) {
        return conversationRepository.findConversationById(id);
    }

    @Transactional(readOnly = true)
    public List<Conversation> findConversationUserEmail(String email, Integer page, Integer size) {
        User user = userRepository.findUserByEmail(email);
        if (page != null && size != null) {
            return conversationRepository.findConversationsByUserId(user.getId(), page, size);
        }
        else {
            return conversationRepository.findConversationsByUserId(user.getId(), 1, 10);
        }
    }

    @Transactional(readOnly = true)
    public Conversation findConversationByParticipantsIds(String email, List<Long> ids) {
        User sender = userRepository.findUserByEmail(email);
        ids.add(sender.getId());
        Long conversationId = conversationRepository.findConversationByParticipantsIds(ids);
        return conversationId != null ? conversationRepository.findConversationById(conversationId) : null;
    }

    @Transactional
    public Conversation saveConversation(String email, Timestamp startDate, Set<String> participantEmails) {

        if (startDate == null) {
            startDate = new Timestamp(System.currentTimeMillis());
        }

        assertNotBlank(email, "email cannot be blank");

        User sender = userRepository.findUserByEmail(email);

        if (sender == null) {
            LOGGER.error("Cannot find a user with email: " + email);
            throw new IllegalArgumentException("Cannot find a user with email.");
        }

        Set<User> participantUsers = new HashSet<User>();

        participantUsers.add(sender);

        if (participantEmails.size() > 0) {

            for (String participantEmail : participantEmails) {
                User participant = userRepository.findUserByEmail(participantEmail);

                if (participant != null) {
                    participantUsers.add(participant);
                }
                else {
                    LOGGER.error("Cannot find a user with email: " + participantEmail);
                    throw new IllegalArgumentException("Cannot find a user with email.");
                }
            }

        }

        Conversation conversation = new ConversationBuilder().startDate(startDate).participants(participantUsers).build();

        return conversationRepository.save(conversation);

    }

    @Transactional
    public void deleteConversationById(Long conversationId) {
        notNull(conversationId, "conversationId is mandatory");
        conversationRepository.deleteById(conversationId);
    }

    @Transactional
    public void leaveConversationById(String email, Long conversationId) {

        assertNotBlank(email, "email cannot be blank");
        notNull(conversationId, "conversationId is mandatory");

        Conversation conversation = conversationRepository.findConversationById(conversationId);

        if (conversation != null) {
            Set<User> participants = conversation.getParticipants();
            Iterator<User> participantsIterator = participants.iterator();
            while (participantsIterator.hasNext()) {
                User user = (User)participantsIterator.next();
                if (user.getEmail().equalsIgnoreCase(email)) {
                    participants.remove(user);
                    break;
                }
            }

            conversationRepository.save(conversation);
        }
        else {
            LOGGER.error("Cannot find a conversation with id: " + conversationId);
            throw new IllegalArgumentException("The conversation is not available.");
        }

    }

    @Transactional
    public void addToConversationById(String email, Long conversationId) {

        assertNotBlank(email, "email cannot be blank");
        notNull(conversationId, "conversationId is mandatory");

        User user = userRepository.findUserByEmail(email);

        if (user == null) {
            LOGGER.error("Cannot find a user with email: " + email);
            throw new IllegalArgumentException("The user is not available.");
        }

        Conversation conversation = conversationRepository.findConversationById(conversationId);

        if (conversation != null) {

            conversation.getParticipants().add(user);

            conversationRepository.save(conversation);
        }
        else {
            LOGGER.error("Cannot find a conversation with id: " + conversationId);
            throw new IllegalArgumentException("The conversation is not available.");
        }

    }

    @Transactional
    public void deleteConversation(Long conversationId) {

        notNull(conversationId, "conversationId is mandatory");

        Conversation conversation = conversationRepository.findConversationById(conversationId);

        conversation.getMessages().forEach(message -> messageRepository.delete(message));

        conversationRepository.delete(conversation);
    }

}
