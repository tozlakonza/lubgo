package com.lubgo.app.services.config;

/**
 * Created by Pedja on 1/9/2016.
 */
public class UserFetchConfig {
    private boolean connections;

    public UserFetchConfig(boolean connections) {
        this.connections = connections;
    }

    public boolean isConnections() {
        return connections;
    }

    public void setConnections(boolean connections) {
        this.connections = connections;
    }
}
