package com.lubgo.app.services;

import static com.lubgo.app.services.util.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

import java.sql.Timestamp;
import java.util.List;

import com.lubgo.config.root.ApplicationProperties;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lubgo.app.dao.ConversationRepository;
import com.lubgo.app.dao.MessageRepository;
import com.lubgo.app.dao.UserRepository;
import com.lubgo.app.model.Conversation;
import com.lubgo.app.model.Message;
import com.lubgo.app.model.User;

import javax.mail.internet.InternetAddress;

@Service
public class MessageService {

    private static final Logger LOGGER = Logger.getLogger(MessageService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Transactional(readOnly = true)
    public Message findMessageById(Long id) {
        return messageRepository.findMessageById(id);
    }

    @Transactional(readOnly = true)
    public List<Message> findMessagesByConversationId(Long conversationId) {
        return messageRepository.findMessagesByConversationId(conversationId);
    }

    @Transactional
    public Message saveMessage(String senderEmail, Long conversationId, String body, Timestamp timestamp) {

        if (timestamp == null) {
            timestamp = new Timestamp(System.currentTimeMillis());
        }

        assertNotBlank(senderEmail, "senderEmail cannot be blank");
        assertNotBlank(body, "body cannot be blank");

        User sender = userRepository.findUserByEmail(senderEmail);

        if (sender == null) {
            LOGGER.error("Cannot find a user with email: " + senderEmail);
            throw new IllegalArgumentException("Cannot find a user with email.");
        }

        Conversation conversation = conversationRepository.findConversationById(conversationId);

        if (conversation == null) {
            LOGGER.error("Cannot find a conversation with id: " + conversationId);
            throw new IllegalArgumentException("Cannot find a conversation with id.");
        }

        Message message = Message.builder()
                                 .body(body)
                                 .sender(sender)
                                 .timestamp(timestamp)
                                 .conversation(conversation)
                                 .build();

        message = messageRepository.save(message);

        for (User u : conversation.getParticipants()) {
            if (!u.getId().equals(sender.getId())) {
                MimeMessagePreparator preparator = mimeMessage -> {

                    mimeMessage.setSubject(sender.getName() + " sent you a new message | LubGo.net");
                    mimeMessage.setFrom(applicationProperties.getNoReplyAddress());
                    mimeMessage.setText(
                            "You have a new message from " + sender.getName() + ".<br/><br/>LubGo.net Team", "UTF-8", "html");
                    mimeMessage.setRecipient(javax.mail.Message.RecipientType.TO,
                            new InternetAddress(u.getEmail()));
                };
                emailService.sendEmail(preparator);
            }
        }
        return message;
    }

    @Transactional
    public void deleteMessageById(Long messageId) {
        notNull(messageId, "messageId is mandatory");
        messageRepository.deleteById(messageId);
    }

}
