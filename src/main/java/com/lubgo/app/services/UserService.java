package com.lubgo.app.services;

import com.lubgo.app.dao.ConnectionRepository;
import com.lubgo.app.dao.ImageRepository;
import com.lubgo.app.dao.UserRepository;
import com.lubgo.app.dto.UserInfoDTO;
import com.lubgo.app.enums.ConnectionStatus;
import com.lubgo.app.model.Connection;
import com.lubgo.app.model.Image;
import com.lubgo.app.model.SearchResult;
import com.lubgo.app.model.User;
import com.lubgo.app.model.User.UserBuilder;
import com.lubgo.app.services.util.SearchUtils;
import com.lubgo.config.root.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.lubgo.app.services.util.ValidationUtils.assertMatches;
import static com.lubgo.app.services.util.ValidationUtils.assertNotBlank;

@Service
public class UserService {

    private static final Pattern EMAIL_REGEX =
            Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    private static final Pattern PASSWORD_REGEX = Pattern.compile("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}");

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConnectionRepository connectionRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Transactional
    public User createUser(String firstName, String lastName, String email, String password) {

        assertNotBlank(firstName, "First Name cannot be empty.");
        assertNotBlank(lastName, "Last Name cannot be empty.");
        assertNotBlank(email, "Email cannot be empty.");
        assertMatches(email, EMAIL_REGEX, "Invalid email.");
        assertNotBlank(password, "Password cannot be empty.");
        assertMatches(password, PASSWORD_REGEX, "Password must have at least 6 characters, with 1 numeric and 1 uppercase character.");

        if (!userRepository.isEmailAvailable(email)) {
            throw new IllegalArgumentException("The email is not available.");
        }

        Image image = imageRepository.findImageByUUID("no-image");

        User user = new UserBuilder().email(email)
                .firstName(firstName)
                .lastName(lastName)
                .passwordDigest(new BCryptPasswordEncoder().encode(password))
                .image(image)
                .build();

        MimeMessagePreparator preparator = mimeMessage -> {

            mimeMessage.setSubject("Welcome to LubGo.net!");
            mimeMessage.setRecipient(Message.RecipientType.TO,
                    new InternetAddress(email));
            mimeMessage.setFrom(new InternetAddress(applicationProperties.getNoReplyAddress()));
            mimeMessage.setText(
                    "Dear " + firstName + " "
                            + lastName
                            + ", <br/><br/>You have successfully registered at LubGo.net. This site is still in beta version. If you have any questions or suggestions, please write us at support@lubgo.net. Thank you.<br/><br/>LubGo.net Team<br/>www.lubgo.net", "UTF-8", "html");
        };
        emailService.sendEmail(preparator);

        return userRepository.save(user);
    }

    @Transactional
    public void changeUserPassword(String email, String oldPassword, String newPassword) {

        assertNotBlank(oldPassword, "Old password cannot be empty.");
        assertNotBlank(newPassword, "New password cannot be empty.");
        assertMatches(newPassword, PASSWORD_REGEX, "Password must have at least 6 characters, with 1 numeric and 1 uppercase character.");

        User user = userRepository.findUserByEmail(email);

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if (!passwordEncoder.matches(oldPassword, user.getPasswordDigest())) {
            throw new IllegalArgumentException("Old password does not match.");
        }

        user.setPasswordDigest(passwordEncoder.encode(newPassword));

        userRepository.save(user);
    }

    @Transactional
    public void updateBasicInfo(String email, String firstName, String lastName, String newEmail, String phone) {

        assertNotBlank(firstName, "Old password cannot be empty.");
        assertNotBlank(lastName, "New password cannot be empty.");
        assertNotBlank(newEmail, "New password cannot be empty.");

        User user = userRepository.findUserByEmail(email);

        if (!user.getEmail().equalsIgnoreCase(newEmail)) {
            User userWithSameEmail = userRepository.findUserByEmail(newEmail);
            if (userWithSameEmail != null) {
                throw new IllegalArgumentException("There is already user with that email.");
            }
        }
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(newEmail);
        user.setPhone(phone);

        userRepository.save(user);
    }

    @Transactional
    public void updateOtherInfo(String email, String alliance, String location, String club, String autobiography) {

        User user = userRepository.findUserByEmail(email);

        user.setAlliance(alliance);
        user.setLocation(location);
        user.setClub(club);
        user.setAutobiography(autobiography);

        userRepository.save(user);
    }

    @Transactional(readOnly = true)
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Transactional(readOnly = true)
    public SearchResult<UserInfoDTO> findUsers(String term) {
        term = SearchUtils.prepareParamForLikeSearch(term);
        List<User> users = userRepository.findUsers(term);
        List<UserInfoDTO> usersDTO = new ArrayList<>();
        for (User user : users) {
            usersDTO.add(new UserInfoDTO(user.getEmail(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getPhone(),
                    user.getUuidStr(),
                    user.getImage()));
        }
        return new SearchResult<UserInfoDTO>(users.size(), usersDTO);
    }

    @Transactional(readOnly = true)
    public SearchResult<UserInfoDTO> findUsers(String term, Integer firstResult, Integer maxResult) {
        term = SearchUtils.prepareParamForLikeSearch(term);
        List<User> users = userRepository.findUsers(term, firstResult, maxResult);
        List<UserInfoDTO> usersDTO = new ArrayList<>();
        for (User user : users) {
            usersDTO.add(new UserInfoDTO(user.getEmail(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getPhone(),
                    user.getUuidStr(),
                    user.getImage()));
        }
        return new SearchResult<UserInfoDTO>(users.size(), usersDTO);
    }

    @Transactional(readOnly = true)
    public User findUserByUUID(String uuid) {
        return userRepository.findUserByUUID(uuid);
    }

    @Transactional(readOnly = true)
    public ConnectionStatus getConnectionStatus(String fromUuid, String toUuid) {
        if (fromUuid.equals(toUuid)) {
            return ConnectionStatus.NOT_APPLICABLE;
        }
        ConnectionStatus result;
        // check if users are already connected
        boolean alreadyConnected = userRepository.checkIfAlreadyConnected(fromUuid, toUuid);
        if (alreadyConnected) {
            result = ConnectionStatus.CONNECTED;
        } else {
            // if not, check if there is a pending connection
            Connection cr = connectionRepository.findPendingConnectionRequest(fromUuid, toUuid);
            if (cr != null) {
                result = ConnectionStatus.PENDING;
            } else {
                // check the other way around
                cr = connectionRepository.findPendingConnectionRequest(toUuid, fromUuid);
                if (cr != null) {
                    result = ConnectionStatus.AWAITING_ACCEPTANCE;
                } else {
                    result = ConnectionStatus.AVAILABLE;
                }
            }
        }
        return result;
    }

    @Transactional
    public void updateProfilePicture(String email, String uuid) {
        User user = userRepository.findUserByEmail(email);
        Image image = imageRepository.findImageByUUID(uuid);
        user.setImage(image);
        userRepository.save(user);
    }
}
