package com.lubgo.app.services;

import static com.lubgo.app.services.util.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lubgo.app.dao.ArticleRepository;
import com.lubgo.app.dao.ImageRepository;
import com.lubgo.app.dao.LinkRepository;
import com.lubgo.app.dao.PigeonRepository;
import com.lubgo.app.dao.PostRepository;
import com.lubgo.app.dao.UserRepository;
import com.lubgo.app.enums.ActivityType;
import com.lubgo.app.model.Activity;
import com.lubgo.app.model.Article;
import com.lubgo.app.model.Image;
import com.lubgo.app.model.Link;
import com.lubgo.app.model.Pigeon;
import com.lubgo.app.model.Post;
import com.lubgo.app.model.User;
import com.lubgo.app.services.util.NotificationUtils;

@Service
public class PostService {

    private static final Logger LOGGER = Logger.getLogger(PostService.class);

    @Autowired
    PostRepository postRepository;

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    PigeonRepository pigeonRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    LinkRepository linkRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private SimpMessagingTemplate template;

    @Transactional
    public void deletePost(Long deletedPostId) {
        notNull(deletedPostId, "deletedPostsId is mandatory");
        activityService.deleteActivitiesByPostId(deletedPostId);
        postRepository.delete(deletedPostId);
    }

    @Transactional
    public void likePost(String email, Long id) {
        assertNotBlank(email, "email cannot be blank");
        notNull(id, "id is mandatory");
        User user = userRepository.findUserByEmail(email);

        if (user != null) {
            postRepository.likePost(user, id);
        }
        else {
            LOGGER.error("Cannot find a user with email: " + email);
            throw new IllegalArgumentException("Cannot find a user with email.");
        }
    }

    @Transactional
    public void unlikePost(String email, Long id) {
        assertNotBlank(email, "email cannot be blank");
        notNull(id, "id is mandatory");
        User user = userRepository.findUserByEmail(email);

        if (user != null) {
            postRepository.unlikePost(user, id);
        }
        else {
            LOGGER.error("Cannot find a user with email: " + email);
            throw new IllegalArgumentException("Cannot find a user with email.");
        }
    }

    @Transactional
    public Post savePost(String email, Long id, Date date, Time time, String description, String linkTitle, String linkDescription, String url,
        String linkImageUUID,
        String imageUUID) {

        if (date == null) {
            date = new Date();
        }
        if (time == null) {
            time = new Time(date.getTime());
        }
        assertNotBlank(email, "email cannot be blank");
        notNull(date, "date is mandatory");
        notNull(time, "time is mandatory");
        notNull(description, "description is mandatory");

        Post post = null;

        if (id != null) {
            post = postRepository.findPostById(id);

            post.setDate(date);
            post.setTime(time);
            post.setDescription(description);
        }
        else {
            User user = userRepository.findUserByEmail(email);

            Image image = null;
            Link link = null;

            if (imageUUID != null) {
                image = imageRepository.findImageByUUID(imageUUID);
            }

            if (url != null) {
                Image linkImage = null;

                if (linkImageUUID != null) {
                    linkImage = imageRepository.findImageByUUID(linkImageUUID);
                }

                link = linkRepository.save(new Link(linkTitle, linkDescription, url, linkImage, null));
            }

            if (user != null) {
                post = postRepository.save(new Post(user, date, time, description, link, image));
                Activity activity = new Activity(user, ActivityType.POST_ADDED, post.getId());
                activity.setPost(post);
                activityService.save(activity);
            }
            else {
                LOGGER.error("Cannot find a user with email: " + email);
                throw new IllegalArgumentException("Cannot find a user with email.");
            }
        }

        return post;
    }

    @Transactional(readOnly = true)
    public List<Post> getNewsFeed(String email) {
        User user = userRepository.findUserByEmail(email);
        return postRepository.findPostFeedByUser(user.getId());
    }

    @Transactional(readOnly = true)
    public List<Post> getPostsByUUID(String uuid) {
        User user = userRepository.findUserByUUID(uuid);
        return postRepository.findPostByUser(user.getId());
    }

    @Transactional(readOnly = true)
    public Post getPost(Long id) {
        return postRepository.findPostById(id);
    }

    @Transactional
    public Post saveComment(String email, Long postId, String comment) {
        User user = userRepository.findUserByEmail(email);
        Post post = postRepository.findPostById(postId);
        Post result = postRepository.saveComment(user, post, comment);
        Activity activity = new Activity(user, ActivityType.POST_COMMENT, result.getId());
        activity.setPost(result);
        String msg = NotificationUtils.getMessage(activity);
        activityService.save(activity);
        template.convertAndSend("/topic/message/" + result.getParent().getUser().getUuidStr(),
                                NotificationUtils.makeActivityJSONString("note", ActivityType.POST_COMMENT, msg));

        return result;
    }

    @Transactional
    public void deleteComment(Long deletedPostId, Long deletedCommentId) {
        notNull(deletedPostId, "deletedPostId is mandatory");
        notNull(deletedCommentId, "deletedCommentId is mandatory");
        activityService.deleteActivitiesByPostId(deletedPostId);
        Post post = postRepository.findPostById(deletedPostId);
        Post comment = postRepository.findPostById(deletedCommentId);
        postRepository.deleteComment(post, comment);
    }

    @Transactional
    public Post saveArticleComment(String email, Long articleId, String comment) {
        User user = userRepository.findUserByEmail(email);
        Article article = articleRepository.findArticleById(articleId);
        return postRepository.saveArticleComment(user, article, comment);
    }

    @Transactional
    public void deleteArticleComment(Long deletedArticleId, Long deletedCommentId) {
        notNull(deletedArticleId, "deletedArticleId is mandatory");
        notNull(deletedCommentId, "deletedCommentId is mandatory");
        Article article = articleRepository.findArticleById(deletedArticleId);
        activityService.deleteActivitiesByArticleId(deletedArticleId);
        Post comment = postRepository.findPostById(deletedCommentId);
        postRepository.deleteArticleComment(article, comment);
    }

    @Transactional
    public Post savePigeonComment(String email, Long pigeonId, String comment) {
        User user = userRepository.findUserByEmail(email);
        Pigeon pigeon = pigeonRepository.findPigeonById(pigeonId);
        return postRepository.savePigeonComment(user, pigeon, comment);
    }

    @Transactional
    public void deletePigeonComment(Long deletedPigeonId, Long deletedCommentId) {
        notNull(deletedPigeonId, "deletedPostId is mandatory");
        notNull(deletedCommentId, "deletedCommentId is mandatory");
        Pigeon pigeon = pigeonRepository.findPigeonById(deletedPigeonId);
        activityService.deleteActivitiesByPigeonId(deletedPigeonId);
        Post comment = postRepository.findPostById(deletedCommentId);
        postRepository.deletePigeonComment(pigeon, comment);
    }
}
