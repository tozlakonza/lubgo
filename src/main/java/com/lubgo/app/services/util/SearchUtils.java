package com.lubgo.app.services.util;

/**
 * Created by Pedja on 12/28/2015.
 */
public class SearchUtils {

    public static String prepareParamForLikeSearch(String param) {
        if (param != null) {
            param = param.toLowerCase();
        }
        return "%" + param + "%";
    }
}
