package com.lubgo.app.services.util;

import com.google.gson.JsonObject;
import com.lubgo.app.enums.ActivityType;
import com.lubgo.app.model.Activity;

/**
 * Created by Pedja on 3/27/2016.
 */
public class NotificationUtils {
    public static String getNotificationLink(Activity activity) {
        switch (activity.getType()) {
            case CONNECTION_SENT:
                return "/profile/" + activity.getPerformer().getUuidStr();
            default:
                return "";
        }
    }

    public static String getMessage(Activity activity) {
        String result;
        switch (activity.getType()) {
            case CONNECTION_SENT:
                result = activity.getPerformer().getName() + " wants to connect";
                break;
            case CONNECTION_ACCEPTED:
                result = activity.getConnection().getTo().getName() + " is now a connection";
                break;
            case POST_ADDED:
                result = activity.getPerformer().getName() + " added a new post";
                break;
            case POST_COMMENT:
                result = activity.getPerformer().getName() + " commented on your post";
                break;
            default:
                result = "TODO";
        }
        return result;
    }

    public static String makeActivityJSONString(String level, ActivityType type, String message) {
        JsonObject result = new JsonObject();
        result.addProperty("level", level);
        result.addProperty("type", type.toString());
        result.addProperty("message", message);
        return result.toString();
    }
}
