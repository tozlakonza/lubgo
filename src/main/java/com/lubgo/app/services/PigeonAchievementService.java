package com.lubgo.app.services;

import static org.springframework.util.Assert.notNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lubgo.app.dao.PigeonAchievementRepository;
import com.lubgo.app.enums.PigeonAchivementType;
import com.lubgo.app.model.Pigeon;
import com.lubgo.app.model.PigeonAchievement;

@Service
public class PigeonAchievementService {

    private static final Logger LOGGER = Logger.getLogger(PigeonAchievementService.class);

    @Autowired
    private PigeonAchievementRepository achievementRepository;

    @Transactional(readOnly = true)
    public PigeonAchievement findPigeonById(Long id) {
        return achievementRepository.findById(id);
    }

    @Transactional
    public PigeonAchievement savePigeonAchievement(Long id, PigeonAchivementType type, Integer place,
        String race, Integer birds, Integer year, String description, Pigeon pigeon) {

        notNull(type, "type cannot be blank");
        notNull(pigeon, "pigeon is mandatory");

        PigeonAchievement achievement = new PigeonAchievement();

        if (id != null) {

            achievement = achievementRepository.findById(id);

            if (achievement == null) {
                LOGGER.error("Cannot find a pigeon achievement with id: " + id);
                throw new IllegalArgumentException("Cannot find a achievement with id.");
            }
        }

        achievement.setType(type);
        achievement.setPlace(place);
        achievement.setRace(race);
        achievement.setBirds(birds);
        achievement.setYear(year);
        achievement.setDescription(description);
        achievement.setPigeon(pigeon);

        achievement = achievementRepository.save(achievement);

        return achievement;
    }

}
