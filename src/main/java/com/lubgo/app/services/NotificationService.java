package com.lubgo.app.services;

import com.lubgo.app.dao.ActivityRepository;
import com.lubgo.app.model.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Pedja on 3/18/2016.
 */
@Service
public class NotificationService {
    @Autowired
    private ActivityRepository activityRepository;

    @Transactional
    public void markRead(Long activityId, Long userId) {
        Activity activity = activityRepository.findById(activityId);
        activity.setNotified(activity.getNotified() + "|" + userId + "|");
        activityRepository.update(activity);
    }

    @Transactional
    public void markAllNotificationsRead(Long userId) {
        for (Activity activity : findNewActivities(userId)) {
            markRead(activity.getId(), userId);
        }
    }

    public List<Activity> findNewActivities(Long userId) {
        return activityRepository.findNewForUser(userId);
    }

    public List<Activity> findAllActivities(Long userId, int pageNumber, int pageSize) {
        return activityRepository.findAllForUser(userId, pageNumber, pageSize);
    }
}
