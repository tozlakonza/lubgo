package com.lubgo.app.services;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lubgo.app.model.Image;
import com.lubgo.app.model.Link;

@Service
public class LinkService {

    @Autowired
    private ImageService imageService;

    public Link linkPreview(String url) {
        Document doc;
        try {
            doc = Jsoup.connect(url).get();

            String title = getTitle(doc);
            String description = getDescription(doc);
            String imageNode = getImage(doc);

            Image img = new Image();

            if (imageNode != null) {
                img.setUuidStr(imageService.uploadImageFromUrl(imageNode));
            }

            return new Link(title, description, url, img, null);
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public String getDescription(Document document) {
        String description = getMetaTag(document, "og:description");

        if (description == null) {
            description = getMetaTag(document, "description");
        }
        return description;
    }

    public String getTitle(Document document) {
        String title = getMetaTag(document, "og:title");

        if (title == null) {
            Elements titleElement = document.select("title");
            title = titleElement != null ? titleElement.first().text() : null;
        }
        return title;
    }

    public String getImage(Document document) {
        String image = getMetaTag(document, "og:image");

        return image;
    }

    private String getMetaTag(Document document, String attr) {
        Elements elements = document.select("meta[name=" + attr + "]");
        for (Element element : elements) {
            String s = element.attr("content");
            if (s != null)
                return s;
        }

        elements = document.select("meta[property=" + attr + "]");
        for (Element element : elements) {
            String s = null;
            if (attr != "og:image") {
                s = element.attr("content");
            }
            else {
                s = element.absUrl("content");
            }

            if (s != null)
                return s;
        }
        return null;
    }

}
