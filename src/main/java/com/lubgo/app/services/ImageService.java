package com.lubgo.app.services;

import static com.lubgo.app.services.util.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lubgo.app.dao.ImageRepository;
import com.lubgo.app.model.Image;
import com.lubgo.config.root.ApplicationProperties;

@Service
public class ImageService {

    private static final Logger LOGGER = Logger.getLogger(ImageService.class);

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private ImageRepository imageRepository;

    @Transactional(readOnly = true)
    public Image findImageByUUID(String uuid) {
        return imageRepository.findImageByUUID(uuid);
    }

    public byte[] getImageBytes(String uuid) {

        assertNotBlank(uuid, "uuid cannot be blank");

        byte[] imageBytes = null;

        try {
            Image image = imageRepository.findImageByUUID(uuid);

            Path imagePath = Paths.get(applicationProperties.getFilesUploadPath() + image.getUuidStr() + ".jpg");

            imageBytes = Files.readAllBytes(imagePath);
        }
        catch (Exception e) {

            LOGGER.error("Failed to read image file!" + e.getMessage());

            try {

                Image image = imageRepository.findImageByUUID("no-image");

                Path imagePath = Paths.get(applicationProperties.getFilesUploadPath() + image.getUuidStr() + ".jpg");

                imageBytes = Files.readAllBytes(imagePath);

            }
            catch (IOException e1) {
                throw new IllegalArgumentException("The image is not available.", e1);
            }

        }

        return imageBytes;
    }

    public byte[] getTempImageBytes(String uuid) {

        byte[] imageBytes = null;

        try {

            String tempDir = FileUtils.getTempDirectoryPath();

            Path imagePath = Paths.get(tempDir + File.separator + uuid);
            imageBytes = Files.readAllBytes(imagePath);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return imageBytes;
    }

    public String uploadImageFromUrl(String url) {
        byte[] bytes = null;

        try {
            URL imageUrl = new URL(url);

            InputStream stream = imageUrl.openStream();
            bytes = IOUtils.toByteArray(stream);
            return this.uploadImage(bytes);
        }
        catch (Exception e) {
            LOGGER.error("Failed to upload image!", e);
            throw new IllegalArgumentException("The image is not available.");
        }

    }

    public String uploadImage(byte[] bytes) {

        notNull(bytes, "file is mandatory");

        try {

            String tempDir = FileUtils.getTempDirectoryPath();

            String tempImageUUID = UUID.randomUUID().toString();

            File imageFile = new File(tempDir + File.separator + tempImageUUID);

            FileUtils.writeByteArrayToFile(imageFile, bytes);

            LOGGER.debug("Uploaded image path: " + imageFile.getAbsolutePath());

            return tempImageUUID;
        }
        catch (Exception e) {
            LOGGER.error("Failed to upload image!", e);
            throw new IllegalArgumentException("The image is not available.");
        }
    }

    @Transactional
    public Image saveImage(String tempUUID) {

        assertNotBlank(tempUUID, "tempUUID cannot be blank");

        try {

            Image image = imageRepository.save(new Image());

            String tempDir = FileUtils.getTempDirectoryPath();

            File tempImageFile = new File(tempDir + File.separator + tempUUID);

            BufferedImage src = ImageIO.read(tempImageFile);

            boolean shouldScale = false;
            if (src.getWidth() > Image.MAX_WIDTH || src.getHeight() > Image.MAX_HEIGHT
                || src.getWidth() > Image.MAX_HEIGHT || src.getHeight() > Image.MAX_WIDTH) {
                shouldScale = true;
            }

            if (shouldScale) {
                BufferedImage resized = Scalr.resize(src, Method.QUALITY, Image.MAX_WIDTH, Image.MAX_HEIGHT);
                ImageIO.write(resized, "jpg", tempImageFile);
            }

            File localImageFile = new File(applicationProperties.getFilesUploadPath() + File.separator + image.getUuid() + ".jpg");

            FileUtils.copyFile(tempImageFile, localImageFile);

            return image;
        }
        catch (Exception e) {
            LOGGER.error("Failed to upload image!", e);
            throw new IllegalArgumentException("The image is not available.");
        }
    }

}
