package com.lubgo.app.controllers;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lubgo.app.dto.CommentDTO;
import com.lubgo.app.dto.LinkDTO;
import com.lubgo.app.dto.PostDTO;
import com.lubgo.app.dto.PostsDTO;
import com.lubgo.app.model.Image;
import com.lubgo.app.model.Post;
import com.lubgo.app.services.ImageService;
import com.lubgo.app.services.LinkService;
import com.lubgo.app.services.PostService;

/**
 *
 * REST service for posts - allows to update, create and search for posts for
 * the currently logged in user.
 *
 */
@Controller
@RequestMapping("post")
public class PostController {

    Logger LOGGER = Logger.getLogger(PostController.class);

    @Autowired
    private PostService postService;

    @Autowired
    ImageService imageService;

    @Autowired
    LinkService linkService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST)
    public PostDTO savePost(Principal principal, @RequestBody PostDTO post) {

        Image postImage = null;
        Image linkImage = null;

        if (post.getImage().getUuid() != null) {
            postImage = imageService.saveImage(post.getImage().getUuid());
        }

        if (post.getLink().getImage() != null && post.getLink().getImage().getUuid() != null) {
            linkImage = imageService.saveImage(post.getLink().getImage().getUuid());
        }

        Post savedPost =
            postService.savePost(principal.getName(),
                                 post.getId(),
                                 post.getDate(),
                                 post.getTime(),
                                 post.getDescription(),
                                 post.getLink().getTitle(),
                                 post.getLink().getDescription(),
                                 post.getLink().getUrl(),
                                 linkImage != null ? linkImage.getUuidStr() : null,
                                 postImage != null ? postImage.getUuidStr() : null);
        return PostDTO.mapFromPostEntity(savedPost);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/like", method = RequestMethod.POST)
    public void likePost(Principal principal, @RequestBody Long postId) {
        postService.likePost(principal.getName(), postId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/unlike", method = RequestMethod.POST)
    public void unlikePost(Principal principal, @RequestBody Long postId) {
        postService.unlikePost(principal.getName(), postId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.DELETE)
    public void deletePost(@RequestBody Long deletedPostId) {
        postService.deletePost(deletedPostId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/deleteComment/{postId}/{commentId}", method = RequestMethod.DELETE)
    public void deleteComment(@PathVariable("postId") Long deletedPostId, @PathVariable("commentId") Long deleteCommentId) {
        postService.deleteComment(deletedPostId, deleteCommentId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/deleteArticleComment/{articleId}/{commentId}", method = RequestMethod.DELETE)
    public void deleteArticleComment(@PathVariable("articleId") Long articleId, @PathVariable("commentId") Long commentId) {
        postService.deleteArticleComment(articleId, commentId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/deletePigeonComment/{pigeonId}/{commentId}", method = RequestMethod.DELETE)
    public void deletePigeonComment(@PathVariable("pigeonId") Long pigeonId, @PathVariable("commentId") Long commentId) {
        postService.deletePigeonComment(pigeonId, commentId);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        LOGGER.error(exc.getMessage(), exc);
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public PostsDTO getPosts(Principal principal) {
        List<Post> posts = postService.getNewsFeed(principal.getName());
        List<PostDTO> postsDTO =
            posts.stream()
                 .map(s -> new PostDTO(s.getId(),
                                       s.getDate(),
                                       s.getTime(),
                                       s.getDescription(),
                                       s.getUser(),
                                       s.getChildren(),
                                       s.getLikes(),
                                       s.getLink(),
                                       s.getImage()))
                 .collect(Collectors.toList());
        return posts != null ? new PostsDTO(postsDTO) : null;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/posts/{uuid}", method = RequestMethod.GET)
    public PostsDTO getPosts(Principal principal, @PathVariable("uuid") String uuid) {
        List<Post> posts = postService.getPostsByUUID(uuid);
        List<PostDTO> postsDTO =
            posts.stream()
                 .map(s -> new PostDTO(s.getId(),
                                       s.getDate(),
                                       s.getTime(),
                                       s.getDescription(),
                                       s.getUser(),
                                       s.getChildren(),
                                       s.getLikes(),
                                       s.getLink(),
                                       s.getImage()))
                 .collect(Collectors.toList());
        return posts != null ? new PostsDTO(postsDTO) : null;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PostDTO getPost(Principal principal, @PathVariable("id") Long id) {
        Post post = postService.getPost(id);
        return post != null ? PostDTO.mapFromPostEntity(post) : null;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/saveComment", method = RequestMethod.POST)
    public PostDTO saveComment(Principal principal, @RequestBody CommentDTO comment) {
        Post savedComment = postService.saveComment(principal.getName(), comment.getPostId(), comment.getComment());
        return PostDTO.mapFromPostEntity(savedComment);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/linkPreview", method = RequestMethod.POST)
    public LinkDTO linkPreview(Principal principal, @RequestBody String url) {
        return LinkDTO.mapFromLinkEntity(linkService.linkPreview(url));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/savePigeonComment/{pigeonId}", method = RequestMethod.POST)
    public PostDTO savePigeonComment(Principal principal, @PathVariable("pigeonId") Long pigeonId, @RequestBody CommentDTO comment) {
        Post savedComment = postService.savePigeonComment(principal.getName(), pigeonId, comment.getComment());
        // template.convertAndSend("/topic/message/" + savedComment.getParent().getUser().getUuidStr(), "{message:
        // 'PEDJAAAAAAA'}");
        return PostDTO.mapFromPostEntity(savedComment);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/saveArticleComment/{articleId}", method = RequestMethod.POST)
    public PostDTO saveArticleComment(Principal principal, @PathVariable("articleId") Long articleId, @RequestBody CommentDTO comment) {
        Post savedComment = postService.saveArticleComment(principal.getName(), articleId, comment.getComment());
        // template.convertAndSend("/topic/message/" + savedComment.getParent().getUser().getUuidStr(), "{message:
        // 'PEDJAAAAAAA'}");
        return PostDTO.mapFromPostEntity(savedComment);
    }

}
