package com.lubgo.app.controllers;

import java.security.Principal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lubgo.app.dto.MessageDTO;
import com.lubgo.app.model.Message;
import com.lubgo.app.services.MessageService;

@Controller
@RequestMapping("/message")
public class MessageController {

    private static final Logger LOGGER = Logger.getLogger(MessageController.class);

    @Autowired
    MessageService messageService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST)
    public MessageDTO saveMessage(Principal principal, @RequestBody MessageDTO message) {
        Message savedMessage = messageService.saveMessage(principal.getName(),
                                                          message.getConversationId(),
                                                          message.getBody(),
                                                          null);
        return MessageDTO.mapFromMessageEntity(savedMessage);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public MessageDTO getMesssages(@PathVariable("id") Long id, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {

        // List<Message> messages = messageService.findMessageByConversationId(id, page, size);
        // for (Message message : messages) {}
        // return new MessagesDTO(MessageDTO.mapFromConversationsEntities(conversations));

        return null;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteMessage(@RequestBody Long deletedMessageId) {
        messageService.deleteMessageById(deletedMessageId);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        LOGGER.error(exc.getMessage(), exc);
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
