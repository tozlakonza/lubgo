package com.lubgo.app.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lubgo.app.dto.PigeonAchievementDTO;
import com.lubgo.app.dto.PigeonDTO;
import com.lubgo.app.dto.PigeonsDTO;
import com.lubgo.app.enums.PigeonAchivementType;
import com.lubgo.app.model.Image;
import com.lubgo.app.model.Pigeon;
import com.lubgo.app.model.PigeonAchievement;
import com.lubgo.app.model.User;
import com.lubgo.app.services.ImageService;
import com.lubgo.app.services.PigeonAchievementService;
import com.lubgo.app.services.PigeonService;
import com.lubgo.app.services.UserService;

@Controller
@RequestMapping("/pigeon")
public class PigeonController {

    private static final Logger LOGGER = Logger.getLogger(PigeonController.class);

    @Autowired
    PigeonService pigeonService;

    @Autowired
    PigeonAchievementService achievementService;

    @Autowired
    UserService userService;

    @Autowired
    ImageService imageService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public PigeonDTO getPigeonInfo(@PathVariable("uuid") String uuid) {
        Pigeon pigeon = pigeonService.findPigeonByUUID(uuid);
        return PigeonDTO.mapFromPigeonEntity(pigeon);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/pigeons", method = RequestMethod.GET)
    public PigeonsDTO getPigeons(
        @RequestParam("uuid") String uuid,
        @RequestParam("firstResult") Integer firstResult,
        @RequestParam("maxResult") Integer maxResult) {
        User user = userService.findUserByUUID(uuid);
        List<Pigeon> pigeons = pigeonService.findPigeonByUserId(user.getEmail(), firstResult, maxResult);
        return new PigeonsDTO(PigeonDTO.mapFromPigeonsEntities(pigeons));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/count/{uuid}", method = RequestMethod.GET)
    public Long getPigeonsCount(Principal principal, @PathVariable("uuid") String uuid) {
        User user = userService.findUserByUUID(uuid);
        Long pigeonsCount = pigeonService.countPigeonByUserId(user.getId());
        return pigeonsCount;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST)
    public PigeonDTO savePigeon(Principal principal, @RequestBody PigeonDTO pigeon) {

        Image image = null;

        if (pigeon.getImage().getUuid() != null) {
            image = imageService.saveImage(pigeon.getImage().getUuid());
        }

        Pigeon savedPigeon =
            pigeonService.savePigeon(principal.getName(),
                                     pigeon.getId(),
                                     pigeon.isMainstream(),
                                     pigeon.getBreed(),
                                     pigeon.getRing(),
                                     pigeon.getName(),
                                     pigeon.getSex(),
                                     pigeon.getOwner(),
                                     pigeon.getOrigin(),
                                     pigeon.getDynasty(),
                                     pigeon.getDateOfBirth(),
                                     pigeon.getDateAcquired(),
                                     pigeon.getDateDisposedOf(),
                                     pigeon.getDescription(),
                                     image != null ? image.getUuidStr() : null);

        List<PigeonAchievement> achievements = null;

        if (pigeon.getAchievements() != null && !pigeon.getAchievements().isEmpty()) {
            achievements = new ArrayList<PigeonAchievement>();
            for (PigeonAchievementDTO achievement : pigeon.getAchievements()) {
                achievements.add(achievementService.savePigeonAchievement(achievement.getId(),
                                                                          PigeonAchivementType.valueOf(achievement.getType()),
                                                                          achievement.getPlace(),
                                                                          achievement.getRace(),
                                                                          achievement.getBirds(),
                                                                          achievement.getYear(),
                                                                          achievement.getDescription(),
                                                                          savedPigeon));
            }
        }

        savedPigeon.setAchievements(achievements);

        return PigeonDTO.mapFromPigeonEntity(savedPigeon);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/like", method = RequestMethod.POST)
    public void likePigeon(Principal principal, @RequestBody Long pigeonId) {
        pigeonService.likePigeon(principal.getName(), pigeonId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/unlike", method = RequestMethod.POST)
    public void unlikePigeon(Principal principal, @RequestBody Long pigeonId) {
        pigeonService.unlikePigeon(principal.getName(), pigeonId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.DELETE)
    public void deletePigeon(@RequestBody Long deletedPigeonId) {
        pigeonService.deletePigeon(deletedPigeonId);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        LOGGER.error(exc.getMessage(), exc);
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
