package com.lubgo.app.controllers;

import com.lubgo.app.model.Connection;
import com.lubgo.app.model.User;
import com.lubgo.app.services.ConnectionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lubgo.app.dto.UserInfoDTO;
import com.lubgo.app.model.SearchResult;
import com.lubgo.app.services.UserService;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {

    private static final Logger LOGGER = Logger.getLogger(SearchController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private ConnectionService connectionService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public SearchResult<UserInfoDTO> findUsers(
            @RequestParam("keyword") String keyword,
            @RequestParam("firstResult") Integer firstResult,
            @RequestParam("maxResult") Integer maxResult, Principal principal) {

        User user = userService.findUserByEmail(principal.getName());
        LOGGER.debug("Search for keyword=" + keyword + ", firstResult=" + firstResult + ", maxResult=" + maxResult);
        SearchResult<UserInfoDTO> result =  userService.findUsers(keyword, firstResult, maxResult);
        List<User> friends = connectionService.findFriends(user.getId());
        for (UserInfoDTO userDTO : result.getResult()) {
            for (User friend : friends) {
                if (userDTO.getUuid().equals(friend.getUuidStr())) {
                    userDTO.setFriend(true);
                }
            }
        }
        return result;
    }
}
