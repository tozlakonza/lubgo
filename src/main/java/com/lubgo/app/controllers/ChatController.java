package com.lubgo.app.controllers;

import java.util.Date;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.stereotype.Controller;

import com.lubgo.app.dto.MessageDTO;

@Controller
public class ChatController {

    // @MessageMapping("/chat/{conversationId}")
    // @SendTo("/topic/message/{conversationId}")
    public MessageDTO sendMessage(@DestinationVariable Long conversationId, MessageDTO message) {
        message.setTime(new Date());
        return message;
    }

}