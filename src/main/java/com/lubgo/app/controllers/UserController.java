package com.lubgo.app.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lubgo.app.dto.ConnectionDTO;
import com.lubgo.app.dto.ConnectionsDTO;
import com.lubgo.app.dto.ImageDTO;
import com.lubgo.app.dto.NewUserDTO;
import com.lubgo.app.dto.NotificationDTO;
import com.lubgo.app.dto.UpdateUserDTO;
import com.lubgo.app.dto.UserBasicDTO;
import com.lubgo.app.dto.UserInfoDTO;
import com.lubgo.app.dto.UsersBasicDTO;
import com.lubgo.app.enums.ConnectionStatus;
import com.lubgo.app.model.Activity;
import com.lubgo.app.model.Connection;
import com.lubgo.app.model.User;
import com.lubgo.app.security.LubgoUserDetails;
import com.lubgo.app.services.ConnectionService;
import com.lubgo.app.services.NotificationService;
import com.lubgo.app.services.UserService;
import com.lubgo.app.services.util.NotificationUtils;

@Controller
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = Logger.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private ConnectionService connectionService;

    @Autowired
    private NotificationService notificationService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public UserInfoDTO getUserInfo(Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        return user != null ? new UserInfoDTO(user.getEmail(),
                                              user.getFirstName(),
                                              user.getLastName(),
                                              user.getPhone(),
                                              user.getUuidStr(),
                                              user.getImage())
                        : null;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/full", method = RequestMethod.GET)
    public UserInfoDTO getUserInfoFull(Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        return user != null ? new UserInfoDTO(user.getEmail(),
                                              user.getFirstName(),
                                              user.getLastName(),
                                              user.getPhone(),
                                              user.getAlliance(),
                                              user.getLocation(),
                                              user.getClub(),
                                              user.getAutobiography(),
                                              user.getUuidStr(),
                                              user.getImage())
                        : null;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/full/{uuid}", method = RequestMethod.GET)
    public UserInfoDTO getUserInfoFull(Principal principal, @PathVariable("uuid") String uuid) {
        User user = userService.findUserByUUID(uuid);
        UserInfoDTO result = user != null ? new UserInfoDTO(user.getEmail(),
                                                            user.getFirstName(),
                                                            user.getLastName(),
                                                            user.getPhone(),
                                                            user.getAlliance(),
                                                            user.getLocation(),
                                                            user.getClub(),
                                                            user.getAutobiography(),
                                                            user.getUuidStr(),
                                                            user.getImage())
                        : null;
        if (result != null) {
            ConnectionStatus connectionStatus;
            if (user.getEmail().equals(principal.getName())) {
                connectionStatus = ConnectionStatus.NOT_APPLICABLE;
            }
            else {
                if (principal instanceof LubgoUserDetails) {
                    connectionStatus = userService.getConnectionStatus(((LubgoUserDetails)principal).getUuid(), uuid);
                }
                else {
                    User user2 = userService.findUserByEmail(principal.getName());
                    connectionStatus = userService.getConnectionStatus(user2.getUuidStr(), uuid);
                }
            }
            result.setConnectionStatus(connectionStatus.toString());
        }
        return result;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/connections", method = RequestMethod.GET)
    public UsersBasicDTO getConnectionsByUUID(Principal principal) {

        User user = userService.findUserByEmail(principal.getName());

        List<Connection> connections = connectionService.findAcceptedConnectionsByUserId(user.getId());

        List<UserBasicDTO> userDTOs = connections.stream()
                                                 .map(c -> user.equals(c.getTo())
                                                                 ? UserBasicDTO.mapFromUserEntity(c.getFrom())
                                                                 : UserBasicDTO.mapFromUserEntity(c.getTo()))
                                                 .collect(Collectors.toList());

        UsersBasicDTO usersBasicDTO = new UsersBasicDTO(userDTOs);

        return usersBasicDTO;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/connections/{uuid}", method = RequestMethod.GET)
    public ConnectionsDTO getConnectionsByUUID(Principal principal, @PathVariable("uuid") String uuid) {

        User user = userService.findUserByUUID(uuid);

        List<Connection> connections = connectionService.findAcceptedConnectionsByUserId(user.getId());

        List<ConnectionDTO> connectionDTOs =
            connections.stream()
                       .map(m -> user.equals(m.getTo())
                                       ? ConnectionDTO.mapFromConnectionEntity(m.getFrom(), m)
                                       : ConnectionDTO.mapFromConnectionEntity(m.getTo(), m))
                       .collect(Collectors.toList());

        ConnectionsDTO connectionsDTO = new ConnectionsDTO(connectionDTOs);

        return connectionsDTO;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/connections/count/{uuid}", method = RequestMethod.GET)
    public Long getConnectionsCount(Principal principal, @PathVariable("uuid") String uuid) {

        User user = userService.findUserByUUID(uuid);

        Long connectionsCount = connectionService.countAcceptedConnectionsByUserId(user.getId());

        return connectionsCount;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/newConnectionRequests", method = RequestMethod.GET)
    public ConnectionsDTO getNewConnectionRequests(Principal principal) {

        User user = userService.findUserByEmail(principal.getName());

        List<Connection> connections = connectionService.findPendingConnectionsByUserId(user.getId());

        List<ConnectionDTO> connectionDTOs =
            connections.stream()
                       .filter(m -> user.equals(m.getTo()))
                       .map(m -> ConnectionDTO.mapFromConnectionEntity(m.getFrom(), m))
                       .collect(Collectors.toList());

        ConnectionsDTO connectionsDTO = new ConnectionsDTO(connectionDTOs);

        return connectionsDTO;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public UserInfoDTO getUserInfo(Principal principal, @PathVariable("uuid") String uuid) {
        User user = userService.findUserByUUID(uuid);

        UserInfoDTO result = user != null ? new UserInfoDTO(user.getEmail(),
                                                            user.getFirstName(),
                                                            user.getLastName(),
                                                            user.getPhone(),
                                                            user.getAlliance(),
                                                            user.getLocation(),
                                                            user.getClub(),
                                                            user.getAutobiography(),
                                                            user.getUuidStr(),
                                                            user.getImage())
                        : null;
        if (result != null) {
            result.setLoggedInUser(principal.getName().equals(user.getEmail()));
        }
        return result;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public void changePassword(Principal principal, @RequestBody UpdateUserDTO userDTO) {
        userService.changeUserPassword(principal.getName(), userDTO.getOldPassword(), userDTO.getNewPassword());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/updateBasicInfo", method = RequestMethod.POST)
    public void updateBasicInfo(Principal principal, @RequestBody UpdateUserDTO userDTO) {
        userService.updateBasicInfo(principal.getName(), userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(), userDTO.getPhone());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/updateProfilePicture", method = RequestMethod.POST)
    public void updateProfilePicture(Principal principal, @RequestBody ImageDTO imageDTO) {
        userService.updateProfilePicture(principal.getName(), imageDTO.getUuid());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/updateOtherInfo", method = RequestMethod.POST)
    public void updateOtherInfo(Principal principal, @RequestBody UpdateUserDTO userDTO) {
        userService.updateOtherInfo(principal.getName(), userDTO.getAlliance(), userDTO.getLocation(), userDTO.getClub(), userDTO.getAutobiography());
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST)
    public void createUser(@RequestBody NewUserDTO user) {
        userService.createUser(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPlainTextPassword());
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(params = "email", method = RequestMethod.POST)
    public void resetPassword(@RequestParam String email) {
        userService.findUserByEmail(email);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "makeConnectionRequest/{toUserUuid}", method = RequestMethod.GET)
    public void makeConnectionRequest(Principal principal, @PathVariable("toUserUuid") String toUserUuid) {
        connectionService.saveConnectionRequest(principal.getName(), toUserUuid);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "acceptConnectionRequest/{fromUserUuid}", method = RequestMethod.GET)
    public void acceptConnectionRequest(Principal principal, @PathVariable("fromUserUuid") String fromUserUuid) {
        User user = userService.findUserByEmail(principal.getName());
        connectionService.acceptConnection(fromUserUuid, user.getUuidStr());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "rejectConnectionRequest/{fromUserUuid}", method = RequestMethod.GET)
    public void rejectConnectionRequest(Principal principal, @PathVariable("fromUserUuid") String fromUserUuid) {
        User user = userService.findUserByEmail(principal.getName());
        connectionService.rejectConnection(fromUserUuid, user.getUuidStr());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "notifications", method = RequestMethod.GET)
    public List<NotificationDTO> getNotifications(Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        List<Activity> activities = notificationService.findNewActivities(user.getId());
        return convertToDTO(activities);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "allNotifications", method = RequestMethod.GET)
    public List<NotificationDTO> getAllNotifications(Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        // TODO implement paging
        List<Activity> activities = notificationService.findAllActivities(user.getId(), 1, 10);
        return convertToDTO(activities);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "notifications/markread/{notificationId}", method = RequestMethod.GET)
    public void markNotificationRead(Principal principal, @PathVariable("notificationId") Long notificationId) {
        User user = userService.findUserByEmail(principal.getName());
        notificationService.markRead(notificationId, user.getId());
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "notifications/markread", method = RequestMethod.GET)
    public void markAllNotificationsRead(Principal principal) {
        User user = userService.findUserByEmail(principal.getName());
        notificationService.markAllNotificationsRead(user.getId());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        LOGGER.error(exc.getMessage(), exc);
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }

    private List<NotificationDTO> convertToDTO(List<Activity> activities) {
        List<NotificationDTO> result = new ArrayList<>(activities.size());
        for (Activity activity : activities) {
            NotificationDTO notificationDTO = new NotificationDTO();
            notificationDTO.setType(activity.getType().toString());
            notificationDTO.setLink(NotificationUtils.getNotificationLink(activity));
            notificationDTO.setMessage(NotificationUtils.getMessage(activity));
            notificationDTO.setId(activity.getId());
            result.add(notificationDTO);
        }
        return result;
    }
}
