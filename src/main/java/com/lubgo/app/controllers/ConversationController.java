package com.lubgo.app.controllers;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lubgo.app.dto.ConversationDTO;
import com.lubgo.app.dto.ConversationsDTO;
import com.lubgo.app.model.Conversation;
import com.lubgo.app.services.ConversationService;

@Controller
@RequestMapping("/conversation")
public class ConversationController {

    private static final Logger LOGGER = Logger.getLogger(ConversationController.class);

    @Autowired
    ConversationService conversationService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST)
    public ConversationDTO saveConversation(Principal principal, @RequestBody ConversationDTO conversationDTO) {

        List<Long> participantsIds = conversationDTO.getParticipants()
                                                    .stream()
                                                    .map(p -> p.getId())
                                                    .collect(Collectors.toList());

        Conversation conversation = conversationService.findConversationByParticipantsIds(principal.getName(), participantsIds);

        if (conversation == null) {
            conversation = conversationService.saveConversation(principal.getName(),
                                                                null,
                                                                conversationDTO.getParticipants()
                                                                               .stream()
                                                                               .map(r -> r.getEmail())
                                                                               .collect(Collectors.toSet()));
        }

        // Prepare participants
        conversation.setParticipants(conversation.getParticipants()
                                                 .stream()
                                                 .filter(m -> !m.getEmail().equalsIgnoreCase(principal.getName()))
                                                 .collect(Collectors.toSet()));

        return ConversationDTO.mapFromConversationEntity(conversation);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ConversationDTO getConversation(@PathVariable("id") Long id) {
        Conversation conversation = conversationService.findConversationById(id);
        return ConversationDTO.mapFromConversationEntity(conversation);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public ConversationsDTO getConversations(Principal principal, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {

        List<Conversation> conversations = conversationService.findConversationUserEmail(principal.getName(), page, size);

        for (Conversation conversation : conversations) {
            conversation.setParticipants(conversation.getParticipants()
                                                     .stream()
                                                     .filter(m -> !m.getEmail().equalsIgnoreCase(principal.getName()))
                                                     .collect(Collectors.toSet()));
        }

        return new ConversationsDTO(ConversationDTO.mapFromConversationsEntities(conversations));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteConversation(@RequestBody Long deletedConversationId) {
        conversationService.deleteConversation(deletedConversationId);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        LOGGER.error(exc.getMessage(), exc);
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
