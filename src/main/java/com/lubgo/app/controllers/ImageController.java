package com.lubgo.app.controllers;

import java.io.IOException;
import java.security.Principal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lubgo.app.dto.ImageDTO;
import com.lubgo.app.model.Image;
import com.lubgo.app.services.ImageService;

@RestController
@RequestMapping("/images")
public class ImageController {

    private static final Logger LOGGER = Logger.getLogger(ImageController.class);

    @Autowired
    ImageService imageService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST)
    public ImageDTO uploadImage(Principal principal,
        @RequestParam("file") MultipartFile file,
        @RequestParam("save") Boolean save)
            throws IOException {
        String uploadImageUUID = imageService.uploadImage(file.getBytes());
        if (save) {
            Image image = imageService.saveImage(uploadImageUUID);
            uploadImageUUID = image.getUuidStr();
        }
        return new ImageDTO(uploadImageUUID);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public HttpEntity<byte[]> getImage(@PathVariable("uuid") String uuid) {

        byte[] imageBytes = imageService.getImageBytes(uuid);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setContentLength(imageBytes.length);
        return new HttpEntity<byte[]>(imageBytes, headers);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/temp/{uuid}", method = RequestMethod.GET)
    public HttpEntity<byte[]> getTempImage(@PathVariable("uuid") String uuid) {

        byte[] imageBytes = imageService.getTempImageBytes(uuid);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setContentLength(imageBytes.length);
        return new HttpEntity<byte[]>(imageBytes, headers);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        LOGGER.error(exc.getMessage(), exc);
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
