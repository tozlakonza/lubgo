package com.lubgo.app.controllers;

import java.security.Principal;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lubgo.app.dto.ArticleDTO;
import com.lubgo.app.dto.ArticlesDTO;
import com.lubgo.app.model.Article;
import com.lubgo.app.model.User;
import com.lubgo.app.services.ArticleService;
import com.lubgo.app.services.PostService;
import com.lubgo.app.services.UserService;

@Controller
@RequestMapping("/article")
public class ArticleController {

    private static final Logger LOGGER = Logger.getLogger(ArticleController.class);

    @Autowired
    ArticleService articleService;

    @Autowired
    PostService postService;

    @Autowired
    UserService userService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/articles", method = RequestMethod.GET)
    public ArticlesDTO getArticles(
        @RequestParam("uuid") String uuid,
        @RequestParam("firstResult") Integer firstResult,
        @RequestParam("maxResult") Integer maxResult) {
        User user = userService.findUserByUUID(uuid);
        List<Article> articles = articleService.findArticlesByAuthorEmail(user.getEmail(), firstResult, maxResult);
        return new ArticlesDTO(ArticleDTO.mapFromArticlesEntities(articles));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/article/{uuid}", method = RequestMethod.GET)
    public ArticleDTO getArticleInfo(Principal principal, @PathVariable("uuid") String uuid) {
        Article article = articleService.findArticleByUUID(uuid);
        return ArticleDTO.mapFromArticleEntity(article);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/count/{uuid}", method = RequestMethod.GET)
    public Long getArticlesCount(@PathVariable("uuid") String uuid) {
        User user = userService.findUserByUUID(uuid);
        Long articlesCount = articleService.countArticlesByAuthorId(user.getId());
        return articlesCount;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST)
    public ArticleDTO saveArticle(Principal principal, @RequestBody ArticleDTO article) {
        Article savedArticle = articleService.saveArticle(principal.getName(),
                                                          article.getId(),
                                                          article.getTitle(),
                                                          article.getBody(),
                                                          article.getDate());
        return ArticleDTO.mapFromArticleEntity(savedArticle);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/like", method = RequestMethod.POST)
    public void likeArticle(Principal principal, @RequestBody Long articleId) {
        articleService.likeArticle(principal.getName(), articleId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/unlike", method = RequestMethod.POST)
    public void unlikeArticle(Principal principal, @RequestBody Long articleId) {
        articleService.unlikeArticle(principal.getName(), articleId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteArticle(@RequestBody Long deletedArticleId) {
        articleService.deleteArticle(deletedArticleId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/deleteComment/{articleId}/{commentId}", method = RequestMethod.DELETE)
    public void deletePost(@PathVariable("articleId") Long deletedCommentId, @PathVariable("commentId") Long deletedArticleId) {
        postService.deleteComment(deletedArticleId, deletedCommentId);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        LOGGER.error(exc.getMessage(), exc);
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
