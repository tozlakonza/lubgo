package com.lubgo.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.lubgo.app.dao.util.JPAQueryResultUtil;
import com.lubgo.app.enums.ActivityType;
import com.lubgo.app.model.Activity;
import com.lubgo.app.model.Image;

@Repository
public class ActivityRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Image findImageByUUID(String uuid) {
        List<Image> images = entityManager.createNamedQuery(Image.FIND_BY_UUID, Image.class)
                                          .setParameter("uuid", uuid)
                                          .getResultList();

        return images.size() == 1 ? images.get(0) : null;
    }

    public Image save(Image image) {
        return entityManager.merge(image);
    }

    public Activity save(Activity activity) {
        entityManager.persist(activity);
        return activity;
    }

    public Activity update(Activity activity) {
        return entityManager.merge(activity);
    }

    public void delete(Activity activity) {
        entityManager.remove(activity);
    }

    public Activity findById(Long activityId) {
        return entityManager.find(Activity.class, activityId);
    }

    @SuppressWarnings("unchecked")
    public List<Activity> findNewForUser(Long userId) {
        return entityManager.createNamedQuery(Activity.FIND_NEW_FOR_USER)
                            .setParameter("userIdPiped", "%|" + userId + "|%")
                            .getResultList();
    }

    public Activity findByParams(ActivityType type, Long fromId, Long toId) {
        Query query = entityManager.createNamedQuery(Activity.FIND_BY_PARAMS)
                                   .setParameter("type", type)
                                   .setParameter("fromId", fromId)
                                   .setParameter("toId", toId);
        Activity result = (Activity)JPAQueryResultUtil.getSingleResultOrNull(query);
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<Activity> findAllForUser(Long userId, int pageNumber, int pageSize) {
        Query query = entityManager.createNamedQuery(Activity.FIND_ALL_FOR_USER).setParameter("userIdPiped", "%|" + userId + "|%");
        query.setMaxResults(pageSize);
        query.setFirstResult(pageNumber * pageSize - (pageSize));
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Activity> findActivitiesByPostId(Long postId) {
        return entityManager.createNamedQuery(Activity.FIND_BY_POST_ID)
                            .setParameter("postId", postId)
                            .getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Activity> findActivitiesByPigeonId(Long pigeonId) {
        return entityManager.createNamedQuery(Activity.FIND_BY_PIGEON_ID)
                            .setParameter("pigeonId", pigeonId)
                            .getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Activity> findActivitiesByArticleId(Long articleId) {
        return entityManager.createNamedQuery(Activity.FIND_BY_ARTICLE_ID)
                            .setParameter("articleId", articleId)
                            .getResultList();
    }
}
