package com.lubgo.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.lubgo.app.model.Image;

@Repository
public class ImageRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Image findImageByUUID(String uuid) {
        List<Image> images = entityManager.createNamedQuery(Image.FIND_BY_UUID, Image.class)
                                          .setParameter("uuid", uuid)
                                          .getResultList();

        return images.size() == 1 ? images.get(0) : null;
    }

    public Image save(Image image) {
        return entityManager.merge(image);
    }
}
