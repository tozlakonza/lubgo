package com.lubgo.app.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.lubgo.app.model.Link;

/**
 *
 * Repository class for the Link entity
 *
 */
@Repository
public class LinkRepository {
    @PersistenceContext
    EntityManager entityManager;

    public Link save(Link link) {
        return entityManager.merge(link);
    }
}
