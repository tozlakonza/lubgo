package com.lubgo.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.lubgo.app.model.Message;

@Repository
public class MessageRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Message findMessageById(Long id) {
        Message message = entityManager.createNamedQuery(Message.FIND_BY_ID, Message.class)
                                       .setParameter("id", id)
                                       .getSingleResult();
        return message;
    }

    public List<Message> findMessagesByConversationId(Long id) {
        List<Message> messages = entityManager.createNamedQuery(Message.FIND_BY_CONVERSATION_ID, Message.class)
                                              .setParameter("conversationId", id)
                                              .getResultList();
        return messages;
    }

    public Message save(Message message) {
        return entityManager.merge(message);
    }

    public void deleteById(Long id) {
        Message message = findMessageById(id);
        entityManager.remove(message);
    }

    public void delete(Message message) {
        entityManager.remove(message);
    }
}
