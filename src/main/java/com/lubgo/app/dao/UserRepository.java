package com.lubgo.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.lubgo.app.model.User;

/**
 *
 * Repository class for the User entity
 *
 */
@Repository
public class UserRepository {

    @PersistenceContext
    private EntityManager em;

    public User findUserByEmail(String email) {
        List<User> users = em.createNamedQuery(User.FIND_BY_EMAIL, User.class)
                             .setParameter("email", email)
                             .getResultList();
        return users.size() == 1 ? users.get(0) : null;
    }

    public User save(User user) {
        return em.merge(user);
    }

    public boolean isEmailAvailable(String email) {

        List<User> users = em.createNamedQuery(User.FIND_BY_EMAIL, User.class)
                             .setParameter("email", email)
                             .getResultList();

        return users.isEmpty();
    }

    public List<User> findUsers(String term) {
        List<User> users = em.createNamedQuery(User.FIND, User.class)
                             .setParameter("term", term)
                             .getResultList();

        return users;
    }

    public List<User> findUsers(String term, Integer firstResult, Integer maxResult) {
        List<User> users = em.createNamedQuery(User.FIND, User.class)
                             .setFirstResult(firstResult)
                             .setMaxResults(maxResult)
                             .setParameter("term", term)
                             .getResultList();

        return users;
    }

    public User findUserByUUID(String uuid) {
        User user = em.createNamedQuery(User.FIND_BY_UUID, User.class)
                      .setParameter("uuid", uuid)
                      .getSingleResult();
        return user;
    }

    public boolean checkIfAlreadyConnected(String fromUuid, String toUuid) {
        Number count =
            (Number)em.createNamedQuery(User.CHECK_IF_CONNECTED).setParameter("fromId", fromUuid).setParameter("toId", toUuid).getSingleResult();
        if (count.intValue() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

}
