package com.lubgo.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.lubgo.app.model.PigeonAchievement;

@Repository
public class PigeonAchievementRepository {

    @PersistenceContext
    private EntityManager em;

    public PigeonAchievement save(PigeonAchievement achievement) {
        return em.merge(achievement);
    }

    public PigeonAchievement findById(Long id) {
        List<PigeonAchievement> achievements = em.createNamedQuery(PigeonAchievement.FIND_BY_ID, PigeonAchievement.class)
                                                            .setParameter("id", id)
                                                            .getResultList();

        return achievements.size() == 1 ? achievements.get(0) : null;
    }

}
