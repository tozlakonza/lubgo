package com.lubgo.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.lubgo.app.model.Pigeon;
import com.lubgo.app.model.User;

@Repository
public class PigeonRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Pigeon findPigeonById(Long id) {
        Pigeon pigeon = entityManager.createNamedQuery(Pigeon.FIND_BY_ID, Pigeon.class)
                                     .setParameter("id", id)
                                     .getSingleResult();
        return pigeon;
    }

    public Pigeon findPigeonByUUID(String uuid) {
        Pigeon pigeon = entityManager.createNamedQuery(Pigeon.FIND_BY_UUID, Pigeon.class)
                                     .setParameter("uuid", uuid)
                                     .getSingleResult();
        return pigeon;
    }

    public List<Pigeon> findPigeonsByUserId(Long userId) {
        List<Pigeon> pigeons = entityManager.createNamedQuery(Pigeon.FIND_BY_USER_ID, Pigeon.class)
                                            .setParameter("userId", userId)
                                            .getResultList();
        return pigeons;
    }

    public List<Pigeon> findPigeonsByUserId(Long userId, Integer firstResult, Integer maxResult) {
        List<Pigeon> pigeons = entityManager.createNamedQuery(Pigeon.FIND_BY_USER_ID, Pigeon.class)
                                            .setParameter("userId", userId)
                                            .setFirstResult(firstResult)
                                            .setMaxResults(maxResult)
                                            .getResultList();
        return pigeons;
    }

    public Long countPigeonsByUserId(Long id) {
        return entityManager.createNamedQuery(Pigeon.COUNT_BY_OWNER_ID, Long.class)
                            .setParameter("ownerId", id)
                            .getSingleResult();
    }

    public Pigeon save(Pigeon pigeon) {
        return entityManager.merge(pigeon);
    }

    public void delete(Pigeon pigeon) {
        entityManager.remove(pigeon);
    }

    public void likePigeon(User user, Long id) {
        Pigeon pigeon = findPigeonById(id);
        if (!pigeon.getLikes().contains(user)) {
            pigeon.getLikes().add(user);
            entityManager.merge(pigeon);
        }
    }

    public void unlikePigeon(User user, Long id) {
        Pigeon pigeon = findPigeonById(id);
        if (pigeon.getLikes().remove(user)) {
            entityManager.merge(pigeon);
        }
    }

}
