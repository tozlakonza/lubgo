package com.lubgo.app.dao;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.lubgo.app.model.Article;
import com.lubgo.app.model.Pigeon;
import com.lubgo.app.model.Post;
import com.lubgo.app.model.User;

/**
 *
 * Repository class for the Post entity
 *
 */
@Repository
public class PostRepository {

    @PersistenceContext
    EntityManager entityManager;

    public void delete(Long deletedPostId) {
        Post delete = entityManager.find(Post.class, deletedPostId);
        entityManager.remove(delete);
    }

    public Post findPostById(Long id) {
        return entityManager.find(Post.class, id);
    }

    public Post save(Post post) {
        return entityManager.merge(post);
    }

    public List<Post> findPostByUser(Long userId) {
        List<Post> posts = entityManager.createNamedQuery(Post.FIND_BY_USER, Post.class)
                                        .setParameter("userId", userId)
                                        .getResultList();
        return posts;
    }

    public List<Post> findPostFeedByUser(Long userId) {
        List<Post> posts = entityManager.createNamedQuery(Post.FIND_FEED_BY_USER, Post.class)
                                        .setParameter("userId", userId)
                                        .getResultList();
        return posts;
    }

    public void deleteComment(Post post, Post comment) {
        comment.setParent(null);
        post.getChildren().remove(comment);
        entityManager.persist(comment);
        entityManager.persist(post);
        entityManager.remove(comment);
    }

    public void deleteArticleComment(Article article, Post comment) {
        comment.setParent(null);
        article.getComments().remove(comment);
        entityManager.persist(comment);
        entityManager.persist(article);
        entityManager.remove(comment);
    }

    public void deletePigeonComment(Pigeon pigeon, Post comment) {
        comment.setParent(null);
        pigeon.getComments().remove(comment);
        entityManager.persist(comment);
        entityManager.persist(pigeon);
        entityManager.remove(comment);
    }

    public Post saveComment(User user, Post post, String comment) {
        Date now = new Date();
        Post com = new Post(user, now, new Time(now.getTime()), comment, null, null);
        com.setParent(post);
        post.getChildren().add(com);
        entityManager.persist(com);
        entityManager.merge(post);
        return com;
    }

    public Post saveArticleComment(User user, Article article, String comment) {
        Date now = new Date();
        Post com = new Post(user, now, new Time(now.getTime()), comment, null, null);
        com.setArticle(article);
        article.getComments().add(com);
        entityManager.persist(com);
        entityManager.merge(article);
        return com;
    }

    public Post savePigeonComment(User user, Pigeon pigeon, String comment) {
        Date now = new Date();
        Post com = new Post(user, now, new Time(now.getTime()), comment, null, null);
        com.setPigeon(pigeon);
        pigeon.getComments().add(com);
        entityManager.persist(com);
        entityManager.merge(pigeon);
        return com;
    }

    public void deleteCommentById(Long id) {
        delete(id);
    }

    public void likePost(User user, Long id) {
        Post post = findPostById(id);
        if (!post.getLikes().contains(user)) {
            post.getLikes().add(user);
            entityManager.merge(post);
        }
    }

    public void unlikePost(User user, Long id) {
        Post post = findPostById(id);
        if (post.getLikes().remove(user)) {
            entityManager.merge(post);
        }
    }
}
