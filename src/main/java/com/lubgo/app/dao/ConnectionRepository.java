package com.lubgo.app.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.lubgo.app.model.User;
import org.springframework.stereotype.Repository;

import com.lubgo.app.dao.util.JPAQueryResultUtil;
import com.lubgo.app.model.Connection;

/**
 * Created by Pedja on 1/9/2016.
 */
@Repository
public class ConnectionRepository {

    @PersistenceContext
    private EntityManager em;

    public Connection saveConnection(Connection cr) {
        if (cr.getId() == null) {
            cr.setCreatedOn(new Date());
            em.persist(cr);
        }
        else {
            em.merge(cr);
        }
        return cr;
    }

    @SuppressWarnings("unchecked")
    public List<Connection> findPendingConnectionRequestsForUser(Long userId) {
        return em.createNamedQuery(Connection.FIND_PENDING_BY_TO_USER_ID).setParameter("userId", userId).getResultList();
    }

    public Connection findPendingConnection(Long fromId, Long toId) {
        Query q = em.createNamedQuery(Connection.FIND_PENDING_BY_TO_USER_ID_AND_FROM_USER_ID);
        q.setParameter("fromUserId", fromId);
        q.setParameter("toUserId", toId);
        Connection cr = (Connection)JPAQueryResultUtil.getSingleResultOrNull(q);
        return cr;
    }

    /**
     * Find pending connection request from one user to another
     * 
     * @param email
     *            From user email
     * @param uuid
     *            To user uuid
     * @return pending connection request if exists, null otherwise
     */
    public Connection findPendingConnectionRequest(String fromUuid, String toUuid) {
        Query q = em.createNamedQuery(Connection.FIND_PENDING_BY_UUIDS);
        q.setParameter("fromUuid", fromUuid);
        q.setParameter("toUuid", toUuid);
        Connection cr = (Connection)JPAQueryResultUtil.getSingleResultOrNull(q);
        return cr;
    }

    public boolean canCreateNewConnection(Long fromId, Long toId) {
        Query q = em.createNamedQuery(Connection.FIND_PENDING_AND_ACCEPTED_COUNT);
        q.setParameter("fromId", fromId);
        q.setParameter("toId", toId);
        Number pendingAndAcceptedCount = (Number)JPAQueryResultUtil.getSingleResultOrNull(q);
        return pendingAndAcceptedCount.intValue() == 0;
    }

    @SuppressWarnings("unchecked")
    public List<Connection> findPendingConnectionsByUserId(Long userId) {
        return em.createNamedQuery(Connection.FIND_PENDING_BY_TO_USER_ID)
                 .setParameter("userId", userId)
                 .getResultList();
    }

    public List<Connection> findAcceptedConnectionsByUserId(Long userId) {
        return em.createNamedQuery(Connection.FIND_ACCEPTED_BY_USER_ID, Connection.class)
                 .setParameter("userId", userId)
                 .getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Connection> findDeclinedConnectionsByUserId(Long userId) {
        return em.createNamedQuery(Connection.FIND_DECLINED_BY_USER_ID)
                 .setParameter("userId", userId)
                 .getResultList();
    }

    public Long countAcceptedConnectionsByUserId(Long id) {
        return em.createNamedQuery(Connection.COUNT_ACCEPTED_BY_USER_ID, Long.class)
                 .setParameter("userId", id)
                 .getSingleResult();
    }

    public List<User> findFriendsFrom(Long userId) {
        return em.createNamedQuery(Connection.FIND_FRIENDS_FROM).setParameter("userId", userId).getResultList();
    }

    public List<User> findFriendsTo(Long userId) {
        return em.createNamedQuery(Connection.FIND_FRIENDS_TO).setParameter("userId", userId).getResultList();
    }
}
