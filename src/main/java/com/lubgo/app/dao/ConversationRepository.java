package com.lubgo.app.dao;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.lubgo.app.model.Conversation;

@Repository
public class ConversationRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Conversation findConversationById(Long id) {
        Conversation conversation = entityManager.createNamedQuery(Conversation.FIND_BY_ID, Conversation.class)
                                                 .setParameter("id", id)
                                                 .getSingleResult();
        return conversation;
    }

    public List<Conversation> findConversationsByUserId(Long id, int page, int size) {
        List<Conversation> conversations = entityManager.createNamedQuery(Conversation.FIND_BY_USER_ID, Conversation.class)
                                                        .setParameter("userId", id)
                                                        .setFirstResult((page - 1) * size)
                                                        .setMaxResults(size)
                                                        .getResultList();
        return conversations;
    }

    public Conversation save(Conversation conversation) {
        return entityManager.merge(conversation);
    }

    public void deleteById(Long id) {
        Conversation conversation = findConversationById(id);
        entityManager.remove(conversation);
    }

    public void delete(Conversation conversation) {
        entityManager.remove(conversation);
    }

    @SuppressWarnings("unchecked")
    public Long findConversationByParticipantsIds(List<Long> ids) {

        String condition = "select conversation_id from conversations_users group by conversation_id having ";

        for (Long id : ids) {
            condition += " sum(case when participants_id = " + id + " then 1 else 0 end) > 0 and ";
        }

        condition += " sum(case when participants_id not in ( ";

        for (Long id : ids) {
            condition += id + ",";
        }
        condition = condition.substring(0, condition.length() - 1);

        condition += " ) then 1 else 0 end) = 0";

        List<BigInteger> conversationIds = entityManager.createNativeQuery(condition).getResultList();

        return conversationIds.size() == 0 ? null : Long.parseLong(conversationIds.get(0).toString());
    }
}
