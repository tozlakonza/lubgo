package com.lubgo.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.lubgo.app.model.Article;
import com.lubgo.app.model.User;

@Repository
public class ArticleRepository {

    @PersistenceContext
    private EntityManager em;

    public Article findArticleByUUID(String uuid) {
        List<Article> articles = em.createNamedQuery(Article.FIND_BY_UUID, Article.class)
                                   .setParameter("uuid", uuid)
                                   .getResultList();

        return articles.size() == 1 ? articles.get(0) : null;
    }

    public Article findArticleById(Long id) {
        List<Article> articles = em.createNamedQuery(Article.FIND_BY_ID, Article.class)
                                   .setParameter("id", id)
                                   .getResultList();

        return articles.size() == 1 ? articles.get(0) : null;
    }

    public Long countArticlesByAuthorId(Long id) {
        return em.createNamedQuery(Article.COUNT_BY_AUTHOR_ID, Long.class)
                 .setParameter("id", id)
                 .getSingleResult();
    }

    public List<Article> findArticlesByAuthorId(Long id) {
        List<Article> articles = em.createNamedQuery(Article.FIND_BY_AUTHOR_ID, Article.class)
                                   .setParameter("id", id)
                                   .getResultList();
        return articles;
    }

    public List<Article> findArticlesByAuthorId(Long id, Integer firstResult, Integer maxResult) {
        List<Article> articles = em.createNamedQuery(Article.FIND_BY_AUTHOR_ID, Article.class)
                                   .setFirstResult(firstResult)
                                   .setMaxResults(maxResult)
                                   .setParameter("id", id)
                                   .getResultList();
        return articles;
    }

    public Article save(Article article) {
        return em.merge(article);
    }

    public void delete(Long deletedArticleId) {
        Article delete = em.find(Article.class, deletedArticleId);
        em.remove(delete);
    }

    public void likeArticle(User user, Long id) {
        Article article = findArticleById(id);
        if (!article.getLikes().contains(user)) {
            article.getLikes().add(user);
            em.merge(article);
        }
    }

    public void unlikeArticle(User user, Long id) {
        Article article = findArticleById(id);
        if (article.getLikes().remove(user)) {
            em.merge(article);
        }
    }
}
