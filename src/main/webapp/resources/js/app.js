var lubgoApplication = angular.module('lubGoApp', ['ui.router', 'spring-security-csrf-token-interceptor', 'pascalprecht.translate', 'ngFileUpload', 'ui.bootstrap', 'textAngular', 'chat', 'socket', 'ngCookies', 'ngtimeago', 'toaster', 'multipleSelect', 'infinite-scroll']);

angular.module("chat", []);
angular.module("socket", []);

lubgoApplication.config(function($stateProvider, $urlRouterProvider) {
	    
	    $urlRouterProvider.otherwise('/home');
	    
	    $stateProvider
	        .state('home', {
	            url: '/home',
	            templateUrl: 'views/home.html',
	            controller: 'HomeCtrl',
	            controllerAs: 'home',
	            resolve: {
	            	postsData: ['PostService', function(PostService) {
	            		return PostService.getPosts();
	                }]
	            }
	        })
	        .state('profile', {
	        	url: '/profile/:uuid',
	            templateUrl: 'views/profile.html',
	            controller: 'ProfileCtrl',
	            controllerAs: 'profile',
	        })
	        .state('profile.timeline', {
	        	url: '/timeline',
	            templateUrl: 'views/nested/profile/timeline.html',
	            controller: 'TimelineCtrl',
	            controllerAs: 'timeline',
	            resolve: {
	            	postsData: ['PostService', '$stateParams', function(PostService, $stateParams) {
	            		return PostService.getPostsByUUID($stateParams.uuid);
	                }]
	            }
	        })
	        .state('profile.pigeons', {
	        	url: '/pigeons',
	            templateUrl: 'views/nested/profile/pigeons_pigeon_list.html',
	            controller: 'PigeonsCtrl',
	            controllerAs: 'pigeons'
	        })
	        .state('profile.newpigeon', {
	        	url: '/newpigeon',
	            templateUrl: 'views/nested/profile/pigeons_new_pigeon.html',
	            controller: 'PigeonCtrl',
	            controllerAs: 'pigeon'
	        })
	        .state('profile.pigeon', {
	        	url: '/pigeon/:pigeonuuid',
	            templateUrl: 'views/nested/profile/pigeons_pigeon_profile.html',
	            controller: 'PigeonProfileCtrl',
	            controllerAs: 'pigeonProfile',
	            resolve: {
	            	pigeonData: ['PigeonService', '$stateParams', function(PigeonService, $stateParams) {
	            		return PigeonService.getPigeonByUUID($stateParams.pigeonuuid);
	                }]
	            }
	        })
	        .state('profile.articles', {
	        	url: '/articles',
	            templateUrl: 'views/nested/profile/articles.html',
	            controller: 'ArticlesCtrl',
	            controllerAs: 'articles'
	        })
	        .state('profile.newarticle', {
	        	url: '/article',
	            templateUrl: 'views/nested/profile/articles_new_article.html',
	            controller: 'ArticleCtrl',
	            controllerAs: 'article'
	        })
	        .state('profile.articlepreview', {
	        	url: '/article/:articleUuid',
	            templateUrl: 'views/nested/profile/articles_article_preview.html',
	            controller: 'ArticleCtrl',
	            controllerAs: 'article',
	        })
	        .state('profile.friends', {
	        	url: '/friends',
	            templateUrl: 'views/nested/profile/friends.html',
	            controller: 'FriendsCtrl',
	            controllerAs: 'friends',
	            resolve: {
	            	friendsData: ['UserService', '$stateParams', function(UserService, $stateParams) {
	            		return UserService.getConnections($stateParams.uuid);
	                }]
	            }
	        })
	        .state('profile.activity', {
	        	url: '/activity',
	            templateUrl: 'views/nested/profile/activity.html',
	            controller: 'ActivityCtrl',
	            controllerAs: 'activity'
	        })
	        .state('profile.about', {
	        	url: '/about',
	            templateUrl: 'views/nested/profile/about.html',
	            controller: 'AboutCtrl',
	            controllerAs: 'about'
	        })
			.state('searchResults', {
				url: '/searchResults/:searchTerm',
				templateUrl: 'views/searchResults.html',
				controller: 'SearchCtrl',
				controllerAs: 'searchCtrl'
			})
			.state('auctions', {
				url: '/auctions',
	            templateUrl: 'views/404.html',
	            controller: 'AuctionsCtrl',
	            controllerAs: 'auctions'
			})
			.state('lostFound', {
				url: '/lostFound',
	            templateUrl: 'views/404.html',
	            controller: 'LostFoundCtrl',
	            controllerAs: 'lostFound'
			})
			.state('events', {
				url: '/events',
	            templateUrl: 'views/404.html',
	            controller: 'EventsCtrl',
	            controllerAs: 'events'
			})
			.state('help', {
				url: '/help',
	            templateUrl: 'views/404.html',
	            controller: 'HelpCtrl',
	            controllerAs: 'help'
			})
			.state('conversations', {
				url: '/conversations',
	            templateUrl: 'views/conversations.html',
	            controller: 'ConversationsCtrl',
	            controllerAs: 'conversations',
	            resolve: {
	            	conversationsData: ['ConversationService', function(ConversationService) {
	            		return ConversationService.getConversations();
	                }],
	            	friendsData: ['UserService', '$stateParams', function(UserService) {
	            		return UserService.getMyConnections();
	                }]
	            }
			})
			.state('notifications', {
				url: '/notifications',
	            templateUrl: 'views/notifications.html',
	            controller: 'NotificationsCtrl',
	            controllerAs: 'notificationsCtrl'
			});
	    
	    $urlRouterProvider.otherwise('/home');
});

lubgoApplication.config(function ($translateProvider) {
	  
	$translateProvider.translations('en', englishTranslations);
	$translateProvider.translations('rs', serbianTranslations);
	  
	$translateProvider.preferredLanguage('en');
	$translateProvider.useSanitizeValueStrategy('escape');
	
});

lubgoApplication.config(function($provide) {
    $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions) { // $delegate is the taOptions we are decorating
    	taOptions.toolbar = [
    	                     ['h1', 'h2', 'h3'],
    	                     ['bold', 'italics', 'underline', 'strikeThrough'],
    	                     ['justifyLeft', 'justifyCenter', 'justifyRight'],
    	                     ['ul', 'ol'],
    	                     ['indent', 'outdent'],
    	                     ['insertImage','insertLink', 'insertVideo']
    	                 ];
        return taOptions;
    }]);
})


lubgoApplication.run(['$window', function($window) {
    window.fbAsyncInit = function() {
        FB.init({
            appId: '451870318344256',
            version: 'v2.5',
            xfbml: true
        });
    };

    (function(d) {
        // load the Facebook javascript SDK

        var js,
            id = 'facebook-jssdk',
            ref = d.getElementsByTagName('script')[0];

        if (d.getElementById(id)) {
            return;
        }

        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";

        ref.parentNode.insertBefore(js, ref);

    }(document));
}]);

