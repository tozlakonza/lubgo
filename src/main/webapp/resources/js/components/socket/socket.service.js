(function(angular, SockJS, Stomp, _, undefined) {
  angular.module("socket").service("SocketService", function($q, $timeout) {
    
	var service = {}; 
    var listener = $q.defer();
    var socket = {
      client: null,
      stomp: null
    }
    
    service.receive = function() {
      return listener.promise;
    };
    
    service.send = function(data) {
      socket.stomp.send(
    		  			service.configuration.CHAT_BROKER, 
    		  		 	{priority: service.configuration.PRIORITY}, 
    		  		 	JSON.stringify(data)
    		  		   );
    };
    
    var reconnect = function() {
      $timeout(function() {
        initialize(service.configuration);
      }, this.RECONNECT_TIMEOUT);
    };
    
    var startListener = function() {
      socket.stomp.subscribe(service.configuration.CHAT_TOPIC, function(data) {
        //listener.notify(JSON.parse(data.body));
        listener.notify(data.body);
      });
    };
    
    service.initialize = function(config) {
      service.configuration = config;
      socket.client = new SockJS(service.configuration.SOCKET_URL);
      socket.stomp = Stomp.over(socket.client);
      socket.stomp.connect({}, startListener);
      socket.stomp.onclose = reconnect;
    };
    
    return service;
  });
})(angular, SockJS, Stomp, _);