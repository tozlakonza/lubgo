(function(angular) {
  angular.module("chat").controller("ChatCtrl", function($scope, SocketService) {
    
	$scope.messages = [];
    $scope.message = "";
    $scope.max = 140;
    
    var config = {
		RECONNECT_TIMEOUT : 5000,
	    SOCKET_URL : "/chat", // StompEndpoint
	    CHAT_TOPIC : "/topic/message/2", // @SendTo
	    CHAT_BROKER : "/app/chat/2", // @MessageMapping
	    PRIORITY : 10
    }
    
    SocketService.initialize(config);
    
    $scope.sendMessage = function() {
      SocketService.send({message : $scope.message});
      $scope.message = "";
    };
    
    SocketService.receive().then(null, null, function(message) {
    	$scope.messages.push(message);
	});
    
  });
})(angular);