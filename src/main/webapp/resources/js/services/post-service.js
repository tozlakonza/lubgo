lubgoApplication.service('PostService', ['$http', '$q', function($http, $q) {
    return {
        getPosts: function() {
            var deferred = $q.defer();

            $http.get('/post/', {
                    params: {}
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject('Error retrieving list of posts');
                    }
                });

            return deferred.promise;
        },
        
        getPostsByUUID: function(uuid) {
            var deferred = $q.defer();

            $http.get('/post/posts/' + uuid, {
                    params: {}
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject('Error retrieving list of posts');
                    }
                });

            return deferred.promise;
        },

        getPost: function(postId) {
            var deferred = $q.defer();

            $http.get('/post/' + postId)
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject('Error retrieving list of posts');
                    }
                });

            return deferred.promise;
        },

        savePost: function(dirtyPost) {
            var deferred = $q.defer();

            $http({
                    method: 'POST',
                    url: '/post',
                    data: dirtyPost,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject("Error saving post: " + response.data);
                    }
                });

            return deferred.promise;
        },
        
        saveComment: function(comment) {
            var deferred = $q.defer();

            $http({
                    method: 'POST',
                    url: '/post/saveComment',
                    data: comment,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject("Error saving comment: " + response.data);
                    }
                });

            return deferred.promise;
        },
        
        savePigeonComment: function(comment) {
            var deferred = $q.defer();

            $http({
                    method: 'POST',
                    url: '/post/savePigeonComment/' + comment.pigeonId,
                    data: comment,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject("Error saving comment: " + response.data);
                    }
                });

            return deferred.promise;
        },
        
        saveArticleComment: function(comment) {
            var deferred = $q.defer();

            $http({
                    method: 'POST',
                    url: '/post/saveArticleComment/' + comment.articleId,
                    data: comment,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject("Error saving comment: " + response.data);
                    }
                });

            return deferred.promise;
        },

        deletePost: function(postId) {
            var deferred = $q.defer();

            $http({
                    method: 'DELETE',
                    url: '/post',
                    data: postId,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    } else {
                        deferred.reject("Error deleting post: " + response.data);
                    }
                });

            return deferred.promise;
        },

        deleteComment: function(postId, commentId) {
            var deferred = $q.defer();

            $http({
                    method: 'DELETE',
                    url: '/post/deleteComment/' + postId + '/' + commentId,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    } else {
                        deferred.reject("Error deleting comment: " + response.data);
                    }
                });

            return deferred.promise;
        },
        
        deletePigeonComment: function(pigeonId, commentId) {
            var deferred = $q.defer();

            $http({
                    method: 'DELETE',
                    url: '/post/deletePigeonComment/' + pigeonId + '/' + commentId,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    } else {
                        deferred.reject("Error deleting comment: " + response.data);
                    }
                });

            return deferred.promise;
        },
        
        deleteArticleComment: function(articleId, commentId) {
            var deferred = $q.defer();

            $http({
                    method: 'DELETE',
                    url: '/post/deleteArticleComment/' + articleId + '/' + commentId,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    } else {
                        deferred.reject("Error deleting comment: " + response.data);
                    }
                });

            return deferred.promise;
        },

        likePost: function(postId) {
            var deferred = $q.defer();

            $http({
                    method: 'POST',
                    url: '/post/like',
                    data: postId,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject("Error deleting post: " + response.data);
                    }
                });

            return deferred.promise;
        },

        unLikePost: function(postId) {
            var deferred = $q.defer();

            $http({
                    method: 'POST',
                    url: '/post/unlike',
                    data: postId,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject("Error deleting post: " + response.data);
                    }
                });

            return deferred.promise;
        },
        
        linkPreview: function(url) {
        	 var deferred = $q.defer();

             $http({
                     method: 'POST',
                     url: '/post/linkPreview',
                    data: url,
                     headers: {
                         "Content-Type": "application/json",
                         "Accept": "text/plain, application/json"
                     }
                 })
                 .then(function(response) {
                     if (response.status == 200) {
                         deferred.resolve(response.data);
                     } else {
                         deferred.reject("Error deleting post: " + response.data);
                     }
                 });

             return deferred.promise;
        	
        }
    }
}]);