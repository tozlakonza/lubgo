lubgoApplication.service('MessageService', ['$http','$q', function($http, $q) {
        return {
        	getMessagesByConversationId: function(id, page, size) {
                var deferred = $q.defer();

                $http.get('/message/' + id + "?page=" + page + "&size=" + size)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            sendMessage: function(conversationId, text) {

                var deferred = $q.defer();

                var service = $http({
                        method: 'POST',
                        url: '/message',
                        data: {
                        	conversationId: conversationId,
                        	body: text
                        },
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "text/plain, application/json"
                        }
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        } else {
                            deferred.reject(response.text);
                        }
                    });

                return deferred.promise;

            }
        };
}]);