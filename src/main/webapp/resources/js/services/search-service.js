lubgoApplication.service('SearchService', ['$http','$q', function($http, $q) {
        return {
        	search: function (keyword, firstResult, maxResult) {
                var deferred = $q.defer();
                term = encodeURIComponent(keyword);
                $http.get('/search?keyword=' + keyword + "&firstResult=" + firstResult + "&maxResult=" + maxResult)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                    });

                return deferred.promise;
            }
        };
    }]);