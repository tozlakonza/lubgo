lubgoApplication.service('ConfirmationDialogService', ['$uibModal', function($uibModal) {

    this.openModal = function(titleKey, descriptionKey, submitKey, cancelKey) {
        var instance = $uibModal.open({
            backdrop: false,
            template: '<div class="modal-content"> <div class="modal-header"> <button type="button" class="close" ng-click="cancel()" aria-label="Close"> <span aria-hidden="true">×</span></button> <h4 class="modal-title" translate="' + titleKey + '"> </h4> </div> <div class="modal-body"> <p translate="' + descriptionKey + '"></p> </div> <div class="modal-footer"> <button type="button" class="btn btn-default pull-left" ng-click="cancel()" translate="' + cancelKey + '"">Cancel</button> <button type="button" class="btn btn-primary" ng-click="ok()" translate="' + submitKey + '"></button> </div> </div>',
            controller: ['$scope', '$modalInstance', function(s, m) {
                s.ok = function() {
                    m.close();
                };
                s.cancel = function() {
                    m.dismiss();
                };
            }]
        });

        return instance.result;
    }
}]);