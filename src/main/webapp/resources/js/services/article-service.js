lubgoApplication.service('ArticleService', ['$http', '$q', 'PostService', function($http, $q, PostService) {
    return {
        
        getArticlesByUUID: function(uuid, firstResult, maxResult) {
            var deferred = $q.defer();

            $http.get('/article/articles?uuid=' + uuid + '&firstResult=' + firstResult + '&maxResult=' + maxResult)
            .then(function(response) {
                if (response.status == 200) {
                    deferred.resolve(response.data);
                } else {
                    deferred.reject('Error retrieving list of articles');
                }
            });

            return deferred.promise;
        },
        
        countArticlesByUUID: function(uuid) {
            var deferred = $q.defer();

            $http.get('/article/count/' + uuid)
            .then(function(response) {
                if (response.status == 200) {
                    deferred.resolve(response.data);
                } else {
                    deferred.reject('Error retrieving list of articles');
                }
            });

            return deferred.promise;
        },
        
        getArticleByUUID : function(uuid){
        	
        	var deferred = $q.defer();

            $http.get('/article/article/' + uuid)
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject('Error retrieving an article');
                    }
                });

            return deferred.promise;
        },

        saveArticle: function(dirtyArticle) {
            var deferred = $q.defer();

            $http({
                    method: 'POST',
                    url: '/article',
                    data: dirtyArticle,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject("Error saving article: " + response.data);
                    }
                });

            return deferred.promise;
        },
        
        saveComment: function(comment) {
            return PostService.saveComment(comment);
        },

        deleteArticle: function(articleId) {
            return PostService.deletePost(articleId);
        },

        deleteComment: function(articleId, commentId) {
            return PostService.deleteComment(articleId, commentId);
        },

        likeArticle: function(articleId) {
            var deferred = $q.defer();

            $http({
                    method: 'POST',
                    url: '/article/like',
                    data: articleId,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject("Error deleting post: " + response.data);
                    }
                });

            return deferred.promise;
        },

        unLikeArticle: function(articleId) {
            var deferred = $q.defer();

            $http({
                    method: 'POST',
                    url: '/article/unlike',
                    data: articleId,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    } else {
                        deferred.reject("Error deleting post: " + response.data);
                    }
                });

            return deferred.promise;
        },
        
        deleteArticle: function(articleId) {
        	
            var deferred = $q.defer();

            $http({
                    method: 'DELETE',
                    url: '/article',
                    data: articleId,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function(response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    } else {
                        deferred.reject("Error deleting article: " + response.data);
                    }
                });

            return deferred.promise;
        }
        
    }
}]);