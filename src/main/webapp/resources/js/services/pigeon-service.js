lubgoApplication.service('PigeonService', ['$http','$q', function($http, $q) {
        return {
        	getPigeonsByUUID: function(uuid, firstResult, maxResult) {
                var deferred = $q.defer();

                $http.get('/pigeon/pigeons?uuid=' + uuid + '&firstResult=' + firstResult + '&maxResult=' + maxResult)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            
            countPigeonsByUUID: function(uuid) {
                var deferred = $q.defer();

                $http.get('/pigeon/count/' + uuid)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            
            getPigeonByUUID: function(uuid) {
                var deferred = $q.defer();

                $http.get('/pigeon/' + uuid)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            createNewPigeon: function(pigeon) {

                var deferred = $q.defer();

                var service = $http({
                        method: 'POST',
                        url: '/pigeon',
                        data: pigeon,
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "text/plain, application/json"
                        }
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        } else {
                            deferred.reject(response.text);
                        }
                    });

                return deferred.promise;

            },
            likePigeon: function(pigeonId) {
                var deferred = $q.defer();

                $http({
                        method: 'POST',
                        url: '/pigeon/like',
                        data: pigeonId,
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "text/plain, application/json"
                        }
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        } else {
                            deferred.reject("Error deleting post: " + response.data);
                        }
                    });

                return deferred.promise;
            },

            unLikePigeon: function(pigeonId) {
                var deferred = $q.defer();

                $http({
                        method: 'POST',
                        url: '/pigeon/unlike',
                        data: pigeonId,
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "text/plain, application/json"
                        }
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        } else {
                            deferred.reject("Error deleting post: " + response.data);
                        }
                    });

                return deferred.promise;
            },
            
            savePigeonComment: function(comment) {
                var deferred = $q.defer();

                $http({
                        method: 'POST',
                        url: '/pigeon/saveComment',
                        data: comment,
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "text/plain, application/json"
                        }
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        } else {
                            deferred.reject("Error saving comment: " + response.data);
                        }
                    });

                return deferred.promise;
            },
            
            deletePigeon: function(pigeonId) {
            	
                var deferred = $q.defer();

                $http({
                        method: 'DELETE',
                        url: '/pigeon',
                        data: pigeonId,
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "text/plain, application/json"
                        }
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            deferred.resolve();
                        } else {
                            deferred.reject("Error deleting pigeon: " + response.data);
                        }
                    });

                return deferred.promise;
            }
        };
}]);