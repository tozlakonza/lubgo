lubgoApplication.service('UserService', ['$http','$q', function($http, $q) {
        return {
            getUserInfo: function() {
                var deferred = $q.defer();

                $http.get('/user')
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            getUserInfoFull: function() {
                var deferred = $q.defer();

                $http.get('/user/full')
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            getMyConnections: function() {
                var deferred = $q.defer();

                $http.get('/user/connections')
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving connections');
                        }
                });

                return deferred.promise;
            },
            getConnections: function(uuid) {
                var deferred = $q.defer();

                $http.get('/user/connections/' + uuid)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving connections');
                        }
                });

                return deferred.promise;
            },
            
            countFriendsByUUID: function(uuid) {
                var deferred = $q.defer();

                $http.get('/user/connections/count/' + uuid)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving connections');
                        }
                });

                return deferred.promise;
            },
            
            getNewConnectionRequests: function() {
                var deferred = $q.defer();

                $http.get('/user/newConnectionRequests/')
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving new connection requests');
                        }
                });

                return deferred.promise;
            },
            getUserInfoFullById: function(uuid) {
                var deferred = $q.defer();

                $http.get('/user/full/' + uuid)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            updateBasicInfo: function(userFirstName, userLastName, userEmail, userPhone) {
                var deferred = $q.defer();
                
                $http({
                    method: 'POST',
                    url: '/user/updateBasicInfo',
                    data: {
                    	firstName: userFirstName,
                    	lastName: userLastName,
                    	email: userEmail,
                    	phone: userPhone
                    },
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain"
                    }
                })	
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject('Error updating basic info');
                    }
                });

                return deferred.promise;
            },
            updateProfilePicture: function(uuid) {
                var deferred = $q.defer();
                
                $http({
                    method: 'POST',
                    url: '/user/updateProfilePicture',
                    data: {
                    	uuid: uuid,
                    },
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain"
                    }
                })	
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject('Error updating profile picture');
                    }
                });

                return deferred.promise;
            },
            changePassword: function(userOldPassword, userNewPassword) {
                var deferred = $q.defer();
                
                $http({
                    method: 'POST',
                    url: '/user/changePassword',
                    data: {
                    	oldPassword: userOldPassword,
                    	newPassword: userNewPassword
                    },
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain"
                    }
                })	
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject('Error changing password');
                    }
                });

                return deferred.promise;
            },
            updateOtherInfo: function(userAlliance, userLocation, userClub, userAutobiography) {
                var deferred = $q.defer();
                
                $http({
                    method: 'POST',
                    url: '/user/updateOtherInfo',
                    data: {
                    	alliance: userAlliance,
                    	location: userLocation,
                    	club: userClub,
                    	autobiography: userAutobiography
                    },
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain"
                    }
                })	
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject('Error updating basic info');
                    }
                });

                return deferred.promise;
            },
            getUserDetails: function(uuid) {
                var deferred = $q.defer();

                $http.get('/user/' + uuid)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                    });

                return deferred.promise;
            },
            logout: function () {
                $http({
                    method: 'POST',
                    url: '/logout'
                })
                .then(function (response) {
                    if (response.status == 200) {
                    	window.location.reload();
                    }
                    else {
                        console.log("Logout failed!");
                    }
                });
            },
            makeConnectionRequest: function(toUserUuid) {
                var deferred = $q.defer();
                $http.get('/user/makeConnectionRequest/' + toUserUuid)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error making connection request');
                        }
                    });
                return deferred.promise;
            }, 
            acceptConnectionRequest: function(fromUserUuid) {
                var deferred = $q.defer();
                $http.get('/user/acceptConnectionRequest/' + fromUserUuid)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error accepting connection request');
                        }
                    });
                return deferred.promise;
            },
            rejectConnectionRequest: function(fromUserUuid) {
                var deferred = $q.defer();
                $http.get('/user/rejectConnectionRequest/' + fromUserUuid)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error rejecting connection request');
                        }
                    });
                return deferred.promise;
            },
            getNotifications: function() {
                var deferred = $q.defer();
                $http.get('/user/notifications')
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error getting notifications');
                        }
                    });
                return deferred.promise;
            },
            getAllNotifications: function() {
                var deferred = $q.defer();
                $http.get('/user/allNotifications')
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error getting notifications');
                        }
                    });
                return deferred.promise;
            },
            markNotificationRead: function(notificationId) {
                var deferred = $q.defer();
                $http.get('/user/notifications/markread/' + notificationId)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error marking notification read');
                        }
                    });
                return deferred.promise;
            },
            markAllNotificationsRead: function() {
                var deferred = $q.defer();
                $http.get('/user/notifications/markread')
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error marking all notifications read');
                        }
                    });
                return deferred.promise;
            }
        };
    }]);