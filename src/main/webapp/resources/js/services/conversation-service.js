lubgoApplication.service('ConversationService', ['$http','$q', function($http, $q) {
        return {
        	getConversations: function(page, size) {
                var deferred = $q.defer();
                
                if(page == undefined){
                	page = 1;
                }
                
                if(size == undefined){
                	size = 10;
                }

                $http.get('/conversation?page=' + page + '&size=' + size)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            saveConversation: function(newMessageRecepients){
            	
            	var deferred = $q.defer();

                var service = $http({
                        method: 'POST',
                        url: '/conversation',
                        data: {
                        	participants: newMessageRecepients
                        },
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "text/plain, application/json"
                        }
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        } else {
                            deferred.reject(response.text);
                        }
                    });

                return deferred.promise;
            },
            getConversationById: function(id) {
                var deferred = $q.defer();

                $http.get('/conversation/' + id)
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving conversation info');
                        }
                });

                return deferred.promise;
            },
            startNewConversation: function() {

                var deferred = $q.defer();

                var service = $http({
                        method: 'POST',
                        url: '/conversation',
                        data: {
                        	to : id
                        },
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "text/plain, application/json"
                        }
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        } else {
                            deferred.reject(response.text);
                        }
                    });

                return deferred.promise;

            },
            
            deleteConversation: function(conversationId) {
            	
                var deferred = $q.defer();

                $http({
                        method: 'DELETE',
                        url: '/conversation',
                        data: conversationId,
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "text/plain, application/json"
                        }
                    })
                    .then(function(response) {
                        if (response.status == 200) {
                            deferred.resolve();
                        } else {
                            deferred.reject("Error deleting post: " + response.data);
                        }
                    });

                return deferred.promise;
            }
            
        };
}]);