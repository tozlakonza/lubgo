lubgoApplication.controller('ConversationsCtrl', ['$scope', '$log', 'conversationsData', 'friendsData', 'ConversationService', 'ConfirmationDialogService', '$location', 'MessageService', 
                                                  function ($scope, $log, conversationsData, friendsData, ConversationService, ConfirmationDialogService, $location, MessageService) {
	
	var _self = this;
	
	_self.conversations = conversationsData.conversations;
	_self.friends = friendsData.users;
	_self.currentConversation = _self.conversations[0];
	_self.newMessageRecepients = [];
	_self.isNewMessage = false;
	
	this.refresh = function(){
		ConversationService.getConversations().then(function(data) {
			_self.conversations = data.conversations;
        });
	}
	
	this.selectConversation = function(conversation, index){
		_self.currentConversation = conversation;
		_self.currentConversationIndex = index;
		_self.isNewMessage = false;
	}
	
	this.send = function(){
		
		if(_self.currentConversation.id < 0){
			
			ConversationService.saveConversation(_self.newMessageRecepients)
			.then(function(data) {
				_self.prepareNewConversation(data);
				MessageService.sendMessage(_self.currentConversation.id, _self.message)
				.then(function(data) {
					_self.currentConversation.messages.push(data);
					$location.hash('message_' + (_self.currentConversation.messages.length - 1));
					_self.message = '';	
		        });
	        });
		}else{
			MessageService.sendMessage(_self.currentConversation.id, _self.message)
			.then(function(data) {
				_self.currentConversation.messages.push(data);
				$location.hash('message_' + (_self.currentConversation.messages.length - 1));
				_self.message = '';	
	        });
		}
		
	}
	
	this.prepareNewConversation = function(conversation){
		
		var index = -1;
		var currentConversation;
		angular.forEach(_self.conversations, function(value, key) {
			  if(value.id == conversation.id){
				  index = key;
			  }
		});
		
		if(-1 !== index){
			_self.selectConversation(_self.conversations[index], index);
		}else{
			_self.conversations.unshift(conversation);
			_self.selectConversation(conversation, 0);
		}
	}
	
	this.newMessage = function(){
		_self.isNewMessage = true;
		_self.currentConversation = { id : -1};
		_self.message = '';
		_self.newMessageRecepients = [];
	}
	
	this.deleteConversation = function(){
		ConfirmationDialogService.openModal("CONVERSATION_DELETE_MODAL_TITLE", 
											"CONVERSATION_DELETE_MODAL_DESCRIPTION", 
											"SUBMIT_BUTTON", 
											"CANCEL_BUTTON")
	        .then(function() {
	        	ConversationService.deleteConversation(_self.currentConversation.id)
				.then(function(data) {
					_self.currentConversation = {};
					_self.message = '';
					_self.conversations.splice(_self.currentConversationIndex, 1);
					$log.debug("Conversation deleted");
		        });
	        });
			
		
	}
	
}]);