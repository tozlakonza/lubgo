lubgoApplication.controller('PigeonCtrl', ['$scope', '$log', '$stateParams', 'PigeonService', 'Upload', function ($scope, $log, $stateParams, PigeonService, Upload) {
	
	var _self = this;

	_self.uuid = $stateParams.uuid;

	$scope.selectPane('pigeons', false);

	this.pigeon = {
		sex : 'COCK',
		image : {},
		achievements : []
	};
	
	this.achievement = {
		type : 'PRIZE'
	};
	
	this.initPigeon = function() {
		this.pigeon = {
			sex : 'COCK',
			image : {},
			achievements : []
		}; 
		this.achievement = {
			type : 'PRIZE'
		};
		_self.file = null;
	}
	
	this.createNewPigeon = function() {
		PigeonService.createNewPigeon(this.pigeon)
			.then(function(data) {
				$scope.changeState('profile.pigeons', {uuid : _self.uuid})
	        });
	}
	
	this.addAchievement = function() {
		if(!angular.equals({}, this.achievement)){
			this.pigeon.achievements.push(this.achievement);
			this.achievement = {
				type : 'PRIZE'
			};
		}
	}
	
	$scope.$watch(function() {
        return _self.file;}, function () {
        if (_self.file != null) {
            $scope.uploadFile(_self.file); 
        }
    });
    
    $scope.uploadFile = function (file) {
    	if (file && !file.$error) {
			Upload.upload({
			    url: '/images',
			    data: {
			      file: file,
			      save: false
			    }
			}).success(function (data, status, headers, config) {
			    _self.pigeon.image.uuid = data.uuid;
			});
        }
    };
	
}]);