lubgoApplication.controller('FriendsCtrl', ['$scope', '$log', 'friendsData',  function ($scope, $log, friendsData) {

	var _self = this;

	$scope.selectPane('friends', false);
	
	_self.connections = friendsData.connections;
	
}]);
