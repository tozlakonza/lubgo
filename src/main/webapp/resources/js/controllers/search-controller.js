lubgoApplication.controller('SearchCtrl', ['$scope', '$stateParams', '$location', 'SearchService', '$log', function ($scope, $stateParams, $location, SearchService, $log) {

	var _self = this;
	
    $scope.searchResults = [];

    $scope.keyword = $stateParams.searchTerm;

    _self.maxResult = 20;
    _self.firstResult = 0;
    _self.scrollActive = false;
    _self.noMoreResults = false;
    
    $scope.loadResults = function(){
    	
    	if(_self.scrollActive || _self.noMoreResults){
    		return;
    	}
    	
		_self.scrollActive = true;
		
    	SearchService.search($scope.keyword, _self.firstResult, _self.maxResult).then(function(data) {
    		
    		$scope.searchResults.push.apply($scope.searchResults, data.result);
            
    		if(data.result.length < _self.maxResult){
    			_self.noMoreResults = true;
    		}else{
    			_self.firstResult = _self.firstResult + _self.maxResult;
    			_self.scrollActive = false;
    		}
    		
        });
    	
    }

}]);
