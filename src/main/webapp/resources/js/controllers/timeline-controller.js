lubgoApplication.controller('TimelineCtrl', ['$scope', '$log', 'postsData', function ($scope, $log, postsData) {

	var _self = this;

	$scope.selectPane('timeline', false);
	
	_self.posts = postsData.posts;
	
}]);
