lubgoApplication.controller("RootController", [ '$scope', '$translate', '$location', '$state', 'UserService', 'SocketService', function($scope, $translate, $location, $state, UserService, SocketService){
	
	var _self = this;
	
	// Left Side Menu 
	
	this.sidebarMenuCollapsed = true;
	
	this.isSidebarMenuCollapsed = function(){
		return this.sidebarMenuCollapsed;
	};
	
	this.collapseSidebarMenu = function(){
		
		_self.sidebarMenuCollapsed = !_self.sidebarMenuCollapsed;
		
		if(!_self.sidebarMenuCollapsed && !_self.controlPanelCollapsed){
			_self.controlPanelCollapse();	
		}
	};
	
	// Right control panel
	
	this.controlPanelCollapsed = true;
	this.controlPanelStyle = '';
	
	this.isControlPanelCollapsed = function(){
		return this.controlPanelCollapsed;
	};
	
	this.controlPanelCollapse = function(){
		
		_self.controlPanelCollapsed = !_self.controlPanelCollapsed;
		
		if(_self.controlPanelCollapsed){
			_self.controlPanelStyle = '';
		}else{
			_self.controlPanelStyle = {position: 'absolute', height: '2106px'};
		}
		
		if(!_self.sidebarMenuCollapsed && !_self.controlPanelCollapsed){
			_self.collapseSidebarMenu();	
		}
	};
	
	// Language 
	
	$scope.changeLanguage = function (langKey) {
	    $translate.use(langKey);
	};
	
	
	UserService.getUserInfoFull().then(function(data) {
		$scope.currentUser = data;
		var config = {
			RECONNECT_TIMEOUT : 5000,
			SOCKET_URL : "/chat", // StompEndpoint
			CHAT_TOPIC : "/topic/message/" + $scope.currentUser.uuid, // @SendTo
			//CHAT_BROKER : "/app/chat/2", // @MessageMapping
			PRIORITY : 10
		}
		SocketService.initialize(config);
	});
	
	$scope.changeState = function(state, params){
	    $state.transitionTo(state, params, {reload: true});
	}	
	
	$scope.defaultDateformat = 'yyyy/MM/dd';
	
	this.term = '';

    this.search = function() {
        $location.path("/searchResults/" + encodeURIComponent(_self.term));
    }

    this.logout = function () {
        UserService.logout();
    }

}]);