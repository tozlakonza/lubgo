lubgoApplication.controller('ArticleCtrl', ['$scope', '$log', '$stateParams', 'ArticleService', 'PostService', 'ConfirmationDialogService', 'toaster', function ($scope, $log, $stateParams, ArticleService, PostService, ConfirmationDialogService, toaster) {
	
	var _self = this;

	_self.uuid = $stateParams.uuid;
	_self.articleUuid = $stateParams.articleUuid;
	
	$scope.selectPane('articles', false);
	
	this.article = {
		title : '',
		body : '',
		likes : []
	};
	
	if(_self.articleUuid != undefined){
		ArticleService.getArticleByUUID(_self.articleUuid)
			.then(function(data) {
				_self.article = data;
	        });
	}
	
	
	this.publishArticle = function() {
		ArticleService.saveArticle(this.article)
			.then(function(data) {
				$scope.changeState('profile.articles', {uuid : _self.uuid})
	        });
	}
	
	$scope.newCommentContent = "";

    $scope.addComment = function() {
        PostService.saveArticleComment({
        		articleId: _self.article.id,
                comment: $scope.newCommentContent
            })
            .then(function(data) {
                $scope.newCommentContent = "";
                _self.article.comments.push(data);
                
                toaster.success({title: "Comment added"});
            })
    };
    
    $scope.deleteComment = function(index, commentId) {
        ConfirmationDialogService.openModal("POSTS_COMMENT_DELETE_MODAL_TITLE", "POSTS_COMMENT_DELETE_MODAL_DESCRIPTION", "POSTS_COMMENT_DELETE_MODAL_SUBMIT_BUTTON", "POSTS_COMMENT_DELETE_MODAL_CANCEL_BUTTON")
        .then(function() {
            PostService.deleteArticleComment(_self.article.id, commentId)
                .then(function(data) {
                	_self.article.comments.splice(index, 1);
                    toaster.error({title: "Comment deleted"});
                });
        });
    }
    
    $scope.shareLink = function() {
        FacebookService.share(_self.article.description);
    }

    $scope.isLiked = function() {
        for (var i = 0; i < _self.article.likes.length; i++) {
            if (_self.article.likes[i].email == $scope.currentUser.email) {
                return true;
            }
        }
        return false;
    }

    $scope.toogleLike = function() {

        var promise;

        if ($scope.isLiked()) {
            promise = ArticleService.unLikeArticle(_self.article.id);
        } else {
            promise = ArticleService.likeArticle(_self.article.id);
        }

        promise.then(function(result) {
        	ArticleService.getArticleByUUID(_self.article.uuid)
                .then(function(data) {
                    _self.article = data;
                });
        })
    }
	
}]);