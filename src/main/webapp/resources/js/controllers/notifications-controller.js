lubgoApplication.controller('NotificationsCtrl', ['$scope', '$log', 'UserService', function ($scope, $log, UserService) {
    var _self = this;
    _self.notifications = [];

    this.getNotifications = function() {
        UserService.getAllNotifications().then(function(data) {
            _self.notifications = data;
        });
    };

    this.getNotificationClass = function(notification) {
        if (notification.type == 'POST_COMMENT') {
            return 'fa-comments bg-yellow';
        }
        else if (notification.type == 'POST_ADDED') {
            return 'fa-envelope bg-yellow';
        }
        else if (notification.type == 'CONNECTION_SENT') {
            return 'fa-user bg-aqua';
        }
        else if (notification.type == 'CONNECTION_ACCEPTED') {
            return 'fa-user bg-aqua';
        }
    };

    this.getNotifications();
}]);