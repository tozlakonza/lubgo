lubgoApplication.controller('PigeonsCtrl', ['$scope', '$log', '$stateParams', 'PigeonService', 'ConfirmationDialogService', 'Upload', 'toaster', function ($scope, $log, $stateParams, PigeonService, ConfirmationDialogService, Upload, toaster) {
	
	var _self = this;
	
	_self.uuid = $stateParams.uuid;

	this.pigeons = [];
	
	$scope.selectPane('pigeons', false);
	
	this.createNewPigeon = function() {
		$scope.changeState('profile.newpigeon', {uuid : _self.uuid})
	}

	this.openPigeonProfile = function(uuid) {
		$scope.changeState('profile.pigeon', {uuid : _self.uuid, pigeonuuid : uuid})
	}
	
	_self.maxResult = 10;
    _self.firstResult = 0;
    _self.scrollActive = false;
    _self.noMoreResults = false;
    
    $scope.loadPigeons = function(){
    	
    	if(_self.scrollActive || _self.noMoreResults){
    		return;
    	}
    	
		_self.scrollActive = true;
		
		PigeonService.getPigeonsByUUID(_self.uuid, _self.firstResult, _self.maxResult).then(function(data) {
    		
    		_self.pigeons.push.apply(_self.pigeons, data.pigeons);
            
    		if(data.pigeons.length < _self.maxResult){
    			_self.noMoreResults = true;
    		}else{
    			_self.firstResult = _self.firstResult + _self.maxResult;
    			_self.scrollActive = false;
    		}
    		
        });
    	
    }	
    
    $scope.deletePigeon = function(pigeonId, index) {
        ConfirmationDialogService.openModal("PIGEONS_PIGEON_DELETE_MODAL_TITLE", "PIGEONS_PIGEON_DELETE_MODAL_DESCRIPTION", "PIGEONS_PIGEON_DELETE_MODAL_SUBMIT_BUTTON", "PIGEONS_PIGEON_DELETE_MODAL_CANCEL_BUTTON")
            .then(function() {
            	PigeonService.deletePigeon(pigeonId)
                    .then(function(data) {
                    	$log.debug("index: " + index + ", pigeonId: " + pigeonId);
                    	_self.pigeons.splice(index, 1);
                    	
                    	toaster.error({title: "Pigeon deleted"});
                    });
            });
    }
		
}]);