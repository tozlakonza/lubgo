lubgoApplication.controller('PigeonProfileCtrl', ['$scope', '$log', '$stateParams', 'PigeonService', 'FacebookService', 'PostService', 'ConfirmationDialogService', 'Upload', 'toaster', 'pigeonData', function ($scope, $log, $stateParams, PigeonService, FacebookService, PostService, ConfirmationDialogService, Upload, toaster, pigeonData) {
	
	var _self = this;

	_self.uuid = $stateParams.uuid;
	_self.pigeonuuid = $stateParams.pigeonuuid;

	$scope.selectPane('pigeons', false);

	this.pigeon = pigeonData;
	
	$scope.newCommentContent = "";

    $scope.addComment = function() {
        PostService.savePigeonComment({
                pigeonId: _self.pigeon.id,
                comment: $scope.newCommentContent
            })
            .then(function(data) {
                $scope.newCommentContent = "";
                _self.pigeon.comments.push(data);
                
                toaster.success({title: "Comment added"});
            })
    };
    
    $scope.deleteComment = function(index, commentId) {
        ConfirmationDialogService.openModal("POSTS_COMMENT_DELETE_MODAL_TITLE", "POSTS_COMMENT_DELETE_MODAL_DESCRIPTION", "POSTS_COMMENT_DELETE_MODAL_SUBMIT_BUTTON", "POSTS_COMMENT_DELETE_MODAL_CANCEL_BUTTON")
        .then(function() {
            PostService.deletePigeonComment(_self.pigeon.id, commentId)
                .then(function(data) {
                	_self.pigeon.comments.splice(index, 1);
                    toaster.error({title: "Comment deleted"});
                });
        });
    }
    
    $scope.shareLink = function() {
        FacebookService.share(_self.pigeon.description);
    }

    $scope.isLiked = function() {
        for (var i = 0; i < _self.pigeon.likes.length; i++) {
            if (_self.pigeon.likes[i].email == $scope.currentUser.email) {
                return true;
            }
        }
        return false;
    }

    $scope.toogleLike = function() {

        var promise;

        if ($scope.isLiked()) {
            promise = PigeonService.unLikePigeon(_self.pigeon.id);
        } else {
            promise = PigeonService.likePigeon(_self.pigeon.id);
        }

        promise.then(function(result) {
            PigeonService.getPigeonByUUID(_self.pigeon.uuid)
                .then(function(data) {
                    _self.pigeon = data;
                });
        })
    }
	
}]);