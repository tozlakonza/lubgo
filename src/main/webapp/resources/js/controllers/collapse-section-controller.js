lubgoApplication.controller('CollapseSectionCtrl', ['$scope', '$log', function ($scope, $log) {

	$scope.collapsed = true;
	
	$scope.collapse = function() {
		$scope.collapsed = !$scope.collapsed;
	};
	
}]);
