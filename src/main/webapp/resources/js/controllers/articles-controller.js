lubgoApplication.controller('ArticlesCtrl', ['$scope', '$log', '$stateParams', 'ArticleService', 'ConfirmationDialogService', 'toaster', function ($scope, $log, $stateParams, ArticleService, ConfirmationDialogService, toaster) {

	var _self = this;

	_self.uuid = $stateParams.uuid;

	this.articles = [];
	
	$scope.selectPane('articles', false);
	
	this.writeANewArticle = function() {
		$scope.changeState('profile.newarticle', {uuid : _self.uuid})
	}
	
	this.openArticle = function(uuid){
		$scope.selectPane('articles', true, 'profile.articlepreview', {articleUuid : uuid});
	}
	
	_self.maxResult = 10;
    _self.firstResult = 0;
    _self.scrollActive = false;
    _self.noMoreResults = false;
    
    $scope.loadArticles = function(){
    	
    	if(_self.scrollActive || _self.noMoreResults){
    		return;
    	}
    	
		_self.scrollActive = true;
		
		ArticleService.getArticlesByUUID(_self.uuid, _self.firstResult, _self.maxResult).then(function(data) {
    		
    		_self.articles.push.apply(_self.articles, data.articles);
            
    		if(data.articles.length < _self.maxResult){
    			_self.noMoreResults = true;
    		}else{
    			_self.firstResult = _self.firstResult + _self.maxResult;
    			_self.scrollActive = false;
    		}
    		
        });
    	
    }	
    
    $scope.deleteArticle = function(articleId, index) {
        ConfirmationDialogService.openModal("ARTICLES_ARTICLE_DELETE_MODAL_TITLE", "ARTICLES_ARTICLE_DELETE_MODAL_DESCRIPTION", "ARTICLES_ARTICLE_DELETE_MODAL_SUBMIT_BUTTON", "ARTICLES_ARTICLE_DELETE_MODAL_CANCEL_BUTTON")
            .then(function() {
            	ArticleService.deleteArticle(articleId)
                    .then(function(data) {
                    	$log.debug("index: " + index + ", articleId: " + articleId);
                    	_self.articles.splice(index, 1);
                    	
                    	toaster.error({title: "Article deleted"});
                    });
            });
    }
	
}]);
