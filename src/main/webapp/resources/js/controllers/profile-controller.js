lubgoApplication.controller('ProfileCtrl', ['$scope', '$log', 'UserService', 'ArticleService', 'PigeonService', '$stateParams', 'Upload', 'toaster', function ($scope, $log, UserService, ArticleService, PigeonService, $stateParams, Upload, toaster) {
	
	var _self = this;
	
	$scope.paneSelected = {
		timeline : true
	};
	
	_self.connectionStatus = '';
	
	_self.uuid = $stateParams.uuid;
	
	_self.numberOfFriends = 0;
	_self.numberOfPigeons = 0;
	_self.numberOfArticles = 0;
	
    this.getUserDetails = function (uuid) {
    	
    	UserService.getUserInfoFullById(uuid).then(function(data) {
    		_self.userInfoData = data;
    		_self.connectionStatus = data.connectionStatus;
        });
    	
    	UserService.countFriendsByUUID(uuid).then(function(data) {
    		_self.numberOfFriends = data;
        });
    	
    	PigeonService.countPigeonsByUUID(uuid).then(function(data) {
    		_self.numberOfPigeons = data;
        });
    	
    	ArticleService.countArticlesByUUID(uuid).then(function(data) {
    		_self.numberOfArticles = data;
        });
    }

	this.getUserDetails(this.uuid);
	
	this.updateBasicInfo = function(){

		$log.debug("Updating basic info.");
		
		UserService.updateBasicInfo(_self.userInfoData.firstName, _self.userInfoData.lastName, _self.userInfoData.email, _self.userInfoData.phone)
	        .then(function(data) {
                toaster.success({title: "Basic info successfully updated."});
	        	$log.debug("Basic info successfully updated.");
	        });
	};
	
	_self.passwordManager = {};
	
	this.changePassword = function(){

		$log.debug("Changing old password: " + _self.passwordManager.oldPassword + " to new password: " + _self.passwordManager.newPassword);
		
		UserService.changePassword(_self.passwordManager.oldPassword, _self.passwordManager.newPassword)
	        .then(function(data) {
	        	_self.passwordManager = {};
                toaster.success({title: "Password successfully changed."});
	        	$log.debug("Password successfully changed.");
	        });
	};
	
	this.updateOtherInfo = function(){

		$log.debug("Updating Other info.");
		
		UserService.updateOtherInfo(_self.userInfoData.alliance, _self.userInfoData.location, _self.userInfoData.club, _self.userInfoData.autobiography)
	        .then(function(data) {
                toaster.success({title: "Other info successfully updated."});
                $log.debug("Other info successfully updated.");
	        });
		
	};
	
	$scope.selectPane = function(pane, redirect, view, params){
		$scope.paneSelected={};
		$scope.paneSelected[pane] = true;
		if(redirect){
			if(view == undefined){
				view = 'profile.' + pane;
			}
			if(params == undefined){
				params = {uuid : _self.uuid}
			}else{
				params.uuid = _self.uuid;
			}
			$scope.changeState(view, params);
		}
	}
	
    this.makeConnectionRequest = function() {
        UserService.makeConnectionRequest(_self.userInfoData.uuid).then(function(data) {
            _self.getUserDetails(_self.uuid);
        });
    }
    
    this.acceptConnectionRequest = function() {
        UserService.acceptConnectionRequest(_self.userInfoData.uuid).then(function(data) {
            _self.getUserDetails(_self.uuid);
        });
    }
    
    $scope.$watch(function() {
        return _self.file;}, function () {
        if (_self.file != null) {
        	_self.uploadFile(_self.file); 
        }
    });
    
    this.uploadFile = function (file) {
    	if (file && !file.$error) {
			Upload.upload({
			    url: '/images',
			    data: {
			      file: file,
			      save: true
			    }
			}).success(function (data, status, headers, config) {
				_self.userInfoData.image.uuid = data.uuid;
				UserService.updateProfilePicture(data.uuid)
					.then(function(data) {
						$log.debug("Successfully updated");
				});
			});
        }
    };
    
}]);
	
