lubgoApplication.directive('controlSidebar', function(){
	return {
		restrict: 'E',
		templateUrl: 'views/partials/control-sidebar-partial.html'
	};
});