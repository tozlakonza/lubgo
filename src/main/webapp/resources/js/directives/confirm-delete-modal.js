lubgoApplication.directive('confirm', ['$uibModal', '$parse', function($uibModal, $parse){
  return {
    link: function(scope, el, attr){
      var title = attr.title;
      var description = attr.description;
      var confirmButton = attr.confirm;
      el.bind('click', function(){
        var instance = $uibModal.open({ backdrop: false, 
          template: '<div class="modal-content"> <div class="modal-header"> <button type="button" class="close" ng-click="cancel()" aria-label="Close"> <span aria-hidden="true">×</span></button> <h4 class="modal-title">Default Modal</h4> </div> <div class="modal-body"> <p>One fine body…</p> </div> <div class="modal-footer"> <button type="button" class="btn btn-default pull-left" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="cancel()">Delete Post</button> </div> </div>',    
          controller: ['$scope', '$modalInstance', function(s, m){
            s.ok = function(){
              m.close();
            };
            s.cancel = function(){
              m.dismiss();
            };
          }]
        });
        
        instance.result.then(function(){
          // close - action!
          $parse(attr.onConfirm)(scope);
        }, 
        function(){
          // dimisss - do nothing
        });
      });
    }
  }
}])