lubgoApplication.directive('sidebarMenu', function(){
	return {
		restrict: 'E',
		templateUrl: 'views/partials/sidebar-menu-partial.html'
	};
});