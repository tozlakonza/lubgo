lubgoApplication.directive("post", ['PostService', 'FacebookService', 'ConfirmationDialogService', '$window', '$log', 'toaster', '$state', function(PostService, FacebookService, ConfirmationDialogService, $window, $log, toaster, $state) {
    return {
        restrict: 'E',

        scope: { posts: '=', post : '=', user: '=', index: '='},

        controller: function($scope) {

            $scope.newCommentContent = "";

            $scope.addComment = function(postId) {
                PostService.saveComment({
                        postId: postId,
                        comment: $scope.newCommentContent
                    })
                    .then(function(data) {
                        $scope.newCommentContent = "";
                        $scope.post.children.push(data);
                        
                        toaster.success({title: "Comment added"});
                    })
            };

            $scope.deletePost = function(postId) {
                ConfirmationDialogService.openModal("POSTS_POST_DELETE_MODAL_TITLE", "POSTS_POST_DELETE_MODAL_DESCRIPTION", "POSTS_POST_DELETE_MODAL_SUBMIT_BUTTON", "POSTS_POST_DELETE_MODAL_CANCEL_BUTTON")
                    .then(function() {
                        PostService.deletePost(postId)
                            .then(function(data) {
                            	$log.debug("index: " + $scope.index + ", postId: " + postId);
                            	$scope.posts.splice($scope.index, 1);
                            	
                            	toaster.error({title: "Post deleted"});
                            });
                    });
            }

            $scope.deleteComment = function(index, postId, commentId) {
                ConfirmationDialogService.openModal("POSTS_COMMENT_DELETE_MODAL_TITLE", "POSTS_COMMENT_DELETE_MODAL_DESCRIPTION", "POSTS_COMMENT_DELETE_MODAL_SUBMIT_BUTTON", "POSTS_COMMENT_DELETE_MODAL_CANCEL_BUTTON")
                .then(function() {
                    PostService.deleteComment(postId, commentId)
                        .then(function(data) {
                            $scope.post.children.splice(index, 1);
                            toaster.error({title: "Comment deleted"});
                        });
                });
            }
            
            $scope.shareLink = function() {
                FacebookService.share($scope.post.description);
            }

            $scope.isLiked = function() {
                for (var i = 0; i < $scope.post.likes.length; i++) {
                    if ($scope.post.likes[i].email == $scope.user.email) {
                        return true;
                    }
                }
                return false;
            }

            $scope.toogleLike = function() {

                var promise;

                if ($scope.isLiked()) {
                    promise = PostService.unLikePost($scope.post.id);
                } else {
                    promise = PostService.likePost($scope.post.id);
                }

                promise.then(function(result) {
                    PostService.getPost($scope.post.id)
                        .then(function(data) {
                            $scope.post = data;
                        });
                })
            }
            
            $scope.changeState = function(state, params){
        	    $state.transitionTo(state, params, {reload: true});
        	}

        },

        templateUrl: 'views/partials/post-single-template.html'

    }
}]);