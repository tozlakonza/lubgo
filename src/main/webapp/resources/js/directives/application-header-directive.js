lubgoApplication.directive("applicationHeader", function(){
	return {
		restrict: 'E',
		templateUrl: 'views/partials/application-header-partial.html',
        controller: function(UserService, ConversationService, $scope, SocketService, toaster) {
        	
        	var _self = this;
        	
        	this.conversationsPage = 1;
        	this.conversationsSize = 10;

        	ConversationService.getConversations(1, 10).then(function(data) {
    			_self.conversations = data.conversations; 
    		});
        	
    		this.connections = [];

			this.getConnectionRequests = function() {
				UserService.getNewConnectionRequests().then(function(data) {
					_self.connections = data.connections;
				});
			}

			this.getConnectionRequests();

    		this.acceptConnectionRequest = function(uuid, index) {
    	        UserService.acceptConnectionRequest(uuid).then(function(data) {
    	        	_self.connections.splice(index, 1);
    	        });
    	    };
    		
    		this.rejectConnectionRequest = function(uuid, index) {
    	        UserService.rejectConnectionRequest(uuid).then(function(data) {
    	        	_self.connections.splice(index, 1);
    	        });
    	    };

			this.notifications = [];

			this.getNotifications = function() {
				UserService.getNotifications().then(function(data) {
					_self.notifications = data;
				});
			};

			this.markNotificationRead = function(notificationId) {
				UserService.markNotificationRead(notificationId).then(function(data) {
					console.log("Notification with id " + notificationId + " marked read");
					_self.getNotifications();
				});
			};

			this.markAllNotificationsRead = function() {
				UserService.markAllNotificationsRead().then(function(data) {
					console.log("All notification marked read");
					_self.getNotifications();
				});
			};

			this.getNotifications();

			/*
        	this.notifications = [
				{
					clazz : 'fa-users text-aqua',
					body : '5 new members joined today'
				},
				{
					clazz : 'fa-warning text-yellow',
					body : 'Very long description here that may not fit into the page and may cause design problems'
				},
				{
					clazz : 'fa-users text-red',
					body : '5 new members joined'
				},
				{
					clazz : 'fa-shopping-cart text-green',
					body : '25 sales made'
				},
				{
					clazz : 'fa-user text-red',
					body : 'You changed your email'
				}
				
        	];
*/

			SocketService.receive().then(null, null, function(msg) {
				var message = JSON.parse(msg);
				//$scope.messages.push($scope.message);
				toaster.pop(message.level, message.message);
				if (message.type == 'CONNECTION_SENT') {
					_self.getConnectionRequests();
				}
				else if (message.type == 'CONNECTION_ACCEPTED') {
					_self.getConnectionRequests();
					_self.getNotifications();
				}
				else if (message.type == 'POST_COMMENT') {
					_self.getNotifications();
				}
				else if (message.type == 'POST_ADDED') {
					_self.getNotifications();
				}
			});

		},
        controllerAs: "header"
	};
});