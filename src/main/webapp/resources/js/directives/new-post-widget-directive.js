lubgoApplication.directive("newPostWidget", ['PostService', 'HelperService', 'Upload', '$log', 'toaster', '$state', function(PostService, HelperService, Upload, $log, toaster, $state) {
    return {
        restrict: 'E',

        scope: { posts: '=', user: '=', homepage: '='},
        
        controller: function($scope) {
        	
            $scope.newPostContent = "";
            $scope.linkContent = {};
            $scope.imageUrl = {};

            $scope.addPost = function() {

                if ($scope.newPostContent == "" && angular.equals({}, $scope.linkContent) && angular.equals({}, $scope.imageUrl)) {
                	$scope.newPostContent = "";
                    return;
                }

                PostService.savePost({
                        description: $scope.newPostContent,
                        parent: {},
                        link: $scope.linkContent,
                        image: $scope.imageUrl
                    })
                    .then(function(data) {
                        $scope.file = null;
                        $scope.newPostContent = "";
                        $scope.linkContent = {};
                        $scope.imageUrl = {};
                        
                        if(!$scope.homepage){
                        	$scope.posts.unshift(data);
                        }

                        toaster.success({title: "Post added"});
                        
                        if($scope.homepage){
                        	$log.debug("Homepage = " + $scope.homepage);
                        	$scope.goToProfilePage();
                        }
                	});
            };

            $scope.goToProfilePage = function(){
        	    $state.transitionTo('profile.timeline', {uuid : $scope.user.uuid}, {reload: true});
        	}
            
            $scope.deleteLink = function() {
                $scope.linkContent = {};
            }

            $scope.clearImage = function() {
                $scope.imageUrl = {};
            }

            $scope.isEmptyLink = function() {
                return angular.equals({}, $scope.linkContent);
            }

            $scope.paste = function(e) {
                if (!angular.equals({}, $scope.linkContent)) {
                    return;
                }

                var content = e.originalEvent.clipboardData.getData('text/plain');

                if (HelperService.isUrlValid(content)) {
                    PostService.linkPreview(content).then(function(data) {
                        $scope.linkContent = data;
                    });
                }
            }

            $scope.$watch('file', function() {
                if ($scope.file != null) {
                    $scope.uploadFile($scope.file);
                } else {
                    $scope.clearImage();
                }
            });


            $scope.uploadFile = function(file) {
                $scope.imageUrl = {};
                if (file && !file.$error) {
                    Upload.upload({
                        url: '/images',
                        data: {
                            file: file,
          			        save: false
                        }
                    }).success(function(data, status, headers, config) {
                        $scope.imageUrl = data.uuid;
                        $scope.linkContent = {};
                        $log.debug('Response: ' + JSON.stringify(data) + '\n');
                    });
                }
            };
        	
        },

        templateUrl: 'views/partials/post-new-template.html'

    }
}]);