var app = angular.module('lubgoPublicApp', ['ui.router', 'ngMessages', 'spring-security-csrf-token-interceptor', 'ngCookies'])
    .config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/login");

        $stateProvider
            .state('login', {
                url: "/login",
                templateUrl: "views/login.html",
                controller: 'LoginController',
                controllerAs: 'loginC'
            })
            .state('newUser', {
                url: "/new-user",
                templateUrl: "views/new-user.html",
                controller: 'NewUserController',
                controllerAs: 'newUserC'
            })
            .state('forgetPassword', {
                url: "/forgot-password",
                templateUrl: "views/forgot-password.html",
                controller: 'ForgotPasswordController',
                controllerAs: 'forgotPasswordC'
            });

        $urlRouterProvider.otherwise('/login');
        
    });