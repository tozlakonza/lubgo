app.service('loginService', ['$http', '$q', function ($http, $q) {

    this.login = function(email, password) {

        var deferred = $q.defer();

        var service = $http({
                method: 'POST',
                url: '/authenticate',
                data: preparePostData(email, password),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "X-Login-Ajax-call": 'true'
                }
            })
            .then(function(response) {
                if (response.data == 'ok') {
                    deferred.resolve();
                } else {
                    deferred.reject();
                }
            });

        return deferred.promise;
    }
    
    var preparePostData = function(em, pas) {
        var email = em != undefined ? em : '';
        var password = pas != undefined ? pas : '';

        return 'email=' + email + '&password=' + password;
    }

    return this.login;

}]);

app.service('createUserService', ['$http', '$q', function($http, $q) {

    this.createUser = function(userFirstName, userLastName, userEmail, userPassword) {

        var deferred = $q.defer();

        var service = $http({
                method: 'POST',
                url: '/user',
                data: {
                	firstName: userFirstName,
                	lastName: userLastName,
                    email: userEmail,
                    plainTextPassword: userPassword
                },
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "text/plain"
                }
            })
            .then(function(response) {
                if (response.status == 200) {
                    deferred.resolve();
                } else {
                    deferred.reject(response.text);
                }
            });

        return deferred.promise;

    }

    return this.createUser;

}]);

app.service('resetPasswordService', ['$http', '$q', function($http, $q) {

    this.resetPassword = function(userMail) {

        var deferred = $q.defer();

        var service = $http({
                method: 'POST',
                url: '/user?email=' + userMail,
                data: {
                    email: userMail
                },
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "text/plain"
                }
            })
            .then(function(response) {
                if (response.status == 200) {
                    deferred.resolve();
                } else {
                    deferred.reject();
                }
            });

        return deferred.promise;

    }

    return this.resetPassword;

}]);