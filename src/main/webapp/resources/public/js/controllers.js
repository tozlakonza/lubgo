app.controller('LoginController', ['$cookies', 'loginService', function($cookies, loginService) {

    var vm = this;

    vm.message = "";

    loadRememberMe();
    
    function loadRememberMe() {
    	
    	if ($cookies.get('lgo-email') != undefined && $cookies.get('lgo-password') != undefined) {
    		vm.email = $cookies.get('lgo-email');
    		vm.password = $cookies.get('lgo-password');
    		vm.remember = true;
    	}
    	
    }
    
    vm.submitForm = function() {
        if (vm.form.$invalid) {
            return;
        }
        vm.login();
    }

    vm.login = function() {
    	
        vm.message = "";

        console.log('Attempting login with email ' + vm.email + ' and password ' + vm.password);

        loginService(vm.email, vm.password)
            .then(function(success) {

            	if(vm.remember){
            		$cookies.put('lgo-email', vm.email);
                	$cookies.put('lgo-password', vm.password);
            	}else{
            		$cookies.remove('lgo-email');
            		$cookies.remove('lgo-password');
            	}
            	window.location.replace('/resources/index.html');
                console.log('You have successfully logged in.');

            })
            .catch(function(error) {
                vm.message = "Your username or password was incorrect!";
                console.log('An error has occured while logging in:' + error);
            });
    }

}]);

app.controller('ForgotPasswordController', ['$http', 'resetPasswordService', function($http, resetPasswordService) {

    var vm = this;

    vm.message = "";

    vm.submitForm = function() {
        if (vm.form.$invalid) {
            return;
        }
        vm.login();
    }

    vm.login = function() {

        vm.message = "";
        console.log('Reseting password for user with email ' + vm.email);

        resetPasswordService(vm.email)
            .then(function(response) {

                window.location.replace('/login');

            })
            .catch(function(error) {
                vm.message = "User not found with this email!";
                console.log("User not found with email: " + error);

            });
    }
}]);

app.controller('NewUserController', ['loginService', 'createUserService', function(loginService, createUserService) {

    var vm = this;

    vm.message = false;
    vm.success = false;

    vm.submitForm = function() {
        if (vm.form.$invalid) {
            return;
        }
        vm.createUser();
    }

    vm.createUser = function() {
        vm.message = "";
        console.log('Creating user with email ' + vm.email + ' and password ' + vm.password);

        var userEmail = vm.email;
        var userPassword = vm.password;

        createUserService(vm.firstName, vm.lastName, userEmail, userPassword)
            .then(function(response) {
                loginService(userEmail, userPassword)
                    .then(function(success) {
                        window.location.replace('/resources/index.html');
                        console.log('You have successfully logged in.');
                    })
                    .catch(function(error) {
                        vm.message = "An error has occured while logging in";
                        console.log('An error has occured while logging in:' + error);
                    });
            })
            .catch(function(error) {
                vm.message = "Failed user creation!";
                console.log("Failed user creation: " + error);
            });;
    }
}]);